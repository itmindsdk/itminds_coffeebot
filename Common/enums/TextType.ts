/**
 * All the texts that need to be changed by overiding the default have an enum value that can identify the text needed.
 */
enum TextType {
  WelcomeUserText = "WelcomeUserText",
  VacationText = "VacationText",
  BotPresentationText = "BotPresentationText",
  OnlyAdminText = "OnlyAdminText",
  GroupSizeText = "GroupSizeText",
  GroupSizeErrorText = "GroupSizeErrorText",
  SetGroupSizeText = "SetGroupSizeText",
  ErrorUnknownCommandText = "ErrorUnknownCommandText",
  GroupWelcomeText = "GroupWelcomeText",
  CoffeeCaptainPresentationText = "CoffeeCaptainPresentationText",
  GroupDetailsText = "GroupDetailsText",
  BotJoinText = "BotJoinText",
  ErrorOnlyAdminGroupSize = "ErrorOnlyAdminGroupSize",
  ErrorNotInt = "ErrorNotInt",
  ErrorGroupGreaterThan = "ErrorGroupGreaterThan",
  WelcomeGroup = "WelcomeGroup",
  WhoIsCaptain = "WhoIsCaptain",
  CoffeeCaptainDescription = "CoffeeCaptainDescription",
  RepostMsg = "RepostMsg",
  YourGroupCompleted = "YourGroupCompleted",
  ChannelGroupCompleted = "ChannelGroupCompleted",
  ActionReminder = "ActionReminder",
  ActionReminderDetails = "ActionReminderDetails"
}

export default TextType;
