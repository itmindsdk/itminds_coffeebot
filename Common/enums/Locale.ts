enum Locale {
  default = "*",
  enUs = "en-us", // NOTE: Not in use!
  enGb = "en-gb", // NOTE: Not in use!
  daDk = "da-dk" // NOTE: Not in use!
}

export default Locale;
