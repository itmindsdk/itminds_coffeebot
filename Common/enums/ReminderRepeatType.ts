enum ReminderRepeatType {
  BEFORE_END,
  AFTER_START,
  NONE
}

export default ReminderRepeatType;
