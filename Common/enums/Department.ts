enum Department {
  KØBENHAVN,
  AARHUS,
  OSLO,
  UDEN_AFDELING,
  AALBORG
}

export default Department;
