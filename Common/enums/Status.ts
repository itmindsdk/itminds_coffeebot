/**
 * Describes the status of a vacation requests in the sursen DB.
 */
enum Status {
  NOT_SEEN,
  SEEN,
  ACCEPTED,
  REJECTED
}

export default Status;
