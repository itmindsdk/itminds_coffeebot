export default interface ChannelFeatures {
  useCaptain?: boolean;
  useReminders: boolean;
  useThreads?: boolean;
}
