export default interface DynamicContentContext {
  channelId: string;
  captainUserId?: string;
  groupId?: number;
  authorMessage?: { authorUserId: string; message: string };
}
