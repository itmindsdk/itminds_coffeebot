import Status from "../../enums/Status";

export default interface VacationRequest {
  id: number;
  employeeId: number;
  vacationStart: Date;
  vacationEnd: Date;
  createdDate: Date;
  status: Status;
  applicationNote: string;
  responseNote: string;
}
