import Department from "../../enums/Department";

export default interface Employee {
  id: number;
  name: string;
  initials: string;
  birthdate: Date;
  phoneNumber: string;
  department: Department;
  dateStart: Date;
  dateEnd: Date;
  mail: string;
}
