export default interface Schedule {
  runDate: Date;
  eventInterval: number;
}
