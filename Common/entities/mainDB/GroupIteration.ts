import Group from "./Group";
import Channel from "./Channel";

export default interface GroupIteration {
  id?: number;
  groups?: Group[];
  channelId: string;
  channel?: Channel;
}
