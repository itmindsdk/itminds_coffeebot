import GroupMember from "./GroupMember";
import { GroupIteration } from "..";

export default interface Group {
  id?: number;
  isCompleted?: boolean;
  members: GroupMember[];
  thread_ts?: string;
  msgId?: string;
  captain?: number;
  groupIterationId?: number;
  groupIteration?: GroupIteration;
}
