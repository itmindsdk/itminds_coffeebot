export default interface ChannelMember {
  id?: number;
  userId: string;
  skipRounds?: number;
  channel?: string;
  mail?: string;
}
