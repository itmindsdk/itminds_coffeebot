import TextType from "../../enums/TextType";
import Locale from "../../enums/Locale";

export default interface TextObj {
  id?: number;
  channel: string;
  textType: number;
  text: string;
  locale: number;
}
