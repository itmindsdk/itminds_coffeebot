import Group from "./Group";

export default interface GroupMember {
  id?: number;
  userId: string;
  groupId: number;
  group?: Group;
}
