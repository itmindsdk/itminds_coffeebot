import ReminderType from "../../enums/ReminderType";
import ReminderRepeatType from "../../enums/ReminderRepeatType";

export default interface Reminder {
  id?: number;
  channel: string;
  triggerDate: Date;
  haveTriggered: boolean;
  text: string;
  type: ReminderType;
  repeatType?: ReminderRepeatType;
  RepeatDayNumber: number;
}
