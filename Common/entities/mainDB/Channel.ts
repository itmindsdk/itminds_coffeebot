import ChannelMember from "./ChannelMember";
import GroupIteration from "./GroupIteration";
import TextObj from "./TextObj";
import Reminder from "./Reminder";

export default interface Channel {
  id: string;
  name?: string;
  active?: boolean;
  useThreads?: boolean;
  eventInterval?: number;
  runDate?: Date;
  groupSize: number;
  useReminders: boolean;
  useCaptain?: boolean;
  members?: ChannelMember[];
  groupIterations?: GroupIteration[];
  texts?: TextObj[];
  reminders?: Reminder[];
}
