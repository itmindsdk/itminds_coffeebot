import IRoute from "./IRoute";

export default interface IApi {
  execWithBody<T>(
    route: IRoute,
    body: object,
    queryParams?: Record<string, string>
  ): Promise<T>;
  exec<T>(route: IRoute, queryParams?: Record<string, string>): Promise<T>;
  get<T>(url: string, queryParams?: Record<string, string>): Promise<T>;
  post<T>(
    url: string,
    body: object,
    queryParams?: Record<string, string>
  ): Promise<T>;
  put<T>(
    url: string,
    body: object,
    queryParams?: Record<string, string>
  ): Promise<T>;
  delete<T>(url: string, queryParams?: Record<string, string>): Promise<T>;
}
