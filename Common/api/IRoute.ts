import RouteMethod from "./RouteMethod";

export default interface IRoute {
  url: string;
  method: RouteMethod;
}
