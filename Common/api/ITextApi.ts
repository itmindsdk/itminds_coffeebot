import TextObj from "../entities/mainDB/TextObj";
import DynamicContentContext from "../viewModels/DynamicContentContext";
import TextType from "../enums/TextType";
import Locale from "../enums/Locale";

export default interface ITextApi {
  setLocale(locale: Locale): any;
  get(textType: TextType, context: DynamicContentContext): Promise<string>;
  getObj(textType: TextType, context: DynamicContentContext): Promise<TextObj>;
}
