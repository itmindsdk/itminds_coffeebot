import IRoute from "./IRoute";
import RouteMethod from "./RouteMethod";
import TextType from "../enums/TextType";
import Locale from "../enums/Locale";

// interface Routes {
//   [controller: string]: {
//     [routeName: string]: (...args: any[]) => IRoute;
//   };
// }

const Routes = {
  channel: {
    getAll: () => ({ url: "/channel/all", method: RouteMethod.GET }),
    getAllActive: () => ({
      url: "/channel/allActive",
      method: RouteMethod.GET
    }),
    getAllActiveIds: () => ({
      url: "/channel/allActiveIds",
      method: RouteMethod.GET
    }),
    get: (id: string) => ({ url: `/channel/${id}`, method: RouteMethod.GET }),
    create: () => ({ url: "/channel/", method: RouteMethod.POST }),
    delete: (id: string) => ({
      url: `/channel/${id}`,
      method: RouteMethod.DELETE
    }),
    getEventSchedule: (id: string) => ({
      url: `/channel/${id}/eventSchedule`,
      method: RouteMethod.GET
    }),
    getUseThreads: (id: string) => ({
      url: `/channel/${id}/useThreads`,
      method: RouteMethod.GET
    }),
    getUseReminders: (id: string) => ({
      url: `/channel/${id}/useReminders`,
      method: RouteMethod.GET
    }),
    getUseCaptain: (id: string) => ({
      url: `/channel/${id}/useCaptain`,
      method: RouteMethod.GET
    }),
    setNextRunDate: (id: string) => ({
      url: `/channel/${id}/nextRunDateAuto`,
      method: RouteMethod.POST
    }),
    setNextRunDateWithDate: (id: string) => ({
      url: `/channel/${id}/nextRunDate`,
      method: RouteMethod.POST
    }),
    getNextRunDate: (id: string) => ({
      url: `/channel/${id}/nextRunDate`,
      method: RouteMethod.GET
    }),
    getGroupSize: (id: string) => ({
      url: `/channel/${id}/groupSize`,
      method: RouteMethod.GET
    }),
    updateConfig: (id: string) => ({
      url: `/channel/${id}/config`,
      method: RouteMethod.PUT
    }),
    updateRunDate: (id: string) => ({
      url: `/channel/${id}/runDate`,
      method: RouteMethod.PUT
    }),
    updateActive: (id: string) => ({
      url: `/channel/${id}/active`,
      method: RouteMethod.PUT
    }),
    updateGroupSize: (id: string) => ({
      url: `/channel/${id}/groupSize`,
      method: RouteMethod.PUT
    }),
    isBotInChannel: (id: string) => ({
      url: `/channel/${id}/isBotInChannel`,
      method: RouteMethod.GET
    }),
    updateUseCaptain: (id: string) => ({
      method: RouteMethod.PUT,
      url: `/channel/${id}/useCoffeeCaptain`
    }),
    updateUseReminders: (id: string) => ({
      method: RouteMethod.PUT,
      url: `/channel/${id}/useReminders`
    })
  },
  channelMember: {
    getAll: (cid: string) => ({
      url: `/channelMember/channel/${cid}`,
      method: RouteMethod.GET
    }),
    getCount: (cid: string) => ({
      url: `/channelMember/channel/${cid}/numberOf`,
      method: RouteMethod.GET
    }),
    delete: (cid: string, uid: string) => ({
      url: `/channelMember/channel/${cid}/user/${uid}`,
      method: RouteMethod.DELETE
    }),
    deleteAll: (cid: string) => ({
      url: `/channelMember/channel/${cid}`,
      method: RouteMethod.DELETE
    }),
    deleteById: (id: string) => ({
      url: `/channelMember/${id}`,
      method: RouteMethod.DELETE
    }),
    update: (cid: string, uid: string) => ({
      url: `/channelMember/channel/${cid}/user/${uid}`,
      method: RouteMethod.PUT
    }),
    updateById: (id: string) => ({
      url: `/channelMember/${id}`,
      method: RouteMethod.PUT
    }),
    /**
     * Create new channel members. As body give either a single or an array of channel member objects.
     */
    create: () => ({ url: "/channelMember", method: RouteMethod.POST })
  },
  group: {
    getAll: () => ({ url: "/Group/all", method: RouteMethod.GET }),
    create: () => ({ url: "/Group/", method: RouteMethod.POST }),
    delete: (id: number) => ({
      url: `/Group/${id}`,
      method: RouteMethod.DELETE
    }),
    get: (id: number) => ({ url: `/Group/${id}`, method: RouteMethod.GET }),
    getByIterationAndThreads: (iid: number) => ({
      url: `/Group/iteration/${iid}`,
      method: RouteMethod.GET
    }),
    getByIterationIdAndMsgId: (iid: number, msgId: string) => ({
      url: `/Group/iteration/${iid}/msg/${msgId}`,
      method: RouteMethod.GET
    }),
    updateIsComplete: (id: number) => ({
      url: `/Group/${id}`,
      method: RouteMethod.PUT
    }),
    updateMsgId: (groupId: number, msgId: string) => ({
      url: `/Group/${groupId}/msg/${msgId}`,
      method: RouteMethod.PUT
    }),
    updateThreadTs: (groupId: number, threadTs: string) => ({
      url: `/Group/${groupId}/threadts/${threadTs}`,
      method: RouteMethod.PUT
    }),
    createMultiple: () => ({
      url: "/Group/multiple",
      method: RouteMethod.POST
    }),
    getChannelIdFromMsgId: (msgId: String) => ({
      url: `/Group/msg/${msgId}`,
      method: RouteMethod.GET
    })
  },
  groupIteration: {
    create: () => ({ url: "/groupIteration/", method: RouteMethod.POST }),
    delete: (id: number) => ({
      url: `/groupIteration/${id}`,
      method: RouteMethod.DELETE
    }),
    get: (id: number) => ({
      url: `/groupIteration/${id}`,
      method: RouteMethod.GET
    }),
    getAllByChannel: (cid: string) => ({
      url: `/groupIteration/channel/${cid}/all`,
      method: RouteMethod.GET
    }),
    createWithGroupArray: (cid: string) => ({
      url: `/groupIteration/channel/${cid}`,
      method: RouteMethod.POST
    }),
    getCurrentIteration: (cid: string) => ({
      url: `/groupIteration/channel/${cid}`,
      method: RouteMethod.GET
    }),
    getCurrentIterationId: (cid: string) => ({
      url: `/groupIteration/channel/${cid}/id`,
      method: RouteMethod.GET
    }),
    addGroupToCurrentIteration: (cid: string) => ({
      url: `/groupIteration/channel/${cid}`,
      method: RouteMethod.PUT
    }),
    getAllCaptains: (cid: string) => ({
      url: `/groupIteration/channel/${cid}/captains`,
      method: RouteMethod.GET
    })
  },
  groupMember: {
    delete: (gid: number, uid: string) => ({
      url: `/groupMember/user/${uid}/group/${gid}`,
      method: RouteMethod.DELETE
    }),
    deleteById: (id: number) => ({
      url: `/groupMember/${id}`,
      method: RouteMethod.DELETE
    }),
    createGroupMember: () => ({
      url: "/groupMember/",
      method: RouteMethod.POST
    }),
    get: (id: number) => ({
      url: `/groupMember/${id}`,
      method: RouteMethod.GET
    })
  },
  reminder: {
    delete: (cid: string) => ({
      url: `/reminder/channel/${cid}`,
      method: RouteMethod.DELETE
    }),
    getAll: (cid: string) => ({
      url: `/reminder/channel/${cid}`,
      method: RouteMethod.GET
    }),
    getAllNotTrigged: (cid: string) => ({
      url: `/reminder/channel/${cid}/notTriggered`,
      method: RouteMethod.GET
    }),
    updateTriggerDate: (id: number) => ({
      url: `/reminder/${id}/channel`,
      method: RouteMethod.PUT
    }),
    updateHasTriggered: (id: number) => ({
      url: `/reminder/${id}/channel/hasTriggered`,
      method: RouteMethod.PUT
    }),
    deleteById: (id: number) => ({
      url: `/reminder/${id}/channel`,
      method: RouteMethod.DELETE
    }),
    create: () => ({ url: "/channel/reminder/", method: RouteMethod.POST })
  },
  textType: {
    delete: (cid: string) => ({
      url: `/text/channel/${cid}`,
      method: RouteMethod.DELETE
    }),
    get: (cid: string, textType: TextType, locale: Locale) => ({
      url: `/text/channel/${cid}/${textType}/${locale}`,
      method: RouteMethod.POST
    }),
    deleteById: (id: number) => ({
      url: `/text/${id}/channel`,
      method: RouteMethod.DELETE
    }),
    create: () => ({ url: "/text/channel/", method: RouteMethod.POST }),
    createWithChannel: (cid: string) => ({
      url: `/text/channel/${cid}/`,
      method: RouteMethod.POST
    }),
    getCustomString: (id: number, cid: string) => ({
      url: `/text/${id}/channel/${cid}`,
      method: RouteMethod.GET
    })
  },
  sursen: {
    getEmployeeIdFromMail: () => ({
      url: "/vacationRequest/id",
      method: RouteMethod.GET
    }),
    getVacationsForEmployee: () => ({
      url: "/vacationRequest",
      method: RouteMethod.GET
    })
  },
  frontend: {
    getSlackUserId: (cid: string, email: string) => ({
      url: `/frontend/channelMember/channel/${cid}/userId?email=${email}`,
      method: RouteMethod.GET
    })
  }
};
// } as Routes;

export default Routes;
