import ITextApi from "./ITextApi";
import IApi from "./IApi";
import DynamicContentContext from "../viewModels/DynamicContentContext";
import TextObj from "../entities/mainDB/TextObj";
import Locale from "../enums/Locale";
import Routes from "./Routes";
import TextType from "../enums/TextType";

export default class TextApi implements ITextApi {
  constructor(private api: IApi, private locale: Locale = Locale.default) { }

  setLocale(locale: Locale) {
    this.locale = locale;
  }

  async get(textType: TextType, context: DynamicContentContext): Promise<string> {
    const textObj = await this.getObj(textType, context);
    return textObj?.text;
  }

  async getObj(textType: TextType, context: DynamicContentContext): Promise<TextObj> {
    const channelId = context.channelId;
    return await this.api.execWithBody<TextObj>(Routes.textType.get(channelId, textType, this.locale), context);
  }
}
