import IApi from "./IApi";
import IRoute from "./IRoute";
import RouteMethod from "./RouteMethod";

export default class Api implements IApi {
  constructor(private domain: string) { }

  private async call<T>(
    url: string,
    queryParams?: Record<string, string>,
    method: string = "GET",
    body: object = {},
    headers: Record<string, string> = {
      Accept: "application/json",
      "Content-Type": "application/json"
    }
  ): Promise<T> {
    let requestInfo = {
      method: method,
      headers: headers
    };

    if (!(Object.keys(body).length === 0 && body.constructor === Object)) {
      const bodyKey = { body: JSON.stringify(body) };
      requestInfo = {
        ...requestInfo,
        ...bodyKey
      };
    }

    /* console.log("requestInfo: ", requestInfo); */

    return await fetch(
      queryParams ? this.domain + url + "?" + new URLSearchParams(queryParams) : this.domain + url,
      requestInfo
    )
      .then(res => {
        return res.json();
      })
      .catch(err => {
        console.error(err);
      });
  }

  public async execWithBody<T>(route: IRoute, body: object, queryParams?: Record<string, string>): Promise<T> {
    switch (route.method) {
      case RouteMethod.GET:
        return this.get<T>(route.url, queryParams);
      case RouteMethod.POST:
        return this.post<T>(route.url, body, queryParams);
      case RouteMethod.PUT:
        return this.put<T>(route.url, body, queryParams);
      case RouteMethod.DELETE:
        return this.delete<T>(route.url, queryParams);
    }
  }

  public async exec<T>(route: IRoute, queryParams?: Record<string, string>): Promise<T> {
    return this.execWithBody<T>(route, {}, queryParams);
  }

  public async get<T>(url: string, queryParams?: Record<string, string>): Promise<T> {
    return await this.call(url, queryParams, "GET", {}, {});
  }

  public async post<T>(url: string, body: object, queryParams?: Record<string, string>): Promise<T> {
    return await this.call(url, queryParams, "POST", body);
  }

  public async put<T>(url: string, body: object, queryParams?: Record<string, string>): Promise<T> {
    return await this.call(url, queryParams, "PUT", body);
  }

  public async delete<T>(url: string, queryParams?: Record<string, string>): Promise<T> {
    return await this.call(url, queryParams, "DELETE", {}, {});
  }
}
