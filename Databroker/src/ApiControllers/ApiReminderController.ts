import * as Koa from "koa";
import ReminderController from "../database/controllers/mainDB/ReminderController";
import { tagsAll, request, summary, body, path } from "koa-swagger-decorator";
import { ReminderSchema } from "../database/entities/mainDB/Reminder";

const reminderController = new ReminderController();

@tagsAll("Reminder")
export default class ApiReminderController {
  @request("delete", "/reminder/channel/{cid}")
  @summary("delete all reminders by channel")
  @path({
    cid: { type: "string", required: true, description: "Slack Channel Id" }
  })
  public static async delete(ctx: Koa.Context) {
    ctx.body = await reminderController.delete(ctx.params.cid);
  }

  @request("get", "/reminder/channel/{cid}")
  @summary("get all reminders")
  @path({
    cid: { type: "string", required: true, description: "Slack Channel Id" }
  })
  public static async getAll(ctx: Koa.Context) {
    ctx.body = await reminderController.getAll(ctx.params.cid);
  }

  @request("get", "/reminder/channel/{cid}/notTriggered")
  @summary("get all not triggered reminders for one channel")
  @path({
    cid: { type: "string", required: true, description: "Slack Channel Id" }
  })
  public static async getAllNotTrigged(ctx: Koa.Context) {
    ctx.body = await reminderController.getAllNotTrigged(ctx.params.cid);
  }

  @request("put", "/reminder/{id}/channel")
  @summary("update trigger date and set haveTriggered to false.")
  @path({
    id: { type: "number", required: true, description: "Id of a reminder" }
  })
  @body({ triggerDate: { type: "Date", required: true, example: new Date() } })
  public static async updateTriggerDate(ctx: Koa.Context) {
    ctx.body = await reminderController.updateTriggerDate(new Date(ctx.request.body.triggerDate), ctx.params.id);
  }

  @request("put", "/reminder/{id}/channel/hasTriggered")
  @summary("update Have Triggered - set the new trigger date and set haveTriggered to false.")
  @path({
    id: { type: "number", required: true, description: "Id of a reminder" }
  })
  @body({ hasTriggered: { type: "boolean", required: true, example: false } })
  public static async updateHasTriggered(ctx: Koa.Context) {
    ctx.body = await reminderController.updateHasTriggered(ctx.request.body.hasTriggered, ctx.params.id);
  }

  @request("delete", "/reminder/{id}/channel")
  @summary("delete reminder by ID")
  @path({
    id: { type: "number", required: true, description: "Id of a reminder" }
  })
  public static async deleteById(ctx: Koa.Context) {
    ctx.body = await reminderController.deleteById(ctx.params.id);
  }

  @request("post", "/channel/reminder/")
  @summary("create one reminder")
  @body(ReminderSchema)
  public static async create(ctx: Koa.Context) {
    ctx.body = await reminderController.create(ctx.request.body);
  }
}
