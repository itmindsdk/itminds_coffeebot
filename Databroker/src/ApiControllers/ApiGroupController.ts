import * as Koa from "koa";
import GroupController from "../database/controllers/mainDB/GroupController";
import { GroupEntity } from "../database/entities/Entities";
import { tagsAll, summary, body, request, path, query } from "koa-swagger-decorator";
import { GroupSchema } from "../database/entities/mainDB/Group";

const groupController = new GroupController();
@tagsAll("Group")
export default class ApiGroupController {
  @request("get", "/Group/all")
  @summary("get a list of all groups")
  public static async getAllGroups(ctx: Koa.Context) {
    ctx.body = await groupController.getAllGroups();
  }

  @request("post", "/Group/")
  @summary("create a group")
  @body(GroupSchema)
  public static async postGroup(ctx: Koa.Context) {
    ctx.body = await groupController.createGroup(ctx.request.body as GroupEntity);
  }

  @request("delete", "/Group/{id}")
  @summary("delete a Group by id")
  @path({
    id: { type: "number", required: true }
  })
  public static async deleteGroup(ctx: Koa.Context) {
    ctx.body = await groupController.deleteGroup(ctx.params.id);
  }

  @request("get", "/Group/{id}")
  @summary("get one group by id")
  @path({
    id: { type: "number", required: true }
  })
  public static async getOneGroup(ctx: Koa.Context) {
    ctx.body = await groupController.getGroup(ctx.params.id);
  }

  @request("get", "/Group/iteration/{iid}")
  @summary("get one group by iteration ID and a thread_ts")
  @path({
    iid: { type: "number", required: true }
  })
  @query({
    thread_ts: { type: "string", required: true, default: "abc" }
  })
  public static async getOneGroupByIterationAndThreadTs(ctx: Koa.Context) {
    const group = await groupController.getGroupByIterationIdAndThreadTs(ctx.params.iid, ctx.request.query.thread_ts);
    console.log("API - getOneGroupByIterationAndThreadTs: Group found:", group);
    ctx.body = group;
  }

  @request("get", "/Group/iteration/{iid}/msg/{msgId}")
  @summary("get one group by iteration ID and a MsgId")
  @path({
    iid: { type: "number", required: true },
    msgId: { type: "string", required: true }
  })
  public static async getGroupByIterationIdAndMsgId(ctx: Koa.Context) {
    const { iid, msgId }: { iid: number, msgId: string } = ctx.params;
    const group = await groupController.getGroupByIterationIdAndMsgId(iid, msgId);
    console.log("API - getGroupByIterationIdAndMsgId: Group found:", group);
    ctx.body = group;
  }

  @request("put", "/Group/{id}")
  @summary("update IsComplete")
  @path({
    id: { type: "number", required: true }
  })
  @body({
    isCompleted: {
      type: "boolean",
      example: true
    }
  })
  public static async updateIsComplete(ctx: Koa.Context) {
    ctx.body = await groupController.updateIsCompleted(ctx.params.id, ctx.request.body.isCompleted);
  }

  @request("put", "/Group/{id}/msg/{msgId}")
  @summary("update MsgId")
  @path({
    id: { type: "number", required: true },
    msgId: { type: "string", required: true }
  })
  public static async updateMsgId(ctx: Koa.Context) {
    const { id, msgId }: { id: number, msgId: string } = ctx.params;
    ctx.body = await groupController.updateMsgId(id, msgId);
  }

  @request("put", "/Group/{id}/threadts/{threadts}")
  @summary("update ThreadTs")
  @path({
    id: { type: "number", required: true },
    threadts: { type: "string", required: true }
  })
  public static async updateThreadTs(ctx: Koa.Context) {
    const { id, threadts }: { id: number, threadts: string } = ctx.params;
    ctx.body = await groupController.updateThreadTs(id, threadts);
  }

  @request("post", "/Group/multiple")
  @summary("create multiple groups")
  @body({
    groups: {
      type: "array",
      required: true,
      items: {
        type: "object",
        properties: GroupSchema
      }
    }
  })
  public static async groupMultiple(ctx: Koa.Context) {
    ctx.body = await groupController.setGroupFromArray(ctx.request.body.groups as GroupEntity[]);
  }

  @request("get", "/Group/msg/{msgId}")
  @summary("get channel id from message id")
  @path({
    msgId: { type: "string", required: true }
  })
  public static async getChannelIdFromMsgId(ctx: Koa.Context) {
    const channelId = await groupController.getChannelIdFromMsgId(ctx.params.msgId);
    console.log("API - getChannelIdFromMsgId:", channelId);
    ctx.body = { channelId };
  }
}
