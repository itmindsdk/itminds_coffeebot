import * as Koa from "koa";
import ChannelController from "../database/controllers/mainDB/ChannelController";
import { ChannelEntity } from "../database/entities/Entities";
import { ChannelSchema } from "../database/entities/mainDB/Channel";
import { request, summary, path, body, responsesAll, tagsAll } from "koa-swagger-decorator";
import e = require("express");

const channelController = new ChannelController();

@tagsAll("Channel")
export default class ApiChannelController {
  @request("get", "/channel/all")
  @summary("Find all channels")
  public static async getAllChannels(ctx: Koa.Context) {
    ctx.body = await channelController.getAllChannels();
  }

  @request("get", "/channel/allActive")
  @summary("Find all active channels")
  public static async getAllActiveChannels(ctx: Koa.Context) {
    ctx.body = await channelController.getAllActiveChannels();
  }

  @request("get", "/channel/allActiveIds")
  @summary("Find all active channels ids")
  public static async getAllActiveChannelsIds(ctx: Koa.Context) {
    ctx.body = await channelController.getAllActiveChannelIds();
  }

  @request("get", "/channel/{id}")
  @summary("Find channel by id")
  @path({
    id: { type: "string", required: true }
  })
  public static async getChannelById(ctx: Koa.Context) {
    ctx.body = await channelController.getChannel(ctx.params.id);
  }

  @request("get", "/channel/{id}/features")
  @summary("Find channel's features by channel id")
  @path({
    id: { type: "string", required: true }
  })
  public static async getChannelFeaturesById(ctx: Koa.Context) {
    ctx.body = await channelController.getChannelFeatures(ctx.params.id);
  }

  @request("post", "/channel")
  @summary("Create a channel")
  @body(ChannelSchema)
  public static async createChannel(ctx: Koa.Context) {
    console.log("body: ", ctx.request.body);

    ctx.body = await channelController.createChannel(ctx.request.body as ChannelEntity);
  }

  @request("delete", "/channel/{id}")
  @summary("Delete a channel")
  @path({
    id: { type: "string", required: true }
  })
  public static async deleteChannel(ctx: Koa.Context) {
    ctx.body = await channelController.deleteChannel(ctx.params.id);
    if (ctx.body.affected === 0) {
      ctx.status = 500;
      ctx.body = { ...ctx.body, status: "No data was affected" };
    }
    ctx.body = { ...ctx.body, status: `Channel ${ctx.params.id} was deleted!` };
  }

  @request("get", "/channel/{id}/eventSchedule")
  @summary("Find a channels eventschedule")
  @path({
    id: { type: "string", required: true }
  })
  public static async getEventSchedule(ctx: Koa.Context) {
    ctx.body = await channelController.getEventSchedule(ctx.params.id);
  }

  @request("get", "/channel/{id}/useThreads")
  @summary("Find a channels useThreads")
  @path({
    id: { type: "string", required: true }
  })
  public static async getUseThreads(ctx: Koa.Context) {
    ctx.body = await channelController.getUseThreads(ctx.params.id);
  }

  @request("get", "/channel/{id}/useReminders")
  @summary("Find a channels useReminders")
  @path({
    id: { type: "string", required: true }
  })
  public static async getUseReminders(ctx: Koa.Context) {
    ctx.body = await channelController.getUseReminders(ctx.params.id);
  }

  @request("get", "/channel/{id}/useCaptain")
  @summary("Find a channels useCaptain")
  @path({
    id: { type: "string", required: true }
  })
  public static async getUseCaptain(ctx: Koa.Context) {
    ctx.body = await channelController.getUseCaptain(ctx.params.id);
  }

  @request("post", "/channel/{id}/nextRunDateAuto")
  @summary("Create a channels nextRunDateAuto")
  @path({
    id: { type: "string", required: true }
  })
  public static async setNextRunDate(ctx: Koa.Context) {
    ctx.body = await channelController.setNextRunDate(ctx.params.id);
  }

  @request("post", "/channel/{id}/nextRunDate")
  @summary("Create a channels nextRunDate")
  @path({
    id: { type: "string", required: true }
  })
  @body({
    nextRunDate: {
      type: "Date",
      required: true,
      example: "1995-12-17T03:24:00"
    }
  })
  public static async setNextRunDateWithDate(ctx: Koa.Context) {
    try {
      const date = await channelController
        .setNextRunDateWithDate(ctx.params.id, new Date(ctx.request.body.nextRunDate));
      ctx.status = 200;
      ctx.body = {
        runDate: date
      };
    } catch (error) {
      ctx.body = {
        errorMessage: error
      };
      ctx.status = 400;
    }
  }

  @request("get", "/channel/{id}/nextRunDate")
  @summary("Find a channels nextRunDate")
  @path({
    id: { type: "string", required: true }
  })
  public static async getNextRunDate(ctx: Koa.Context) {
    ctx.body = await channelController.getNextRunDate(ctx.params.id);
  }

  @request("get", "/channel/{id}/groupSize")
  @summary("Find a channels groupSize")
  @path({
    id: { type: "string", required: true }
  })
  public static async getGroupSize(ctx: Koa.Context) {
    ctx.body = await channelController.getGroupSize(ctx.params.id);
  }

  @request("put", "/channel/{id}/config")
  @summary("Update a channels config")
  @path({
    id: { type: "string", required: true }
  })
  @body({
    useThreads: { type: "boolean", required: true, example: true },
    eventInterval: { type: "number", required: true, example: 3 },
    runDate: {
      type: "Date",
      required: true,
      example: "1995-12-17T03:24:00"
    },
    groupSize: {
      type: "number",
      example: 2
    },
    useCaptain: {
      type: "boolean",
      example: true
    }
  })
  public static async updateConfig(ctx: Koa.Context) {
    const bodyObj = ctx.request.body;
    ctx.body = await channelController.updateConfig(
      ctx.params.id,
      bodyObj.useThreads,
      bodyObj.runDate,
      bodyObj.eventInterval,
      bodyObj.groupSize,
      bodyObj.useCaptain
    );
  }

  @request("put", "/channel/{id}/runDate")
  @summary("Update a channels runDate")
  @path({
    id: { type: "string", required: true }
  })
  @body({
    runDate: {
      type: "Date",
      required: true,
      example: "1995-12-17T03:24:00"
    }
  })
  public static async updateRunDate(ctx: Koa.Context) {
    ctx.body = await channelController.updateRunDate(ctx.params.id, new Date(ctx.request.body.runDate));
  }

  @request("put", "/channel/{id}/active")
  @summary("Update a channels active")
  @path({
    id: { type: "string", required: true }
  })
  @body({
    active: {
      type: "boolean",
      required: true,
      example: false
    }
  })
  public static async updateActive(ctx: Koa.Context) {
    ctx.body = await channelController.updateActive(ctx.params.id, Boolean(ctx.request.body.active));
  }

  @request("put", "/channel/{id}/useReminders")
  @summary("Update useReminders for one channel")
  @path({
    id: { type: "string", required: true }
  })
  @body({
    useReminders: {
      type: "boolean",
      required: true,
      example: false
    }
  })
  public static async updateUseReminders(ctx: Koa.Context) {
    ctx.body = await channelController.updateUseReminders(ctx.params.id, Boolean(ctx.request.body.useReminders));
  }

  @request("put", "/channel/{id}/groupSize")
  @summary("Update a channels groupSize")
  @path({
    id: { type: "string", required: true }
  })
  @body({
    groupSize: {
      type: "number",
      required: true,
      example: 2
    }
  })
  public static async updateGroupSize(ctx: Koa.Context) {
    ctx.body = await channelController.updateGroupSize(ctx.params.id, Number(ctx.request.body.groupSize));
  }

  @request("get", "/channel/{id}/isBotInChannel")
  @summary("Find out if the bot is part of the given channel.")
  @path({
    id: { type: "string", required: true }
  })
  public static async getIsBotInChannel(ctx: Koa.Context) {
    ctx.body = await channelController.isBotInThisChannel(ctx.params.id);
  }

  @request("put", "/channel/{id}/useCoffeeCaptain")
  @summary("Update use Coffee Captain boolean")
  @path({
    id: { type: "string", required: true }
  })
  @body({
    useCaptain: {
      type: "boolean",
      required: true,
      example: false
    }
  })
  public static async updateUseCaptain(ctx: Koa.Context) {
    ctx.body = await channelController.updateActive(ctx.params.id, Boolean(ctx.request.body.useCaptain));
  }
}
