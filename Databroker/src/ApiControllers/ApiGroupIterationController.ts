/* eslint-disable require-atomic-updates */
import * as Koa from "koa";
import { request, summary, body, path, tagsAll } from "koa-swagger-decorator";

import GroupEntity, { GroupSchema } from "../database/entities/mainDB/Group";
import GroupIterationController from "database/controllers/mainDB/GroupIterationController";
import { GroupIterationEntity } from "database/entities/Entities";

const groupIterationController = new GroupIterationController();
@tagsAll("Group Iteration")
export default class ApiGroupIterationController {
  @request("post", "/groupIteration/")
  @summary("create a groupIteration")
  @body(GroupIterationEntity)
  public static async createGroupIteration(ctx: Koa.Context) {
    ctx.body = await groupIterationController.createGroupIteration(ctx.request.body as GroupIterationEntity);
  }

  @request("delete", "/groupIteration/{id}")
  @summary("delete a GroupIteration")
  @path({
    id: { type: "number", required: true }
  })
  public static async deleteGroupIteration(ctx: Koa.Context) {
    ctx.body = await groupIterationController.deleteGroupIteration(ctx.params.id);
  }

  @request("get", "/groupIteration/{id}")
  @summary("get one GroupIteration by id")
  @path({
    id: { type: "number", required: true }
  })
  public static async getGroupIteration(ctx: Koa.Context) {
    ctx.body = await groupIterationController.getGroupIteration(ctx.params.id);
  }

  @request("get", "/groupIteration/channel/{cid}/all")
  @summary("get All Group Iteration By channal id")
  @path({
    cid: { type: "string", required: true }
  })
  public static async getAllGroupIterationByChannel(ctx: Koa.Context) {
    ctx.body = await groupIterationController.getAllGroupIterationByChannel(ctx.params.cid);
  }

  @request("post", "/groupIteration/channel/{cid}")
  @summary("create GroupIteration With Group-Array")
  @path({
    cid: { type: "string", required: true }
  })
  @body({
    groups: {
      type: "array",
      required: true,
      items: {
        type: "object",
        properties: GroupSchema
      }
    }
  })
  public static async createGroupIterationWithGroupArray(ctx: Koa.Context) {
    ctx.body = await groupIterationController.createGroupIterationWithGroupArray(
      ctx.request.body.groups as GroupEntity[],
      ctx.params.cid
    );
  }

  @request("get", "/groupIteration/channel/{cid}")
  @summary("getCurrentIteration")
  @path({
    cid: { type: "string", required: true }
  })
  public static async getCurrentIteration(ctx: Koa.Context) {
    ctx.body = await groupIterationController.getCurrentIteration(ctx.params.cid);
  }

  @request("get", "/groupIteration/channel/{cid}/id")
  @summary("get Current IterationId")
  @path({
    cid: { type: "string", required: true }
  })
  public static async getCurrentIterationId(ctx: Koa.Context) {
    ctx.body = await groupIterationController.getCurrentIterationId(ctx.params.cid);
  }

  @request("put", "/groupIteration/channel/{cid}")
  @summary("add Group To Current Iteration")
  @path({
    cid: { type: "string", required: true }
  })
  @body(GroupSchema)
  public static async addGroupToCurrentIteration(ctx: Koa.Context) {
    ctx.body = await groupIterationController.addGroupToCurrentIteration(ctx.request.body as GroupEntity, ctx.params.cid);
  }

  @request("get", "/groupIteration/channel/{cid}/captains")
  @summary("get All Captains")
  @path({
    cid: { type: "string", required: true }
  })
  public static async getAllCaptains(ctx: Koa.Context) {
    ctx.body = await groupIterationController.getAllCaptains(ctx.params.cid);
  }
}
