import { tagsAll, request, summary, query } from "koa-swagger-decorator";

import { Context } from "koa";
import VacationRequestController from "../database/controllers/sursenDB/VacationRequestController";
import IVacationRequestController from "../database/controllers/sursenDB/interfaces/IVacationRequestController";
const vacationRequestController: IVacationRequestController = new VacationRequestController();

@tagsAll("Vacation Request")
export default class ApiVacationRequestController {
  @request("get", "/vacationRequest/id")
  @summary("Get EmployeeId From Mail")
  @query({
    email: {
      type: "string",
      required: true,
      default: "tlu@it-minds.dk",
      description: "The XXX@it-minds.dk mail of the Employee"
    }
  })
  public static async getEmployeeIdFromMail(ctx: Context) {
    ctx.body = await vacationRequestController.getEmployeeIdFromMail(ctx.request.query.email);
  }

  @request("get", "/vacationRequest")
  @summary("Get list of vacation request's From Mail")
  @query({
    email: {
      type: "string",
      required: true,
      default: "tlu@it-minds.dk",
      description: "The XXX@it-minds.dk mail of the Employee"
    }
  })
  public static async getVacationsForEmployee(ctx: Context) {
    ctx.body = await vacationRequestController.getVacationsForEmployee(ctx.request.query.email);
  }
}
