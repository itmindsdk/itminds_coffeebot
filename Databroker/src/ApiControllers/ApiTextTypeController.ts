import * as Koa from "koa";
import { tagsAll, request, summary, path, body, query, responses, description } from "koa-swagger-decorator";
import TextTypeController from "../database/controllers/mainDB/TextTypeController";
import TextEntity, { TextEntitySchema } from "../database/entities/mainDB/TextEntity";
import DynamicContentHandler from "../utils/dynamicContent/DynamicContentHandler";

const dynamicContentHandler = new DynamicContentHandler();
const textTypeController = new TextTypeController();

@tagsAll("TextType")
export default class ApiTextTypeController {
  @request("delete", "/text/channel/{cid}")
  @summary("Delete one type of text in relation to a channel")
  @path({
    cid: { type: "string", required: true },
  })
  @query({
    textType: { type: "number", required: true },
  })
  public static async delete(ctx: Koa.Context) {
    ctx.body = await textTypeController.delete(ctx.request.query.textType, ctx.params.cid);
  }

  @request("post", "/text/channel/{cid}/{textType}/{locale}")
  @summary(
    "Get one type of text entity in relation to a channel - This endpoint does not work i swagger because swagger does not allow a body to be set when GET is used.",
  )
  @description(
    "This endpoint does not work i swagger because swagger does not allow a body to be set when GET is used.",
  )
  @path({
    cid: { type: "string", required: true, example: "CNNV0AX2N", default: "CNNV0AX2N" },
    textType: { type: "string", required: true, example: "BotJoinText", default: "BotJoinText" },
    locale: { type: "string", request: true, example: "*", default: "*" },
  })
  @body({
    context: {
      type: "object"
    },
  })
  @responses({
    200: { description: "success" },
    400: { description: "This text doesn't exist." },
  })
  public static async get(ctx: Koa.Context) {
    const { cid, textType, locale } = ctx.params;

    const context = ctx.request.body;
    console.log("Dynamic Content Context:", context);
    ctx.body = await dynamicContentHandler
      .get(textType, locale, {
        ...context,
        channelId: cid,
      })
      .then(res => res);
  }

  @request("get", "/text/channel/{cid}/multiple")
  @summary(
    "Get multiple type of text entity in relation to a channel... THIS ENDPOINT DOES NOT WORK FROM SWAGGER (USE POSTMAN)",
  )
  @path({
    cid: { type: "string", required: true },
  })
  @body({
    types: {
      type: "array",
      items: {
        type: "string",
        example: "WelcomeUserText",
      },
    },
  })
  public static async getMultipleById(ctx: Koa.Context) {
    ctx.body = await textTypeController.getMultiple(ctx.request.body.textTypes, ctx.params.cid);
  }

  @request("put", "/text/multiple")
  @summary("Update multiple type of text entities")
  @body({
    textTypes: {
      type: "array",
      required: true,
      items: {
        type: "object",
        properties: TextEntitySchema,
      },
    },
  })
  public static async updateMultiple(ctx: Koa.Context) {
    ctx.body = await textTypeController.updateMultiple(ctx.request.body.textTypes);
  }

  @request("delete", "/text/{id}/channel")
  @summary("Delete custom text by id")
  @path({
    id: { type: "number", required: true },
  })
  public static async deleteById(ctx: Koa.Context) {
    ctx.body = await textTypeController.deleteById(ctx.params.id);
  }

  @request("post", "/text/channel/")
  @summary("Create custom text")
  @body(TextEntitySchema)
  public static async create(ctx: Koa.Context) {
    ctx.body = await textTypeController.create(ctx.request.body);
  }

  @request("post", "/text/channel/{cid}/")
  @summary("Create custom text")
  @path({
    cid: { type: "string", required: true },
  })
  @body(TextEntitySchema)
  public static async createWithChannel(ctx: Koa.Context) {
    const text: TextEntity = ctx.request.body;
    text.channel = ctx.params.cid;
    ctx.body = await textTypeController.create(text);
  }

  @request("get", "/text/{id}/channel/{cid}")
  @summary("get custom text as a string")
  @path({
    cid: { type: "string", required: true },
    id: { type: "number", required: true },
  })
  @responses({
    200: { description: "success" },
    400: { description: "This text doesn't exist, use default instead." },
  })
  public static async getCustomString(ctx: Koa.Context) {
    ctx.body = await textTypeController.getCustomString(ctx.params.id, ctx.params.cid);
  }
}
