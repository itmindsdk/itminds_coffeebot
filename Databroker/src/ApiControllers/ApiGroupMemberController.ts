import * as Koa from "koa";
import GroupMemberController from "../database/controllers/mainDB/GroupMemberController";
import { GroupMemberEntity } from "../database/entities/Entities";
import { tagsAll, request, summary, path, body } from "koa-swagger-decorator";

const groupMemberController = new GroupMemberController();
@tagsAll("Group Member")
export default class ApiGroupMemberController {
  @request("delete", "/groupMember/user/{uid}/group/{gid}")
  @summary("deleteGroupMember")
  @path({
    gid: { type: "number", required: true },
    uid: { type: "string", required: true }
  })
  public static async deleteGroupMember(ctx: Koa.Context) {
    ctx.body = await groupMemberController.deleteGroupMember(ctx.params.gid, ctx.params.uid);
  }

  @request("delete", "/groupMember/{id}")
  @summary("delete GroupMember By Id")
  @path({
    id: { type: "number", required: true }
  })
  public static async deleteGroupMemberById(ctx: Koa.Context) {
    ctx.body = await groupMemberController.deleteGroupMemberById(ctx.params.id);
  }

  @request("post", "/groupMember/")
  @summary("Create GroupMember")
  @body(GroupMemberEntity)
  public static async createGroupMember(ctx: Koa.Context) {
    ctx.body = await groupMemberController.createGroupMember(ctx.request.body as GroupMemberEntity);
  }

  @request("get", "/groupMember/{id}")
  @summary("Get GroupMember By Id")
  @path({
    id: { type: "number", required: true }
  })
  public static async get(ctx: Koa.Context) {
    ctx.body = await groupMemberController.get(ctx.params.id);
  }
}
