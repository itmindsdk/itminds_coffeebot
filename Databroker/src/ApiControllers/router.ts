import Router from "koa-router";

import { SwaggerRouter } from "koa-swagger-decorator";
import ApiChannelController from "./ApiChannelController";
import ApiChannelMemberController from "./ApiChannelMemberController";
import ApiGroupController from "./ApiGroupController";
import ApiFrontendController from "../FrontendControllers/ApiFrontendController";
const swaggerRouter = new SwaggerRouter();

swaggerRouter.swagger({
  title: "Database backend API",
  description: "API DOC",
  version: "1.0.0"
});

swaggerRouter.map(ApiChannelController, []);
swaggerRouter.map(ApiChannelMemberController, []);
swaggerRouter.map(ApiGroupController, []);
swaggerRouter.map(ApiFrontendController, []);

swaggerRouter.mapDir(__dirname);

export default swaggerRouter;
