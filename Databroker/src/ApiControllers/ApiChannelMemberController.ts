import * as Koa from "koa";
import ChannelMemberController from "../database/controllers/mainDB/ChannelMemberController";
import { ChannelMember } from "common";
import { ChannelMemberSchema } from "../database/entities/mainDB/ChannelMember";
import { request, summary, path, body, query, tagsAll } from "koa-swagger-decorator";

const channelMemberController = new ChannelMemberController();

@tagsAll("Channel Member")
export default class ApiChannelMemberController {
  @request("get", "/channelMember/channel/{cid}")
  @summary("Find all channel members based on channel id")
  @path({
    cid: { type: "string", required: true, description: "Slack Channel Id" }
  })
  public static async getChannelMembers(ctx: Koa.Context) {
    ctx.body = await channelMemberController.getChannelMembers(ctx.params.cid);
  }

  @request("get", "/channelMember/channel/{cid}/numberOf")
  @summary("Find number of channel members based on channel id")
  @path({
    cid: { type: "string", required: true }
  })
  public static async getNumberOfChannelMembers(ctx: Koa.Context) {
    ctx.body = await channelMemberController.getNumberOfChannelMembers(ctx.params.cid);
  }

  @request("delete", "/channelMember/channel/{cid}/user/{uid}")
  @summary("Delete channel members based on channel id")
  @path({
    cid: { type: "string", required: true, description: "Slack Channel Id" },
    uid: { type: "string", required: true, description: "Slack User Id" }
  })
  public static async deleteChannelMember(ctx: Koa.Context) {
    ctx.body = await channelMemberController.deleteChannelMember(ctx.params.cid, ctx.params.uid);
  }

  @request("delete", "/channelMember/channel/{cid}")
  @summary("Delete channel members based on channel id")
  @path({
    cid: { type: "string", required: true, description: "Slack Channel Id" }
  })
  public static async deleteAllChannelMembers(ctx: Koa.Context) {
    ctx.body = await channelMemberController.deleteAllChannelMembers(ctx.params.cid);
  }

  @request("delete", "/channelMember/{id}")
  @summary("Delete channel members based on id")
  @path({
    id: { type: "string", required: true }
  })
  public static async deleteChannelMemberById(ctx: Koa.Context) {
    ctx.body = await channelMemberController.deleteChannelMemberById(ctx.params.id);
  }

  //TODO TEST OM QUERY SKAL VÆRE I URL
  @request("put", "/channelMember/channel/{cid}/user/{uid}")
  @summary("Update channel members based on channel id & user id")
  @path({
    cid: { type: "string", required: true, description: "Slack Channel Id" },
    uid: { type: "string", required: true, description: "Slack User Id" }
  })
  @query({
    skipRounds: { type: "number", required: true, default: 1 }
  })
  public static async updateChannelMember(ctx: Koa.Context) {
    ctx.body = await channelMemberController.updateChannelMember(
      ctx.params.cid,
      ctx.params.uid,
      ctx.request.query.skipRounds
    );
  }

  //TODO TEST OM QUERY SKAL VÆRE I URL
  @request("put", "/channelMember/{id}")
  @summary("Update channel members based on id")
  @path({
    id: { type: "string", required: true, description: "Id of a ChannelMember" }
  })
  @query({
    skipRounds: { type: "number", required: true, default: 1 }
  })
  public static async updateChannelMemberById(ctx: Koa.Context) {
    ctx.body = await channelMemberController.updateChannelMemberById(ctx.params.id, ctx.request.query.skipRounds);
  }

  @request("post", "/channelMember")
  @summary("Create one or more channel members. Either send single channel member object or array of channel member objects.")
  @body(ChannelMemberSchema)
  public static async createChannelMember(ctx: Koa.Context) {
    if (Array.isArray(ctx.request.body)) {
      const channelMembers = ctx.request.body as ChannelMember[];
      ctx.body = await channelMemberController.createChannelMembers(channelMembers);
    } else {
      const channelMember = ctx.request.body as ChannelMember;
      ctx.body = await channelMemberController.createChannelMember(channelMember);
    }
  }

  @request("get", "/frontend/channelMember/channel/{cid}/userId")
  @summary("get slack user id based on channel ID & user email")
  @path({
    cid: { type: "string", required: true }
  })
  @query({
    email: { type: "string", required: true }
  })
  public static async getSlackUserId(ctx: Koa.Context) {
    ctx.body = await channelMemberController.getUserId(ctx.params.cid, ctx.request.query.email);
  }
}
