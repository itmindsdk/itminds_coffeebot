import "./loadEnv"; // this MUST be the topmost import
import "reflect-metadata";
import { createConnections } from "typeorm";
import swaggerRouter from "./ApiControllers/router";
import Koa from "koa";
import logger from "koa-logger";
import Router from "koa-router";
import koaCompress from "koa-compress";
import bodyParser from "koa-bodyparser";
import cors from "@koa/cors";

const port = process.env.PORT;

const app = new Koa();
const router = new Router();
const apiRouterOpts: Router.IRouterOptions = { prefix: "/api" };
const apiRouter = new Router(apiRouterOpts);

// setup test routes
router.get("/health", async (ctx, next) => {
  ctx.body = `
    <h1>I am healthy!</h1>
    <h2>Databroker info:</h2>
    <table>
    <tr>
      <td>Port</td>
      <td>${port}</td>
    </tr>
    <tr>
      <td>Swagger URL</td>
      <td><a href="/swagger-html">Go to Swagger</a></td>
    </tr>
  </table>
  `
  await next();
});

app.use(logger());
app.use(cors());
app.use(koaCompress());
app.use(bodyParser());
app.use(router.routes()).use(router.allowedMethods());
app.use(apiRouter.routes()).use(apiRouter.allowedMethods());

// Route middleware.
app.use(swaggerRouter.routes());

app.listen(port, () => {
  console.log("Koa server started", "http://localhost:" + port + "/swagger-html");
  console.log("Connecting to DB's..........");
  createConnections()
    .then(async res => {
      process.stdout.write("Main DB connnected:  ");
      console.log(res[0].isConnected);
      process.stdout.write("Sursen DB connected: ");
      console.log(res[1].isConnected);
    })
    .catch(ex => {
      console.log(ex);
    });
});
