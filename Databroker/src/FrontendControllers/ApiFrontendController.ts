import * as Koa from "koa";
import { inject } from "tsyringe";
import ApiChannelController from "../ApiControllers/ApiChannelController";
import { ChannelMemberController } from "../database/controllers/Controllers";
import ApiTextTypeController from "../ApiControllers/ApiTextTypeController";
import { request, summary, path, body, tagsAll, query } from "koa-swagger-decorator";

import { TextEntitySchema } from "../database/entities/mainDB/TextEntity";
import { IApi, Routes, Api } from "common";

const channelMemberController = new ChannelMemberController();

@tagsAll("Frontend")
export default class ApiFrontendController {
  private api: IApi;
  constructor() {
    this.api = new Api(process.env.URL);
  }

  @request("get", "/frontend/channel/all")
  @summary("Find all channels")
  public static async getAllChannels(ctx: Koa.Context) {
    return ApiChannelController.getAllChannels(ctx);
  }

  @request("get", "/frontend/channel/{id}/features")
  @summary("Find all of a channel's features")
  @path({
    id: { type: "string", required: true }
  })
  public static async getChannelsFeautures(ctx: Koa.Context) {
    return ApiChannelController.getChannelFeaturesById(ctx);
  }

  @request("get", "/frontend/text/channel/{cid}/multiple")
  @summary(
    "Get multiple type of text entity in relation to a channel... THIS ENDPOINT DOES NOT WORK FROM SWAGGER (USE POSTMAN)"
  )
  @path({
    cid: { type: "string", required: true }
  })
  @body({
    types: {
      type: "array",
      items: {
        type: "number",
        example: 0
      }
    }
  })
  public static async getMultipleById(ctx: Koa.Context) {
    ctx.body = await ApiTextTypeController.getMultipleById(ctx);
  }

  @request("put", "/frontend/text/multiple")
  @summary("Update multiple type of text entities")
  @body({
    textTypes: {
      type: "array",
      required: true,
      items: {
        type: "object",
        properties: TextEntitySchema
      }
    }
  })
  public static async updateMultiple(ctx: Koa.Context) {
    ctx.body = await ApiTextTypeController.updateMultiple(ctx);
  }

  @request("get", "/frontend/channelMember/userId")
  @summary("get slack user id based on only user email")
  @query({
    email: {
      type: "string",
      required: true,
      default: "tlu@it-minds.dk",
      description: "The XXX@it-minds.dk mail of the Employee"
    }
  })
  public async getSlackUserInfoWithEmail(ctx: Koa.Context): Promise<String> {
    const string = await channelMemberController.getUserIdWithEmail(ctx.request.query.email);
    console.log(string);
    return string;
  }
}
