export default interface IVacationRequestController {
  getVacationsForEmployee(mail: String);

  getEmployeeIdFromMail(mail: String): Promise<number>;
}
