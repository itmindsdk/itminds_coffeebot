import { getConnection, Repository, Brackets } from "typeorm";
import IVacationRequestController from "./interfaces/IVacationRequestController";
import { Status } from "common";
import { VacationRequestEntity, EmployeeEntity } from "database/entities/Entities";

export default class VacationRequestController implements IVacationRequestController {
  private static readonly dbKey = "Sursen";

  private getRepository(): Repository<VacationRequestEntity> {
    return getConnection(VacationRequestController.dbKey).getRepository(VacationRequestEntity);
  }

  public async getVacationsForEmployee(mail: String) {
    const employeeId = await this.getEmployeeIdFromMail(mail);
    if (!employeeId) {
      return [];
    }
    const vacations = await this.getRepository()
      .find({
        employeeId: employeeId,
        status: Status.ACCEPTED,
        statusEnumValue: Status.ACCEPTED
      });
    return vacations;
  }

  public async getEmployeeIdFromMail(mail: String): Promise<number> {
    /* 	const person = await getConnection(VacationRequestController.dbKey).getRepository(Employee).findOne({ where: { mail }, select: ["Id"] }); */
    const person = await getConnection(VacationRequestController.dbKey)
      .getRepository(EmployeeEntity)
      .createQueryBuilder("emp")
      .where("emp.mail = :mail", { mail })
      .getOne();
    return person?.id ?? null;
  }
}
