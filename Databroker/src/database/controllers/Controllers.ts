/* MAIN DB */
import IChannelController from "./mainDB/interfaces/IChannelController";
import IChannelMemberController from "./mainDB/interfaces/IChannelMemberController";
import IGroupController from "./mainDB/interfaces/IGroupController";
import IGroupIterationController from "./mainDB/interfaces/IGroupIterationController";
import IGroupMemberController from "./mainDB/interfaces/IGroupMemberController";
import IReminderController from "./mainDB/interfaces/IReminderController";
import ITextTypeController from "./mainDB/interfaces/ITextTypeController";

import ChannelController from "./mainDB/ChannelController";
import ChannelMemberController from "./mainDB/ChannelMemberController";
import GroupController from "./mainDB/GroupController";
import GroupMemberController from "./mainDB/GroupMemberController";
import ReminderController from "./mainDB/ReminderController";
import TextTypeController from "./mainDB/TextTypeController";
/* import GroupIterationController from "./mainDB/GroupIterationController"; */

export {
  IChannelController,
  IChannelMemberController,
  IGroupController,
  IGroupIterationController,
  IGroupMemberController,
  IReminderController,
  ITextTypeController,
  ChannelController,
  ChannelMemberController,
  GroupController,
  /*   GroupIterationController, */
  GroupMemberController,
  ReminderController,
  TextTypeController
};

/* SURSEN DB */
import IVacationRequestController from "./sursenDB/interfaces/IVacationRequestController";

import VacationRequestController from "./sursenDB/VacationRequestController";

export { IVacationRequestController, VacationRequestController };
