import { getConnection, Repository } from "typeorm";
import IGroupIterationController from "./interfaces/IGroupIterationController";
import { GroupIterationEntity, GroupEntity } from "../../entities/Entities";
import { GroupIteration, Group } from "common";

export default class GroupIterationController
  implements IGroupIterationController {
  private getRepository(): Repository<GroupIterationEntity> {
    return getConnection().getRepository(GroupIterationEntity);
  }

  public createGroupIteration(groupIteration: GroupIteration) {
    return this.getRepository().save(groupIteration);
  }

  public deleteGroupIteration(groupIterationId: number) {
    return this.getRepository().delete({ id: groupIterationId });
  }

  public getGroupIteration(groupIterationId: number): Promise<GroupIteration> {
    return this.getRepository().findOneOrFail({ id: groupIterationId });
  }

  public getAllGroupIterationByChannel(
    channelId: string
  ): Promise<GroupIteration[]> {
    return this.getRepository().find({ channelId });
  }

  public async createGroupIterationWithGroupArray(
    groups: Group[],
    channelId: string
  ): Promise<GroupIteration> {
    const groupIteration: GroupIterationEntity = new GroupIterationEntity();
    groupIteration.groups = groups;
    groupIteration.channelId = channelId;
    return await this.createGroupIteration(groupIteration);
  }

  public async getCurrentIteration(channelId: string): Promise<GroupIteration> {
    const query = getConnection()
      .getRepository(GroupIterationEntity)
      .createQueryBuilder("iteration")
      .where("iteration.channelId = :id", { id: channelId });

    const iteration = await query.execute();

    const queryRes = await query.select("MAX(iteration.id)", "max").getRawOne();
    const id: number = queryRes.max;

    const res = await getConnection()
      .getRepository(GroupEntity)
      .find({ where: [{ groupIteration: id }], relations: ["members"] });

    const queryGroupIteration: any = await getConnection()
      .getRepository(GroupIterationEntity)
      .findOne({ where: [{ id }], relations: ["channel"] });

    if (iteration.length > 0) {
      const groupIteration = new GroupIterationEntity();
      groupIteration.channel = queryGroupIteration.channel.id;
      groupIteration.groups = res;
      groupIteration.id = id;

      return groupIteration;
    }
    return null;
  }

  public async getCurrentIterationId(channel: string): Promise<number> {
    const data = await getConnection()
      .getRepository(GroupIterationEntity)
      .createQueryBuilder("iteration")
      .where("iteration.channel = :id", { id: channel })
      .select("MAX(iteration.id)", "max")
      .getRawOne();
    return data.max;
  }

  public async addGroupToCurrentIteration(
    newGroup: Group,
    channelId: string
  ): Promise<Group> {
    newGroup.groupIterationId = await this.getCurrentIterationId(channelId);
    return getConnection().manager.save(GroupEntity, newGroup);
  }

  public async getAllCaptains(channel: string): Promise<string[]> {
    const id: number = await this.getCurrentIterationId(channel);
    const groups: GroupEntity[] = await getConnection()
      .getRepository(GroupEntity)
      .find({ where: { groupIteration: id }, relations: ["members"] });
    const captainsarray: string[] = [];
    groups.forEach(group => {
      captainsarray.push(group.members[group.captain].userId);
    });
    return captainsarray;
  }
}
