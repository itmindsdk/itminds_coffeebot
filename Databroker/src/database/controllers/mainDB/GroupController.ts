import { getConnection, Repository, UpdateResult, InsertResult } from "typeorm";
import IGroupController from "./interfaces/IGroupController";
import { GroupEntity } from "../../entities/Entities";
import { Group } from "common";

export default class GroupController implements IGroupController {
  private getRepository(): Repository<GroupEntity> {
    return getConnection().getRepository(GroupEntity);
  }

  public getAllGroups(): Promise<Group[]> {
    return this.getRepository().find({});
  }

  public createGroup(group: Group) {
    return this.getRepository().save(group);
  }

  public deleteGroup(groupId: number) {
    return this.getRepository().delete({ id: groupId });
  }

  public getGroup(groupId: number): Promise<Group> {
    return this.getRepository().findOneOrFail({ where: { id: groupId }, relations: ["members"] });
  }

  public getGroupByIterationIdAndThreadTs(
    groupIterationId: number,
    thread_ts: string
  ): Promise<Group> {
    return this.getRepository().findOneOrFail({
      where: {
        groupIterationId,
        thread_ts
      },
      relations: ["members"]
    });
  }

  public getGroupByIterationIdAndMsgId(
    groupIterationId: number,
    msgId: string
  ): Promise<Group> {
    return this.getRepository().findOneOrFail({
      where: {
        groupIterationId,
        msgId
      },
      relations: ["members"]
    });
  }

  public updateIsCompleted(
    groupId: number,
    isCompleted: boolean
  ): Promise<UpdateResult> {
    return this.getRepository().update({ id: groupId }, { isCompleted });
  }

  public updateMsgId(groupId: number, msgId: string) {
    return this.getRepository().update({ id: groupId }, { msgId });
  }

  public updateThreadTs(groupId: number, threadTs: string) {
    return this.getRepository().update(
      { id: groupId },
      { thread_ts: threadTs }
    );
  }

  public async setGroupFromArray(groups: Group[]): Promise<boolean> {
    return getConnection().getRepository(GroupEntity).insert(groups).then(() => true);
  }

  public async getChannelIdFromMsgId(msgId: string): Promise<string> {
    const group = await this.getRepository().findOne({ where: { msgId }, relations: ["groupIteration"] });
    console.log("TEST:", group, group?.groupIterationId, group?.groupIteration, group?.groupIteration?.channelId);
    return group?.groupIteration?.channelId;
  }
}
