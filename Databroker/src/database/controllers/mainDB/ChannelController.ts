import { getConnection, UpdateResult, DeleteResult, Repository } from "typeorm";
import ChannelEntity from "../../entities/mainDB/Channel";
import GroupEntity from "../../entities/mainDB/Group";
import { IChannelFeatures, Channel, ChannelMember, GroupIteration, Group } from "common";
import IChannelController from "./interfaces/IChannelController";
import { Schedule } from "common";
import GroupIterationEntity from "../../entities/mainDB/GroupIteration";
import ChannelMemberEntity from "../../entities/mainDB/ChannelMember";

/**
 * Controls all access to the database concerning Channels.
 * @implements IChannelController
 */
export default class ChannelController implements IChannelController {
  private getRepository(): Repository<ChannelEntity> {
    return getConnection().getRepository(ChannelEntity);
  }

  public getAllChannels(): Promise<Channel[]> {
    return this.getRepository().find({}).then(ce => ce as Channel[]);
  }

  public getAllActiveChannels(): Promise<Channel[]> {
    return this.getRepository().find({
      where: { active: true }
    });
  }

  public getAllActiveChannelIds(): Promise<Channel[]> {
    return this.getRepository().find({
      where: { active: true },
      select: ["id"]
    });
  }

  public createChannel(channel: Channel): Promise<Channel> {
    return this.getRepository().save(channel);
  }

  public deleteChannel(id: string): Promise<DeleteResult> {
    return this.getRepository().delete({ id });
  }

  public getChannel(id: string): Promise<Channel> {
    return this.getRepository().findOneOrFail({ id });
  }
  public getChannelFeatures(id: string): Promise<IChannelFeatures> {
    return this.getRepository().findOneOrFail({
      where: { id: id },
      select: ["useThreads", "useReminders", "useCaptain"]
    });
  }

  public async getEventSchedule(id: string): Promise<Schedule> {
    const channel: ChannelEntity = await this.getRepository().findOneOrFail({ id }, { select: ["runDate", "eventInterval"] });

    const res: Schedule = {
      runDate: channel.runDate,
      eventInterval: channel.eventInterval
    };

    return res;
  }

  public async getUseThreads(id: string): Promise<boolean> {
    const channel: ChannelEntity = await this.getRepository().findOneOrFail({ id }, { select: ["useThreads"] });
    return channel.useThreads;
  }

  public async getUseReminders(id: string): Promise<boolean> {
    const channel: ChannelEntity = await this.getRepository().findOneOrFail({ id }, { select: ["useReminders"] });
    return channel.useReminders;
  }

  public async getUseCaptain(id: string): Promise<boolean> {
    const channel: ChannelEntity = await this.getRepository().findOneOrFail({ id }, { select: ["useCaptain"] });
    return channel.useCaptain;
  }

  public async setNextRunDate(id: string): Promise<Date> {
    const schedule = await this.getEventSchedule(id);
    await this.getRepository().update({ id }, { runDate: schedule.runDate });
    return schedule.runDate;
  }

  public async setNextRunDateWithDate(id: string, newDate: Date): Promise<Date> {
    const updateResult = await this.getRepository().update({ id }, { runDate: newDate });
    if (updateResult.affected > 0) {
      return Promise.resolve(newDate);
    }
    console.error(updateResult.raw);
    return Promise.reject("Couldn't update the run date.");
  }

  public async getNextRunDate(id: string): Promise<Date> {
    const channel: ChannelEntity = await this.getRepository().findOneOrFail({ id }, { select: ["runDate"] });
    return channel.runDate;
  }

  public async getGroupSize(id: string): Promise<number> {
    const channel: ChannelEntity = await this.getRepository().findOneOrFail({ id }, { select: ["groupSize"] });
    return channel.groupSize;
  }

  public updateConfig(
    id: string,
    useThreads: boolean,
    runDate: Date,
    eventInterval: number,
    groupSize: number,
    useCaptain: boolean
  ): Promise<UpdateResult> {
    return this.getRepository().update(
      { id },
      {
        runDate,
        useThreads,
        eventInterval,
        groupSize,
        useCaptain
      }
    );
  }

  public updateRunDate(id: string, newDate: Date): Promise<UpdateResult> {
    return this.getRepository().update({ id }, { runDate: newDate });
  }

  public updateActive(id: string, active: boolean): Promise<UpdateResult> {
    return this.getRepository().update({ id }, { active });
  }

  public updateUseReminders(id: string, useReminders: boolean): Promise<UpdateResult> {
    return this.getRepository().update({ id }, { useReminders });
  }

  public updateGroupSize(id: string, groupSize: number): Promise<UpdateResult> {
    return this.getRepository().update({ id }, { groupSize });
  }

  public updateThreadedMsg(id: string, useThreads: boolean): Promise<UpdateResult> {
    return this.getRepository().update({ id }, { useThreads });
  }

  public updateEventInterval(id: string, eventInterval: number): Promise<UpdateResult> {
    return this.getRepository().update({ id }, { eventInterval });
  }

  public addChannelMembers(members: ChannelMember[]): void {
    members.forEach(element => {
      getConnection().manager.save(ChannelMemberEntity, element);
    });
  }

  public updateChannelMembers(members: ChannelMember[]): void {
    const channelId = members[0].id;

    getConnection()
      .getRepository(ChannelMemberEntity)
      .delete({ id: channelId }); // delete all useres for this channel.

    members.forEach(element => {
      getConnection().manager.save(ChannelMemberEntity, element);
    });
  }

  public async addGroupIteration(newGroupIteration: GroupIteration, channelId?: string): Promise<GroupIteration> {
    if (channelId) {
      const channel = await this.getChannel(channelId);
      newGroupIteration.channel = channel;
    }
    return getConnection().manager.save(GroupIterationEntity, newGroupIteration);
  }

  public async addGroup(newGroup: Group, groupIterationId?: number): Promise<Group> {
    if (groupIterationId) {
      const groupIteration = await getConnection()
        .getRepository(GroupIterationEntity).findOne({ id: groupIterationId });
      newGroup.groupIteration = groupIteration;
    }
    return getConnection().manager.save(GroupEntity, newGroup);
  }

  public async isBotInThisChannel(id: string): Promise<boolean> {
    const channel = await this.getRepository().findOne({
      where: { id },
      select: ["id", "active"]
    });
    try {
      if (channel.id === id && channel.active === true) {
        return true;
      }
      return false;
    } catch {
      return false;
    }
  }
}
