import { DeleteResult, getConnection, Repository } from "typeorm";
import IGroupMemberController from "./interfaces/IGroupMemberController";
import { GroupMember } from "common";
import { GroupMemberEntity } from "database/entities/Entities";

export default class GroupMemberController implements IGroupMemberController {
  private getRepository(): Repository<GroupMemberEntity> {
    return getConnection().getRepository(GroupMemberEntity);
  }

  public deleteGroupMember(
    groupId: number,
    userId: string
  ): Promise<DeleteResult> {
    return this.getRepository().delete({ groupId, userId });
  }

  public deleteGroupMemberById(id: number): Promise<DeleteResult> {
    return this.getRepository().delete({ id });
  }

  public createGroupMember(newMember: GroupMember): Promise<GroupMember> {
    const memberEntity = { ...newMember } as GroupMemberEntity;
    return this.getRepository().save(memberEntity).then(gme => gme as GroupMember);
  }

  public get(id: number) {
    return this.getRepository().find({ id });
  }
}
