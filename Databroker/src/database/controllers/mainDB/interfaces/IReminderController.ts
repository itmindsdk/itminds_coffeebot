import { DeleteResult, UpdateResult } from "typeorm";
import { Reminder } from "common";

export default interface IReminderController {
  delete(channelId: string): Promise<DeleteResult>;

  getAll(channel: string): Promise<Reminder[]>;

  deleteById(id: number): Promise<DeleteResult>;

  create(entity: Reminder): Promise<Reminder>;

  updateHasTriggered(haveTriggered: boolean, id: number): Promise<UpdateResult>;

  getAllNotTrigged(channel: string): Promise<Reminder[]>;

  /**
   * set the new trigger date and set haveTriggered to false.
   */
  updateTriggerDate(triggerDate: Date, id: number): Promise<UpdateResult>;
}
