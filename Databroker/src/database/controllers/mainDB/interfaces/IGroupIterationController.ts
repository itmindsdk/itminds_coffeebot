import { DeleteResult } from "typeorm";
import { GroupIteration, Group } from "common";

export default interface IGroupIterationController {
  createGroupIteration(groupIteration: GroupIteration): Promise<GroupIteration>;

  deleteGroupIteration(groupIterationId: number): Promise<DeleteResult>;

  getGroupIteration(groupIterationId: number): Promise<GroupIteration>;

  getAllGroupIterationByChannel(channel: string): Promise<GroupIteration[]>;

  createGroupIterationWithGroupArray(groups: Group[], channel: string): Promise<GroupIteration>;

  getCurrentIteration(channel: string): Promise<GroupIteration>;

  getCurrentIterationId(channel: string): Promise<number>;

  addGroupToCurrentIteration(newGroup: Group, channelId: string): Promise<Group>;

  getAllCaptains(channel: string): Promise<string[]>;
}
