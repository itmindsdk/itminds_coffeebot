import { DeleteResult, UpdateResult } from "typeorm";
import { ChannelMember } from "common";

export default interface IChannelMemberController {
  getChannelMembers(channel: string): Promise<ChannelMember[]>;

  getNumberOfChannelMembers(channel: string): Promise<[ChannelMember[], number]>;

  deleteChannelMember(channel: string, userId: string): Promise<DeleteResult>;

  deleteAllChannelMembers(channel: string): Promise<DeleteResult>;

  deleteChannelMemberById(id: number): Promise<DeleteResult>;

  updateChannelMember(channel: string, userId: string, skipRounds: number): Promise<UpdateResult>;

  updateChannelMemberById(id: number, skipRounds: number): Promise<UpdateResult>;

  createChannelMember(newMember: ChannelMember): Promise<ChannelMember>;

  createChannelMembers(newMembers: ChannelMember[]): Promise<ChannelMember[]>;
}
