import { DeleteResult, UpdateResult } from "typeorm";
import { Group } from "common";

export default interface IGroupController {
  getAllGroups(): Promise<Group[]>;

  createGroup(group: Group): Promise<Group>;

  deleteGroup(groupId: number): Promise<DeleteResult>;

  getGroup(groupId: number): Promise<Group>;

  getGroupByIterationIdAndThreadTs(groupIterationId: number, thread_ts: string): Promise<Group>;

  getGroupByIterationIdAndMsgId(groupIterationId: number, msgId: string): Promise<Group>;

  updateIsCompleted(groupId: number, isCompleted: boolean): Promise<UpdateResult>;

  updateMsgId(groupId: number, msgId: string): Promise<UpdateResult>;

  setGroupFromArray(groups: Group[]): void;

  getChannelIdFromMsgId(msgId: string): Promise<string>;
}
