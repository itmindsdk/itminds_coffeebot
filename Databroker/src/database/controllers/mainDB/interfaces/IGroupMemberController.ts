import { DeleteResult } from "typeorm";
import { GroupMember } from "common";

export default interface IGroupMemberController {
  deleteGroupMember(groupId: number, userId: string): Promise<DeleteResult>;

  deleteGroupMemberById(id: number): Promise<DeleteResult>;

  createGroupMember(newMember: GroupMember): Promise<GroupMember>;
}
