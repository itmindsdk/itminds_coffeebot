import { UpdateResult, DeleteResult } from "typeorm";
import { Channel, ChannelMember, GroupIteration, Group, Schedule } from "common";

export default interface IChannelController {
  getAllChannels(): Promise<Channel[]>;

  getAllActiveChannels(): Promise<Channel[]>;

  getAllActiveChannelIds(): Promise<Channel[]>;

  createChannel(channel: Channel): Promise<Channel>;

  deleteChannel(id: string): Promise<DeleteResult>;

  getChannel(id: string): Promise<Channel>;

  getEventSchedule(id: string): Promise<Schedule>;

  getUseThreads(id: string): Promise<boolean>;

  getUseCaptain(id: string): Promise<boolean>;

  setNextRunDate(id: string): Promise<Date>;

  setNextRunDateWithDate(id: string, newDate: Date): void;

  getNextRunDate(id: string): Promise<Date>;

  getGroupSize(id: string): Promise<number>;

  updateConfig(
    id: string,
    useThreads: boolean,
    runDate: Date,
    eventInterval: number,
    groupSize: number,
    useCaptain: boolean
  ): Promise<UpdateResult>;

  updateRunDate(id: string, newDate: Date): Promise<UpdateResult>;

  updateActive(id: string, active: boolean): Promise<UpdateResult>;

  updateGroupSize(id: string, groupSize: number): Promise<UpdateResult>;

  updateThreadedMsg(id: string, useThreads: boolean): Promise<UpdateResult>;

  updateEventInterval(id: string, eventInterval: number): Promise<UpdateResult>;

  addChannelMembers(members: ChannelMember[]): void;

  updateChannelMembers(members: ChannelMember[]): void;

  addGroupIteration(newGroupIteration: GroupIteration, channel?: string): Promise<GroupIteration>;

  addGroup(newGroup: Group, groupIterationId?: number): Promise<Group>;

  isBotInThisChannel(id: string): Promise<boolean>;

  getUseReminders(id: string): Promise<boolean>;
}
