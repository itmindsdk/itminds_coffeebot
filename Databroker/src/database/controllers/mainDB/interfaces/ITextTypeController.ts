import { DeleteResult } from "typeorm";
import { TextObj, TextType } from "common";

export default interface ITextTypeController {
  delete(type: number, channelId: string): Promise<DeleteResult>;

  get(textType: TextType, channelId: string): Promise<TextObj>;

  deleteById(id: number): Promise<DeleteResult>;

  create(entity: TextObj): Promise<TextObj>;

  getCustomString(textType: TextType, channel: string): Promise<string | undefined>;
}
