import { DeleteResult, getConnection, Repository, UpdateResult } from "typeorm";
import IReminderController from "./interfaces/IReminderController";
import { ReminderEntity } from "../../entities/Entities";
import { Reminder } from "common";

export default class ReminderController implements IReminderController {
  private getRepository(): Repository<ReminderEntity> {
    return getConnection().getRepository(ReminderEntity);
  }

  public delete(channelId: string): Promise<DeleteResult> {
    return this.getRepository().delete({ channel: channelId });
  }

  public getAll(channel: string): Promise<Reminder[]> {
    return this.getRepository().find({
      where: {
        channel
      },
      relations: ["channel"]
    });
  }

  public getAllNotTrigged(channel: string): Promise<Reminder[]> {
    return this.getRepository().find({
      where: {
        channel,
        haveTriggered: false
      },
      relations: ["channel"]
    });
  }

  public updateHasTriggered(
    haveTriggered: boolean,
    id: number
  ): Promise<UpdateResult> {
    return this.getRepository().update({ id }, { haveTriggered });
  }

	/**
	 * set the new trigger date and set haveTriggered to false.
	 */
  public updateTriggerDate(
    triggerDate: Date,
    id: number
  ): Promise<UpdateResult> {
    return this.getRepository().update(
      { id },
      { triggerDate, haveTriggered: false }
    );
  }

  public deleteById(id: number): Promise<DeleteResult> {
    return this.getRepository().delete({ id });
  }

  public create(entity: Reminder): Promise<Reminder> {
    return this.getRepository().save(entity);
  }
}
