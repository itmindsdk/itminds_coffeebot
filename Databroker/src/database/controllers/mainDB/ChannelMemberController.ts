import { getConnection, DeleteResult, UpdateResult, Repository } from "typeorm";
import IChannelMemberController from "./interfaces/IChannelMemberController";
import { ChannelMemberEntity } from "../../entities/Entities";
import { ChannelMember } from "common";

export default class ChannelMemberController implements IChannelMemberController {
  private getRepository(): Repository<ChannelMemberEntity> {
    return getConnection().getRepository(ChannelMemberEntity);
  }

  public getChannelMembers(channel: string): Promise<ChannelMember[]> {
    return this.getRepository().find({ channel });
  }

  public getNumberOfChannelMembers(channel: string) {
    return this.getRepository().findAndCount({ channel });
  }

  public deleteChannelMember(channel: string, userId: string): Promise<DeleteResult> {
    return this.getRepository().delete({ channel, userId });
  }

  public deleteAllChannelMembers(channel: string): Promise<DeleteResult> {
    return this.getRepository().delete({ channel });
  }

  public deleteChannelMemberById(id: number): Promise<DeleteResult> {
    return this.getRepository().delete({ id });
  }

  public updateChannelMember(channel: string, userId: string, skipRounds: number): Promise<UpdateResult> {
    return this.getRepository().update({ channel, userId }, { skipRounds });
  }

  public updateChannelMemberById(id: number, skipRounds: number): Promise<UpdateResult> {
    return this.getRepository().update({ id }, { skipRounds });
  }

  public createChannelMember(newMember: ChannelMember): Promise<ChannelMember> {
    return this.getRepository().save(newMember);
  }

  public createChannelMembers(newMembers: ChannelMember[]): Promise<ChannelMember[]> {
    return this.getRepository().save(newMembers);
  }

  public async getUserId(channelId?: string, email?: string): Promise<string> {
    const channelMember: ChannelMemberEntity = await this.getRepository().findOneOrFail({
      where: { channel: channelId, mail: email },
      select: ["userId"]
    });
    return channelMember.userId;
  }

  public async getUserIdWithEmail(email?: string): Promise<string> {
    const channelMember: ChannelMemberEntity = await this.getRepository().findOneOrFail({
      where: { mail: email },
      select: ["userId"]
    });
    return channelMember.userId;
  }
}
