import { DeleteResult, getConnection, Repository, In, UpdateResult, FindConditions } from "typeorm";
import TextEntity from "../../entities/mainDB/TextEntity";
import { TextType, TextObj } from "common";
import ITextTypeController from "./interfaces/ITextTypeController";

export default class TextTypeController implements ITextTypeController {
  private getRepository(): Repository<TextEntity> {
    return getConnection().getRepository(TextEntity);
  }

  public delete(type: number, channelId: string): Promise<DeleteResult> {
    return this.getRepository().delete({
      textType: Number(type),
      channel: channelId
    });
  }

  public get(textType: TextType, channelId: string): Promise<TextObj> {
    return this.getRepository().findOne({ textType: Number(textType), channel: channelId });
  }

  public getMultiple(textTypes: number[], channel: string): Promise<TextObj[]> {
    console.log(textTypes);
    return this.getRepository().find({
      where: {
        channelId: channel,
        textType: In(textTypes)
      }
    });
  }

  public updateMultiple(textObjs: TextObj[]) {
    textObjs.forEach((e: TextObj) => {
      e.text ? this.update(e) : this.delete(e.textType, e.channel);
    });
  }

  public deleteById(id: number): Promise<DeleteResult> {
    return this.getRepository().delete({ id });
  }

  public create(entity: TextObj): Promise<TextObj> {
    return this.getRepository().save(entity);
  }

  public update(entity: TextObj): Promise<void | UpdateResult> {
    return this.getRepository()
      .update({ channel: entity.channel, textType: entity.textType }, { ...entity })
      .catch(ex => console.log(ex));
  }

  public async getCustomString(textType: TextType, channelId: string): Promise<string | undefined> {
    const text = await this.getRepository().findOne({
      textType: Number(textType),
      channel: channelId
    });
    return text?.text;
  }
}
