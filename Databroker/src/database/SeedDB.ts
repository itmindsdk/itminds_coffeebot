import { injectable } from "tsyringe";
// import { generateGroups } from "~helpers/misc";
// import { Channel } from "~types/Slack";

@injectable()
export default class SeedDb {
  constructor() { }

  // TODO: make some more useful channel/member combos
  // public seed(): void {
  //   const channels: Channel[] = [];
  //   for (let i = 0; i < 5; i++) {
  //     const members: ChannelMember[] = [];
  //     for (let j = 0; j < 50; j++) {
  //       members.push({
  //         userId: this.generateID(11, "U"),
  //         skipRounds: 0,
  //         channel: this.generateID(11, "D")
  //       });
  //     }
  // channels.push({
  //   channel: this.generateID(11, "C"),
  //   active: true,
  //   runDate: new Date(),
  //   eventInterval: 14,
  //   members
  // } as Channel);
  // }

  // Create a group iteration for channel 2
  // const groups: Group[] = [];
  // const generatedGroups: GroupMember[][] = generateGroups(
  //   channels[2].members,
  //   3
  // );

  // generatedGroups.forEach(group => {
  //   groups.push({
  //     members: group,
  //     thread_ts: "",
  //     msgId: "",
  //     captain: 0
  //   });
  // });

  // channels[2].groupIterations = [
  //   {
  //     channel: this.generateID(11, "D"),
  //     groups
  //     //deadline: new Date()
  //   }
  // ];

  // TODO: lav dette i controllerne
  // this.dbService.getCollection(DBCollections.CHANNELS).drop(() => {
  //   this.dbService.getCollection(DBCollections.CHANNELS).insertMany(channels);
  // });
  // }

  private generateID(length: number, firstChar: string): string {
    let result = firstChar;
    const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }
}
