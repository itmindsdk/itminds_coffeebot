import { Column, Entity, PrimaryColumn } from "typeorm";
import { VacationRequest, Status } from "common";

/**
 * VacationRequest is an entity found in the sursen DB.
 */
@Entity({ name: "VacationRequests" }) // force name to match the name in the sursen DB.
export default class VacationRequestEntity implements VacationRequest {
  @PrimaryColumn({ name: "Id" })
  id: number;

  @Column({ name: "EmployeeId" })
  employeeId: number;

  @Column({ name: "VacationStart" })
  vacationStart: Date;

  @Column({ name: "VacationEnd" })
  vacationEnd: Date;

  @Column({ name: "CreatedDate" })
  createdDate: Date;

  @Column({ name: "Status", enum: Status })
  statusEnumValue: number;

  get status(): Status {
    return this.statusEnumValue;
  }

  set status(status: Status) {
    this.statusEnumValue = status;
  }

  @Column({ name: "ApplicationNote" })
  applicationNote: string;

  @Column({ name: "ResponseNote" })
  responseNote: string;
}
