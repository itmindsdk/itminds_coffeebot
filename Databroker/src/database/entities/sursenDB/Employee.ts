import { Entity, PrimaryColumn, Column } from "typeorm";
import { Employee, Department } from "common";

@Entity({ name: "Employees" }) // force name used in sursen DB
export default class EmployeeEntity implements Employee {
  @PrimaryColumn({ name: "Id" })
  id: number;

  @Column({ name: "Name" })
  name: string;

  @Column({ name: "Initials" })
  initials: string;

  @Column({ name: "Birthdate" })
  birthdate: Date;

  @Column({ name: "PhoneNumber" })
  phoneNumber: string;

  @Column({ name: "Department", enum: Department })
  departmentEnumValue: number;

  get department(): Department {
    return this.departmentEnumValue;
  }

  set department(department: Department) {
    this.departmentEnumValue = department;
  }

  @Column({ name: "DateStart" })
  dateStart: Date;

  @Column({ name: "DateEnd" })
  dateEnd: Date;

  @Column({ name: "Mail" })
  mail: string;
}
