import { Entity, Column, ManyToOne } from "typeorm";
import DBEntity from "./DBEntity";
import ChannelEntity from "./Channel";
import { ChannelMember as IChannelMember } from "common";

@Entity({ database: process.env.MAINDB_DATABASE, name: "ChannelMember" })
export default class ChannelMemberEntity extends DBEntity implements IChannelMember {
  @Column()
  userId: string;

  @Column()
  skipRounds?: number;

  @ManyToOne(
    type => ChannelEntity,
    channel => channel.members
  )
  channel?: string;

  @Column({ nullable: true })
  mail?: string;
}

export const ChannelMemberSchema = {
  userId: { type: "string", required: true, example: "1abc" },
  skipRounds: { type: "number", required: true, example: 3 },
  channel: { type: "string", required: true, example: "1abc" },
  mail: {
    type: "string",
    example: "abc@it-minds.dk"
  }
};
