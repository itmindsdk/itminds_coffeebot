import { Entity, Column, OneToMany, PrimaryColumn } from "typeorm";
import ChannelMemberEntity from "./ChannelMember";
import GroupIterationEntity from "./GroupIteration";
import TextEntity from "./TextEntity";
import ReminderEntity from "./Reminder";
import { ChannelMemberSchema } from "./ChannelMember";
import { GroupIterationSchema } from "./GroupIteration";
import { TextEntitySchema } from "./TextEntity";
import { ReminderSchema } from "./Reminder";
import { Channel as IChannel } from "common";

/**
 * The channel entity represents the channel found in slack.
 * Contains all the settings for that channel.
 */
@Entity({ database: process.env.MAINDB_DATABASE, name: "Channel" })
export default class ChannelEntity implements IChannel {
  /**
   * The Channel id directly from slack.
   */
  @PrimaryColumn()
  id: string;

  /**
   * Describes if the bot at the moment is a part of this channel.
   * Bot is right now a part of the channel: true
   * Bot have been a part of this channel: false
   */
  @Column()
  active?: boolean;

  @Column({ default: "Unknown name" })
  name: string;

  /**
   * Describes if the users of the bot should receive
   * messages from the bot in a thread or in a private msg.
   */
  @Column()
  useThreads?: boolean;

  /**
   * The number of weeks until the event should repeat.
   * If the value is 2, the event will happen every second week.
   */
  @Column()
  eventInterval?: number;

  /**
   * Next date the event will be triggered. (Next group creation)
   */
  @Column()
  runDate?: Date;

  /**
   * How big the groups should be at next group creation.
   * The default value is 2.
   */
  @Column({ default: 2 })
  groupSize: number;

  /**
   * If set to true the reminders will fire otherwise no reminders will be triggered.
   */
  @Column()
  useReminders: boolean;

  /**
   * Describes if coffee captains are enabled for the next round.
   */
  @Column({ nullable: true })
  useCaptain?: boolean;

  /**
   * @OneToMany
   * @foreignkey to all members of the channel of slack.
   * cascade: true
   * match on ChannelMember.channel
   */
  @OneToMany(
    () => ChannelMemberEntity,
    member => member.channel,
    { cascade: true }
  )
  members?: ChannelMemberEntity[];

  /**
   * @OneToMany
   * @foreignkey to all group iteration of the channel.
   * cascade: true
   * match on GroupIteration.channel
   */
  @OneToMany(
    () => GroupIterationEntity,
    groupIteration => groupIteration.channel,
    {
      cascade: true
    }
  )
  groupIterations?: GroupIterationEntity[];

  /**
   * @OneToMany
   * @foreignkey to the custom text's for this channel.
   * cascade: true
   * match on TextEntity.channel
   */
  @OneToMany(
    () => TextEntity,
    text => text.channel,
    {
      cascade: true
    }
  )
  texts?: TextEntity[];

  /**
   * @OneToMany
   * @foreignkey to the reminders of this channel.
   * cascade: true
   * match on Reminder.channel
   */
  @OneToMany(
    () => ReminderEntity,
    reminder => reminder.channel,
    {
      cascade: true
    }
  )
  reminders?: ReminderEntity[];
}

//TODO missing the array of other stuff
export const ChannelSchema = {
  id: { type: "string", required: true, example: "1abc" },
  active: { type: "boolean", required: true, example: true },
  name: { type: "string", required: true, example: "name of channel" },
  useThreads: { type: "boolean", required: true, example: true },
  eventInterval: { type: "number", required: true, example: 3 },
  runDate: {
    type: "Date",
    required: true,
    example: new Date()
  },
  groupSize: {
    type: "number",
    example: 2
  },
  useReminders: {
    type: "boolean",
    required: true,
    example: true
  },
  useCaptain: {
    type: "boolean",
    example: true
  },
  members: {
    type: "array",
    required: false,
    items: {
      type: "object",
      properties: ChannelMemberSchema
    }
  },
  groupIterations: {
    type: "array",
    required: false,
    items: {
      type: "object",
      properties: GroupIterationSchema
    }
  },
  textEntitys: {
    type: "array",
    required: false,
    items: {
      type: "object",
      properties: TextEntitySchema
    }
  },
  reminders: {
    type: "array",
    required: false,
    items: {
      type: "object",
      properties: ReminderSchema
    }
  }
};
