import { Entity, OneToMany, ManyToOne, Column } from "typeorm";
import GroupEntity, { GroupSchema } from "./Group";
import DBEntity from "./DBEntity";
import ChannelEntity from "./Channel";
import { GroupIteration as IGroupIteration } from "common";

/**
 * GroupIteration consists of all the groups of a given iteration.
 */
@Entity({ database: process.env.MAINDB_DATABASE, name: "GroupIteration" })
export default class GroupIterationEntity extends DBEntity implements IGroupIteration {
  /**
   * @OneToMany
   * @foreignkey to all groups of a given iteration.
   */
  @OneToMany(
    type => GroupEntity,
    group => group.groupIteration,
    { cascade: true }
  )
  groups?: GroupEntity[];

  @Column({ nullable: true })
  channelId: string;

  /**
   * @ManyToOne
   * @foreignkey to the channel containing this Group iteration.
   */
  @ManyToOne(
    type => ChannelEntity,
    channel => channel.groupIterations
  )
  channel: ChannelEntity;
}

export const GroupIterationSchema = {
  id: { type: "number", required: true, example: 0 },
  groups: {
    type: "array",
    required: false,
    items: {
      type: "object",
      properties: GroupSchema
    }
  },
  channel: { type: "string", required: true, example: "CNNV0AX2N" }
};
