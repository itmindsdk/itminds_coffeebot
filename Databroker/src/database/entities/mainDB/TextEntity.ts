import { Entity, Column, ManyToOne, Index } from "typeorm";
import DBEntity from "./DBEntity";
import ChannelEntity from "./Channel";
import { TextObj, Locale, TextType } from "common";

/**
 * A TextEntity overrides the default found in src/helpers/TextHelper.ts.
 * This means that if a custom text is in the database
 * with the Enum of the text and right channel id
 * then the text contained in the entity will be used instead of the default.
 */
@Entity({ database: process.env.MAINDB_DATABASE, name: "TextEntity" })
/**
 * Make unique combination of the two columns "textType" and "channel".
 * https://typeorm.io/#/indices
 */
@Index(["textType", "channel"], { unique: true })
export default class TextEntity extends DBEntity implements TextObj {
  @ManyToOne(
    type => ChannelEntity,
    channel => channel.texts
  )
  channel: string;

  @Column({ name: "textType", enum: TextType })
  textType: number;

  @Column({ name: "locale", enum: Locale })
  locale: number;

  /*
  get textType(): TextType {
    return this.textTypeEnumValue;
  }

  set textType(textType: TextType) {
    this.textTypeEnumValue = textType;
  } */

  @Column()
  text: string;
}

export const TextEntitySchema = {
  channel: { type: "string", required: true, example: "CNNV0AX2N" },
  textType: { type: "string", required: true, example: "WelcomeUserText" },
  locale: { type: "string", required: true, example: "da-dk" },
  text: { type: "string", required: true, example: "Just a string" }
};
