import { Column, Entity, ManyToOne } from "typeorm";
import ChannelEntity from "./Channel";
import DBEntity from "./DBEntity";
import { Reminder as IReminder, ReminderType, ReminderRepeatType } from "common";

@Entity({ database: process.env.MAINDB_DATABASE, name: "Reminder" })
export default class ReminderEntity extends DBEntity implements IReminder {
  constructor(channel: string, triggerDate: Date) {
    super();
    this.triggerDate = triggerDate;
    this.channel = channel;
  }

  @ManyToOne(
    () => ChannelEntity,
    channel => channel.reminders
  )
  channel: string;

  @Column({ default: () => "CURRENT_TIMESTAMP" })
  triggerDate: Date;

  @Column({ default: false })
  haveTriggered: boolean;

  @Column({ default: "No text" })
  text: string;

  @Column({ default: ReminderType.SIMPLE, enum: ReminderType })
  typeEnumValue: number;

  get type(): ReminderType {
    return this.typeEnumValue;
  }

  set type(type: ReminderType) {
    this.typeEnumValue = type;
  }

  @Column({ default: ReminderRepeatType.NONE, enum: ReminderRepeatType })
  repeatTypeEnumValue?: number;

  get repeatType(): ReminderRepeatType {
    return this.repeatTypeEnumValue;
  }

  set repeatType(repeatType: ReminderRepeatType) {
    this.repeatTypeEnumValue = repeatType;
  }

  @Column({ default: 0 })
  RepeatDayNumber: number;
}

export const ReminderSchema = {
  id: { type: "number", required: false, example: 0 },
  channel: { type: "string", required: true, example: "CNNV0AX2N" },
  triggerDate: {
    type: "Date",
    required: false,
    example: "1995-12-17T03:24:00"
  },
  haveTriggered: { type: "boolean", required: false, example: true },
  text: { type: "string", required: false, example: "Just a string" },
  type: { type: "number", required: false, example: ReminderType.SIMPLE },
  repeatType: { type: "number", required: false, example: 0 },
  RepeatDayNumber: { type: "number", required: false, example: 0 }
};
