import { PrimaryGeneratedColumn } from "typeorm";

/**
 * This abstract class just provides the column with an auto gen Primary key.
 */
export default abstract class DBEntity {
  @PrimaryGeneratedColumn() // Primary key for most entities
  id?: number;
}
