import { Entity, Column, OneToMany, ManyToOne } from "typeorm";
import DBEntity from "./DBEntity";
import GroupMemberEntity, { GroupMemberSchema } from "./GroupMember";
import GroupIterationEntity from "./GroupIteration";
import { Group, GroupIteration, GroupMember } from "common";

/**
 * The group consisting of groupMembers in a particular channel and iteration.
 */
@Entity({ database: process.env.MAINDB_DATABASE, name: "Group" })
export default class GroupEntity extends DBEntity implements Group {
  /**
   * True if the group has met. otherwise false.
   */
  @Column()
  isCompleted?: boolean;

  /**
   * @OneToMany
   * All the members of the group
   */
  @OneToMany(
    type => GroupMemberEntity,
    groupMember => groupMember.group,
    {
      cascade: true
    }
  )
  members: GroupMember[];

  /**
   * Thread to post in for this group
   */
  @Column({ nullable: true })
  thread_ts?: string;

  /**
   * msgId to post in for this group
   */
  @Column({ nullable: true })
  msgId?: string;

  /**
   * Index of who is the captain in this group.
   */
  @Column({ nullable: true })
  captain?: number;

  @Column({ nullable: true })
  groupIterationId?: number;

  /**
   * @ManyToOne
   * Id of the parent groupIteration.
   * match on GroupIteration.id
   */
  @ManyToOne(
    type => GroupIterationEntity,
    groupIteration => groupIteration.id
  )
  groupIteration?: GroupIteration;
}

export const GroupSchema = {
  id: { type: "number", required: true, example: 0 },
  isCompleted: { type: "boolean", required: false, example: true },
  members: {
    type: "array",
    required: false,
    items: {
      type: "object",
      properties: GroupMemberSchema
    }
  },
  thread_ts: { type: "string", required: false, example: "1573203056.00500" },
  msgId: { type: "string", required: false, example: "1573203056.00500" },
  captain: { type: "number", required: false, example: 0 },
  groupIteration: { type: "number", required: false, example: 0 }
};
