import { Entity, Column, ManyToOne } from "typeorm";
import DBEntity from "./DBEntity";
import GroupEntity from "./Group";
import { GroupMember, Group } from "common";

@Entity({ database: process.env.MAINDB_DATABASE, name: "GroupMember" })
export default class GroupMemberEntity extends DBEntity implements GroupMember {
  @Column()
  userId: string;

  @Column({ nullable: true })
  groupId: number;

  @ManyToOne(
    type => GroupEntity,
    group => group.id
  )
  group: Group;
}

export const GroupMemberSchema = {
  id: { type: "number", required: true, example: 0 },
  userId: { type: "string", required: true, example: "UNNV1AJBU" },
  group: { type: "number", required: true, example: 0 }
};
