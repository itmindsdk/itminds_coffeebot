// import TextType from "~enums/TextType";
// import IApi from "~api/IApi";

// /**
//  * All default texts used.
//  */
// const defaultTexts = new Map<TextType, string>([
// 	[
// 		TextType.BotJoinText,
// 		"Hi there! I am your new coffee buddies bot. Groups will be created every #weeks with the first groups being created #startdate."
// 	],
// 	[
// 		TextType.WelcomeUserText,
// 		"Welcome to the channel! In this channel the Coffie buddies can help you organize your meetings."
// 	],
// 	[TextType.OnlyAdminText, "Only admins can initiate group creation"],
// 	[TextType.GroupSizeText, "Group size is currently set to: #size"],
// 	[TextType.ErrorOnlyAdminGroupSize, "Only admins can set the group size"],
// 	[TextType.ErrorNotInt, "Not an integer"],
// 	[TextType.ErrorGroupGreaterThen, "Groups have to be greater than 2 people"],
// 	[TextType.ErrorUnknownCommandText, "Unknown command argument(s)"],
// 	[
// 		TextType.WelcomeGroup,
// 		"Welcome everyone! Say hi to your coffee buddies! :D"
// 	],
// 	[TextType.WhoIsCaptain, "Your Coffee Captain is #captain."],
// 	[
// 		TextType.CoffeeResponsible,
// 		"The Coffee Captain is responsible for setting up a meeting. \nResponsible for making sure the meeting happens.\nResponsible for uploading a picture from the meeting, in this conversation."
// 	],
// 	[
// 		TextType.RepostMsg,
// 		"The group consisting of #group have met and enjoyed their coffee! Well done! :heart::coffee::tada: #author."
// 	],
// 	[
// 		TextType.GroupComplete,
// 		"This group is now marked as having met! well done! :heart::coffee:"
// 	],
// 	[
// 		TextType.ActionReminder,
// 		"*Reminder:*\nYou have know one day left to upload a picture of the meeting wuth your coffie buddies:"
// 	],
// 	[
// 		TextType.ActionReminderDetails,
// 		"*Last day in this round:*\n #LastDate \n*The groups:*\n #groups\n*Coffee Captains:* \n #caps \n Please confirm or deny if you have meet"
// 	]
// ]);

// export default defaultTexts;

// const regex: RegExp = /#[a-zA-Z]*/g;

// /**
//  * returns the tekst needed for that paticular channel and texttype.
//  * returns a custom text if it exist in the db otherwise the default text.
//  * @returns string
//  */
// export async function getText(
// 	type: TextType,
// 	channelId: string,
// 	funcMap?: Map<string, { (): string }>
// ): Promise<string> {
// 	let returnString = await textTypeController.customOrDefaultString(
// 		type,
// 		channelId
// 	);
// 	if (!funcMap) return returnString;
// 	const list = returnString.match(regex) as string[];
// 	list.forEach(element => {
// 		const funk = funcMap.get(element);
// 		if (funk) returnString = returnString.replace(element, funk());
// 	});

// 	if (returnString.includes("#")) console.warn("Error when parsing");

// 	return returnString;
// }

// // all the kinds of #names there can be used in texts to compute a dynamic output.
// export enum BotJoinText {
// 	WEEK = "#weeks",
// 	STARTDATE = "#startdate"
// }

// export enum GroupText {
// 	SIZE = "#size",
// 	CAPTAIN = "#captain"
// }

// export enum Repost {
// 	GROUP = "#group",
// 	AUTHOR = "#author"
// }

// export enum actionReminder {
// 	LASTDATE = "#LastDate",
// 	CAPS = "#caps",
// 	GROUPS = "#groups"
// }
