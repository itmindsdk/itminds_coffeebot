import { TextType, Group, DynamicContentContext } from "common";
import GroupIterationController from "../../database/controllers/mainDB/GroupIterationController";
import { ChannelController, GroupController } from "../../database/controllers/Controllers";

const channelController = new ChannelController();
const groupController = new GroupController();
const groupIterationController = new GroupIterationController();

const checkForRequired = (
  textType: string,
  funcName: string,
  context: DynamicContentContext,
  requiredProps: string[]
): false | string => {
  const missingProps = requiredProps.reduce((acc, prop) => {
    if (!context.hasOwnProperty(prop)) {
      return [...acc, prop];
    }
  }, []);

  if (!missingProps || missingProps.length === 0) {
    return false;
  }

  return `${textType}: Dynamic content '${funcName}' requires the following props in context: ${missingProps.join(", ")}.`;
};

const DynamicContentFuncs = {
  [TextType.BotJoinText]: {
    weeks: async (context: DynamicContentContext) => {
      // Check for required props.
      const errorString = checkForRequired(TextType.BotJoinText, "weeks", context, ["channelId"]);
      if (errorString) {
        throw Error(errorString);
      }

      const schedule = await channelController.getEventSchedule(context.channelId);
      const interval = schedule.eventInterval;
      return interval === 1 ? `${interval} week` : `${interval} weeks`;
    },
    nextRunDate: async (context: DynamicContentContext) => {
      // Check for required props.
      const errorString = checkForRequired(TextType.BotJoinText, "nextRunDate", context, ["channelId"]);
      if (errorString) {
        throw Error(errorString);
      }

      const schedule = await channelController.getEventSchedule(context.channelId);
      return schedule.runDate.toDateString();
    }
  },
  [TextType.GroupSizeText]: {
    groupSize: async (context: DynamicContentContext) => {
      // Check for required props.
      const errorString = checkForRequired(TextType.GroupSizeText, "groupSize", context, ["channelId"]);
      if (errorString) {
        throw Error(errorString);
      }

      const groupSize = await channelController.getGroupSize(context.channelId);
      return groupSize.toString();
    }
  },
  [TextType.WhoIsCaptain]: {
    captain: async (context: DynamicContentContext) => {
      // If we have a captain user ID, use that before anything else.
      if (context.captainUserId) {
        return `<@${context.captainUserId}>`;
      }
      // If we have a group ID, use the group ID to find the captain's user ID.
      if (context.groupId) {
        const getCaptain = (group: Group) => {
          const captainIndex = group.captain as number;
          const captain = group.members[captainIndex];
          return captain;
        };
        const group = await groupController.getGroup(context.groupId);
        const captain = getCaptain(group);
        return `<@${captain.userId}>`;
      }
      // If none of these exist, throw an error.
      throw Error("Dynamic content 'captain' requires either captainUserId or groupId to work.");
    }
  },
  [TextType.RepostMsg]: {
    groupMembers: async (context: DynamicContentContext) => {
      // Check for required props.
      const errorString = checkForRequired(TextType.RepostMsg, "groupMembers", context, ["groupId"]);
      if (errorString) {
        throw Error(errorString);
      }

      const group = await groupController.getGroup(context.groupId);

      if (!group) {
        throw Error(`Group with ID ${context.groupId} not found in database...`);
      }

      const mentions = group.members.map(m => `<@${m.userId}>`);
      const mentionsStr = mentions.reduce(
        (acc, mention, index) =>
          `${acc}${index == 0 ? "" : index > 0 && index < group.members.length - 1 ? ", " : " & "}${mention}`,
        ""
      );

      return mentionsStr;
    },
    authorMessage: async (context: DynamicContentContext) => {
      // Check for required props.
      const errorString = checkForRequired(TextType.RepostMsg, "authorMessage", context, ["authorMessage"]);
      if (errorString) {
        throw Error(errorString);
      }

      const { authorUserId, message } = context.authorMessage;

      return message === "" ? "" : `\n<@${authorUserId}> wrote: "${message}"`;
    }
  },
  [TextType.ChannelGroupCompleted]: {
    groupMembers: async (context: DynamicContentContext) => {
      // Check for required props.
      const errorString = checkForRequired(TextType.ChannelGroupCompleted, "groupMembers", context, ["groupId"]);
      if (errorString) {
        throw Error(errorString);
      }

      const group = await groupController.getGroup(context.groupId);

      const mentions = group.members.map(m => `<@${m.userId}>`);
      const mentionsStr = mentions.reduce(
        (acc, mention, index) =>
          `${acc}${index == 0 ? "" : index > 0 && index < group.members.length - 1 ? ", " : " & "}${mention}`,
        ""
      );

      return mentionsStr;
    },
  },
  [TextType.ActionReminderDetails]: {
    endDate: async (context: DynamicContentContext) => {
      const nextRunDate = await channelController.getNextRunDate(context.channelId);
      const date = new Date(nextRunDate.getTime() - 1);
      return date.toLocaleDateString();
    },
    groups: async (context: DynamicContentContext) => {
      const currentIteration = await groupIterationController.getCurrentIteration(context.channelId);

      const allMentions = currentIteration.groups.reduce((acc, curGroup, groupIndex) => {
        const mentions = curGroup.members.map(m => `<@${m.userId}>`);
        const mentionsStr = mentions.reduce(
          (acc, mention, index) =>
            `${acc}${index == 0 ? "" : index > 0 && index < curGroup.members.length - 1 ? ", " : " & "}${mention}`,
          ""
        );
        return `${acc}\n\nGroup ${groupIndex + 1}\n${mentionsStr}`;
      }, "");

      return allMentions;
    },
    captains: async (context: DynamicContentContext) => {
      const captains = await groupIterationController.getAllCaptains(context.channelId);

      const mentions = captains.map(cUserId => `<@${cUserId}>`);
      const mentionsStr = mentions.reduce(
        (acc, mention, index) =>
          `${acc}${index == 0 ? "" : index > 0 && index < captains.length - 1 ? ", " : " & "}${mention}`,
        ""
      );

      return mentionsStr;
    }
  }
};

export default DynamicContentFuncs;
