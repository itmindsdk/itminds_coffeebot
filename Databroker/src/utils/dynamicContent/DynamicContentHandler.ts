import { TextType, Locale, DynamicContentContext, TextObj } from "common";
import DynamicContentFuncs from "./DynamicContentFuncs";
import texts from "../../data/texts.json";
import { TextTypeController } from "database/controllers/Controllers";

type LocalisedTextObject = {
  text: string;
  dynamicContent?: { name: string; description: string }[];
};

type TextObject = {
  [locale: string]: LocalisedTextObject;
};

type TextObjects = {
  [textType: string]: TextObject;
};

const textObjects = texts as TextObjects;
const textTypeController = new TextTypeController();

export default class DynamicContentHandler {
  constructor() { }

  /**
   * Returns a TextEntity with a string, with interpolated dynamic content values.
   * @param type
   * @param locale
   * @param context
   */
  async get(type: TextType, locale: Locale, context: DynamicContentContext) {
    const { channelId } = context;

    const localisedTextObj = await this.getTextObj(type, locale, channelId);
    const dynamicContent = localisedTextObj.dynamicContent;

    // If there is no dynamic content available for the string, return it immediately.
    if (!dynamicContent || dynamicContent.length == 0) {
      return {
        textType: Number(type),
        text: localisedTextObj.text,
        locale: Number(locale),
        channel: channelId
      } as TextObj;
    }

    const finalText = await this.getFinalText(type, localisedTextObj, context);

    return {
      textType: Number(type),
      text: finalText,
      locale: Number(locale),
      channel: channelId
    } as TextObj;
  }

  /**
   * Returns a text object containing the string without having interpolated the dynamic content
   * and an array of dynamic content items and their descriptions.
   * @param type
   * @param locale
   * @param channelId
   */
  async getRaw(type: TextType, locale: Locale, channelId: string) {
    return await this.getTextObj(type, locale, channelId);
  }

  /**
   * Returns the text object with translations and dynamic content
   * @param type
   * @param locale
   * @param channelId
   */
  private async getTextObj(type: TextType, locale: Locale, channelId: string): Promise<LocalisedTextObject> {
    // TODO: Update database and text type controller to allow for translations.
    const textObjAllLocales = textObjects[type];
    console.log("Type: " + type + "\n", textObjAllLocales);
    const localeToUse = textObjAllLocales[locale] ? locale : Locale.default;

    if (!textObjAllLocales[localeToUse]) {
      throw Error(
        `Neither given locale (${locale}) or a default locale was present in the text object for type ${type}.`
      );
    }

    const updateText = (textObj: LocalisedTextObject, text: string): LocalisedTextObject => {
      if (!text) {
        return { ...textObj };
      }
      return {
        ...textObj,
        text
      };
    };

    const textObj = textObjAllLocales[localeToUse];
    // TODO: When we save translations, finish up the fetching of specific localised text entities.
    const textEntity = await textTypeController.get(type, channelId) || {} as any;
    const { text } = textEntity;
    const updatedTextObj = updateText(textObj, text);

    return updatedTextObj;
  }

  private async getFinalText(
    type: TextType,
    textObj: LocalisedTextObject,
    context: DynamicContentContext
  ): Promise<string> {
    const dcFuncs = DynamicContentFuncs[type];
    const finalText = await Object.keys(dcFuncs).reduce(async (acc, dcItem) => {
      const accText = await acc;
      const dcItemText = `[${dcItem}]`;
      const dcItemFunc = dcFuncs[dcItem];
      // If the dynamic content item was not found, return the string as is.
      if (accText.indexOf(dcItemText) < 0) {
        return accText;
      }
      // Else replace the dynamic content item before returning the string.
      const replacementText = (await dcItemFunc(context)) as string;
      return accText.replace(dcItemText, replacementText);
    }, Promise.resolve(textObj.text));

    return finalText;
  }
}
