import { expect } from "chai";
import { createConnection } from "typeorm";
import { ChannelController, ChannelMemberController } from "database/controllers/Controllers";
import { Channel, ChannelMember, GroupIteration } from "common";

const channelController = new ChannelController();
/* const groupIterationController = new GroupIterationController(); */
const channelMemberController = new ChannelMemberController();
const channelId = "channelId";

before(async function () {
  this.timeout(3000);
  await createConnection();
});

describe("database ChannelController testing - main actions", () => {
  it("should return the new created Channel", () => {
    const channel = {
      id: channelId,
      useThreads: true,
      groupSize: 10,
      eventInterval: 12,
      active: true,
      runDate: new Date()
    } as Channel;
    return channelController.createChannel(channel).then(res => {
      // console.log(res);
      expect(res).to.have.property("threadedMsg");
    });
  });

  it("should return all channels", () => {
    return channelController.getAllChannels().then(res => {
      // console.log(res);
      expect(res).to.be.an("array");
    });
  });
});

describe("database ChannelController testing - partial updating", () => {
  it("Update eventInterval", () => {
    const updateValue = 2;
    return channelController.updateEventInterval(channelId, updateValue).then(async res => {
      expect(res)
        .to.have.property("affected")
        .above(0);
      const channel = await channelController.getChannel(channelId);
      expect(channel)
        .to.have.property("eventInterval")
        .equal(updateValue);
    });
  });
  /*
  it("jep", async () => {
    const iter = new GroupIteration();
    iter.channelId = channelId;
    iter.deadline = new Date();
    iter.groups = [];

    for (let i = 0; i < 3; i++) {
      const array: GroupMember[] = [{ userId: "12345" }, { userId: "12346" }];
      const group = new Group();
      group.isCompleted = false;
      group.members = array;
      group.thread_ts = "";

      iter.groups.push(group);
    }

    const done = await iterController.createGroupIteration(iter);

    console.log(done);

    // iter.channelId = channelId;
    // const group = new Group();

    //iter.groups.push()
    // return channelController.addGroupIteration()
  });

  */

  it("update Channel Members", async () => {
    const array: ChannelMember[] = [
      { userId: "12347", skipRounds: 2 },
      { userId: "12346", skipRounds: 3 }
    ];
    channelController.updateChannelMembers(array);
  });

  it("", () => {
    const group = {
      channelId: "CP946TEST",
      groups: []
    } as GroupIteration;
    /* groupIterationController.createGroupIteration(group); */
  });

  it("getCurrentIteration", async () => {
    /*   const res: GroupIteration = await groupIterationController.getCurrentIteration("CP946TEST"); */
    /*     expect(res)
      .to.have.property("id")
      .equal(channelId);*/
    // expect(res)
    //   .to.have.property("deadline")
    //   .to.be.a("Date");
    /*   expect(res) */
    /*    .to.have.property("groups")
      .to.be.an("array"); */
  });

  it("getCurrentIterationID", async () => {
    /*  const id: number = await groupIterationController.getCurrentIterationId("CP946TEST"); */
    /*   expect(id).to.be.a("number"); */
    // expect(id).to.be.equal(8);
  });

  it("createChannelMember", async () => {
    channelMemberController.createChannelMembers([{
      userId: "UNNV1TEST",
      channel: "CP946TEST",
      skipRounds: 15
    }]);
  });

  it("deleteChannelMember", async () => {
    channelMemberController.deleteChannelMember("CP946TEST", "UNNV1TEST");
  });
});
