import { expect } from "chai";
import { createConnections } from "typeorm";
import { TextTypeController } from "database/controllers/Controllers";
// import { getText } from "~helpers/TextHelper";
import { TextType, Locale } from "common";

before(async function () {
  this.timeout(5000);
  await createConnections().then(async res => {
    console.log("Main DB Connnected: ", res[0].isConnected);
    console.log("Sursen connected: ", res[1].isConnected);
  });
});

describe("Text admin", () => {
  it("createting welcome msg", () => {
    const textTypeController = new TextTypeController();
    textTypeController.create({
      channel: "CPQEJGDK9",
      text: "#weeks , #startDate",
      locale: Number(Locale.default),
      textType: Number(TextType.BotPresentationText)
    });
  });

  // it("parsing welcome msg", async () => {
  //   const interval: number = 2;
  //   const startDate: Date = new Date();

  //   const funk = () =>
  //     interval === 1 ? `${interval} week` : `${interval} weeks`;
  //   const funk2 = () => startDate.toDateString();

  //   const map = new Map<string, { (): string }>();
  //   map.set("#weeks", funk);
  //   map.set("#startDate", funk2);

  //   const res = await getText(TextType.BotPresentationText, "CPQEJGDK9", map);

  //   console.log(res);
  //   expect(res).to.not.contain("#weeks");
  //   expect(res).to.not.contain("#startDate");
  // });

  //   it("Deleting welcome msg", () => {
  //     const textTypeController = new TextTypeController();
  //     textTypeController.delete(TextTypes.BotPresentationText, "CPQEJGDK9");
  //   });

  it("createting welcome msg", async () => {
    const textTypeController = new TextTypeController();
    const text = await textTypeController.getCustomString(TextType.BotJoinText, "CPQEJGDK9");
    console.log(text);
  });
});
