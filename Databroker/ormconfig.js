const dbConfigs = [];

dbConfigs[0] = {
  name: "default",
  type: "mssql",
  host: process.env.MAINDB_HOST,
  port: Number(process.env.MAINDB_PORT),
  username: process.env.MAINDB_USER,
  password: process.env.MAINDB_PASSWORD,
  database: process.env.MAINDB_DATABASE,
  synchronize: true,
  logging: false,
  options: { encrypt: true },
  entities: ["src/database/entities/mainDB/**/*.ts"],
  migrations: ["src/database/mainDB/migrations/**/*.ts"],
  subscribers: ["src/database/mainDB/subscriber/**/*.ts"],
  cli: {
    entitiesDir: "src/database/entities/mainDB/",
    migrationsDir: "src/database/mainDB/migrations",
    subscribersDir: "src/database/mainDB/subscriber",
  },
  connectionTimeout: 60000,
  requestTimeout: 60000,
};

dbConfigs[1] = {
  name: "Sursen",
  type: "mssql",
  host: process.env.SURSEN_HOST,
  port: Number(process.env.SURSEN_PORT),
  username: process.env.SURSEN_USER,
  password: process.env.SURSEN_PASSWORD,
  database: process.env.SURSEN_DATABASE,
  synchronize: false,
  logging: false,
  options: { encrypt: true },
  entities: ["src/database/entities/sursenDB/**/*.ts"],
};

module.exports = dbConfigs;
