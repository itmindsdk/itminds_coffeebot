const dbConfigs = [];

dbConfigs[0] = {
  name: "default",
  type: "mssql",
  host: process.env.MAINDB_HOST,
  port: Number(process.env.MAINDB_PORT),
  username: process.env.MAINDB_USER,
  password: process.env.MAINDB_PASSWORD,
  database: process.env.MAINDB_DATABASE,
  synchronize: false,
  logging: true,
  options: { encrypt: true },
  entities: ["dist/database/entities/mainDB/**/*.js"],
  migrations: ["dist/database/mainDB/migrations/**/*.js"],
  subscribers: ["dist/database/mainDB/subscriber/**/*.js"],
  cli: {
    entitiesDir: "dist/database/entities/mainDB/",
    migrationsDir: "dist/database/mainDB/migrations",
    subscribersDir: "dist/database/mainDB/subscriber",
  },
  connectionTimeout: 60000,
  requestTimeout: 60000,
};

dbConfigs[1] = {
  name: "Sursen",
  type: "mssql",
  host: process.env.SURSEN_HOST,
  port: Number(process.env.SURSEN_PORT),
  username: process.env.SURSEN_USER,
  password: process.env.SURSEN_PASSWORD,
  database: process.env.SURSEN_DATABASE,
  synchronize: false,
  logging: true,
  options: { encrypt: true },
  entities: ["dist/database/entities/sursenDB/**/*.js"],
};

module.exports = dbConfigs;
