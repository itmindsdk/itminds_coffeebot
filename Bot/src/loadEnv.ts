try {
  /* eslint-disable global-require, @typescript-eslint/no-var-requires */
  const dotenv = require("dotenv");
  const dotenvExpand = require("dotenv-expand");
  /* eslint-disable global-require, @typescript-eslint/no-var-requires */

  // Load process.env values from .env file and expand variables
  dotenvExpand(dotenv.config());
} catch (error) {
  console.warn(`If running in a production environment, ignore this warning,
      otherwise run 'npm i' to install missing dev dependencies.`);
}
