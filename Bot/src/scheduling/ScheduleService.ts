import { inject, injectable, singleton } from "tsyringe";
import ScheduleListener from "./interfaces/ScheduleListener";
import IScheduleService from "./interfaces/IScheduleService";
import ScheduleServiceBase from "./ScheduleServiceBase";
import ReminderFeatures from "../features/ReminderFeatures";

@injectable()
@singleton()
export default class ScheduleService extends ScheduleServiceBase implements IScheduleService {
  constructor(
    @inject("ScheduleListener") private scheduleListener: ScheduleListener,
    protected reminderFeatures: ReminderFeatures,
  ) {
    super(reminderFeatures);
  }

  public createJob(name: string, channelId: string, job: () => void): void {
    super.createJob(name, channelId, job);
  }

  public removeJob(channelId: string): void {
    super.removeJob(channelId);
  }

  public start(): void {
    console.log("STARTING SCHEDULED TASKS!");
    this.scheduleListener.on("wake", () => {
      this.runJobs();
    });
  }

  public stopAll(): void {
    this.scheduleListener.removeAllListeners("wake");
    super.removeAllJobs();
  }

  stop(): void {
    throw new Error("Method not implemented.");
  }

  public setSchedule(schedule: Date): void {
    throw new Error("Method not implemented.");
  }

  public getSchedule(): Date {
    throw new Error("Method not implemented.");
  }

  protected runJobs(): void {
    super.runJobs();
  }

  public isJobRunning(channelId: string): boolean {
    return super.isJobRunningAlready(channelId);
  }
}
