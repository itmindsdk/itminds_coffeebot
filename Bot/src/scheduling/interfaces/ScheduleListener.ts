import { EventEmitter } from "events";

export default abstract class ScheduleListener extends EventEmitter {
  abstract configureService(schedule: string): void;
}
