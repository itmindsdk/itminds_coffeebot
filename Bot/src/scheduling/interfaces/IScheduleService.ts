export default interface IScheduleService {
  createJob(name: string, channelId: string, job: () => void): void;
  removeJob(name: string): void;
  start(): void;
  stop(): void;
  stopAll(): void;
  setSchedule(schedule: Date): void;
  getSchedule(): Date;
  isJobRunning(channelId: string): boolean;
}
