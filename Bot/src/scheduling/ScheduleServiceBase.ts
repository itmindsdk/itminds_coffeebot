import { Routes, Schedule, IApi, Api } from "common";
import ReminderFeatures from "../features/ReminderFeatures";
import { Job } from "../types/Job";
import { getNextRunDate } from "../helpers/misc";

export default abstract class ScheduleServiceBase {
  private api: IApi;
  constructor(protected reminderFeatures: ReminderFeatures) {
    this.api = new Api(process.env.DATABROKER_URL);
  }
  protected static scheduledJobs: Map<string, Job> = new Map();

  protected createJob(name: string, channelId: string, job: () => void): void {
    ScheduleServiceBase.scheduledJobs.set(name, {
      channelId,
      invoke: runDateString => {
        const runDate = new Date(runDateString);
        console.log(`Running job ${name}`);
        if (Date.now() >= runDate.valueOf()) {
          job();
          this.setNewRunDate(channelId, runDate);
        } else {
          console.log("No, not yet!");
        }
        this.reminderFeatures.runReminders(channelId); // check if the channel have any reminders there need to run.
      },
    } as Job);
  }

  private async setNewRunDate(channelId: string, runDate: Date): Promise<void> {
    const sch = await this.api.exec<Schedule>(Routes.channel.getEventSchedule(channelId));

    const nextRunDate = getNextRunDate(runDate, sch.eventInterval);

    console.log("Next run date: ", nextRunDate);
    this.api.execWithBody(Routes.channel.setNextRunDateWithDate(channelId), {
      nextRunDate,
    });
  }

  protected removeJob(channelId: string): void {
    const name = `${channelId}_CreateGroups`;
    ScheduleServiceBase.scheduledJobs.delete(name);
    console.log(`${channelId}_CreateGroups is now removed.`);
    // delete from database as well if so desired
  }

  protected removeAllJobs(): void {
    ScheduleServiceBase.scheduledJobs.clear();
    // delete from database as well if so desired
  }

  protected runJobs(): void {
    ScheduleServiceBase.scheduledJobs.forEach(job => {
      this.api.exec<Date>(Routes.channel.getNextRunDate(job.channelId)).then(runDate => {
        job.invoke(runDate);
      });
    });
  }

  protected isJobRunningAlready(channelId: string): boolean {
    // console.log(ScheduleServiceBase.scheduledJobs);
    const name = `${channelId}_CreateGroups`;
    // console.log("name:" , name);
    const job = ScheduleServiceBase.scheduledJobs.get(name);
    if (typeof job !== "undefined") {
      return true;
    }
    return false;
  }
}
