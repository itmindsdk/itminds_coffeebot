import { singleton } from "tsyringe";
import http from "http";
import ScheduleListener from "../interfaces/ScheduleListener";

@singleton()
export default class HttpScheduler extends ScheduleListener {
  private static readonly PORT?: number = Number(process.env.SCHED_PORT);
  private static readonly HOST?: string = process.env.SCHED_HOST;
  private static server: http.Server;

  constructor() {
    super();
    HttpScheduler.server = http.createServer((req, res) => {
      this.emit("wake");
    });
    HttpScheduler.server.listen(HttpScheduler.PORT);
  }

  configureService(schedule: string): void {
    throw new Error("Method not implemented.");
  }
}
