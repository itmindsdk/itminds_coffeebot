import { singleton } from "tsyringe";
import scheduler from "node-schedule";
import ScheduleListener from "../interfaces/ScheduleListener";

@singleton()
export default class NodeScheduler extends ScheduleListener {
  constructor() {
    super();
    scheduler.scheduleJob("lol", "*/1 * * * *", () => {
      this.emit("wake");
    });
  }

  configureService(schedule: string): void {
    throw new Error("Method not implemented.");
  }
}
