/* eslint-disable consistent-return */
/* eslint-disable no-return-await */
import { App, MemberJoinedChannelEvent, Context, MemberLeftChannelEvent } from "@slack/bolt";
import { injectable, inject } from "tsyringe";
import { IApi, TextType, Routes, Channel, ChannelMember, ITextApi, Api, TextApi, Locale } from "common";
import IScheduleService from "../scheduling/interfaces/IScheduleService";
import { ViewOutputExtension, UsersInfo, ChannelInfo } from "../types/Slack";
import BotConfig from "../types/BotConfig";
import { createBotSetupModal, botSetupButton } from "../helpers/botBlocks";
import GroupFeatures from "./GroupFeatures";

/**
 * # Channel Features contain all channel related features
 * Setup via modal.
 * welcome msg.
 * leave and join event actions. - Here channel members are also added and deleted.
 */
@injectable()
export default class ChannelFeatures {
  private textApi: ITextApi;
  private api: IApi;

  constructor(
    private app: App,
    private groupFeatures: GroupFeatures,
    @inject("IScheduleService") private scheduleService: IScheduleService,
  ) {
    this.api = new Api(process.env.DATABROKER_URL);
    this.textApi = new TextApi(this.api, Locale.default);
  }

  public setupModal(): void {
    let channelId: string;
    this.app.action("doSetup", args => {
      args.ack();
      channelId = args?.body?.channel?.id ?? "0";
      this.app.client.views.open(createBotSetupModal(args));
    });

    this.app.view("botModalSubmitted", async ({ ack, view, context }) => {
      ack();
      const {
        state: {
          values: {
            startDate: { startDateSelected },
            interval: { intervalSelected },
            groupSize: { groupSizeSelected },
            msgType: { msgTypeSelected },
            useCaptain: { useCaptainSelected },
          },
        },
      } = view as ViewOutputExtension;

      const botConfig: BotConfig = {
        useThreads: msgTypeSelected.selected_option.value === "thread",
        startDate: startDateSelected.selected_date,
        interval: Number(intervalSelected.selected_option.value) * 7, // Interval given in weeks.
        groupSize: Number(groupSizeSelected.selected_option.value),
        useCaptain: useCaptainSelected.selected_option.value === "true",
      };

      const datePattern = /^(\d{4})-(\d{2})-(\d{2})$/;
      const [, year, month, date] = datePattern.exec(botConfig.startDate) || [];
      const startDate = new Date(
        Number(year),
        Number(month) - 1, // who the fuck decided to index months from 0-11????
        Number(date),
        10, // for now always set event to 10 AM. TODO: Make configurable.
      );
      this.api.execWithBody(Routes.channel.updateConfig(channelId), {
        useThreads: botConfig.useThreads,
        runDate: startDate,
        eventInterval: botConfig.interval,
        groupSize: botConfig.groupSize,
        useCaptain: botConfig.useCaptain,
      });

      const botJoinedText = await this.textApi.get(TextType.BotJoinText, { channelId });

      this.app.client.chat.postMessage({
        token: context.botToken,
        channel: channelId,
        text: botJoinedText,
      });
    });
  }

  public onJoinEvent(): void {
    const onBotJoined = (event: MemberJoinedChannelEvent, context: Context): void => {
      // this bot has been added to a channel
      console.log(`BOT JOINED CHANNEL (with ID ${event.channel})!`);
      this.app.client.chat.postEphemeral({
        channel: event.channel,
        token: context.botToken,
        user: event.inviter as string,
        text: "",
        blocks: [botSetupButton],
      });
      this.app.client.conversations
        .members({ token: context.botToken, channel: event.channel })
        .then(result => result.members as string[])
        .then(
          (members): Promise<ChannelMember[]> => {
            const channelMemberPromises = members.map(async userId => {
              const mail = await this.getMailOfUser(context.botToken, userId);
              if (!mail) return;
              return {
                userId,
                skipRounds: 0,
                mail,
                channel: event.channel,
              } as ChannelMember;
            });

            // Filter promises to remove undefined members.
            // eslint-disable-next-line no-shadow
            return Promise.all(channelMemberPromises).then(members => members.filter(m => !!m));
          },
        )
        .then(async members => {
          try {
            const channelInfo = await this.app.client.channels.info({
              channel: event.channel,
              token: context.botToken,
            });

            const existingChannel = await this.api.exec(Routes.channel.get(event.channel));

            // If the channel already exists, just add all members and activate it.
            if (existingChannel) {
              await this.api.execWithBody(Routes.channelMember.create(), members);
              await this.api.execWithBody(Routes.channel.updateActive(event.channel), { active: true });
              // TODO: Perhaps change this later, since it gets the same channel twice (although it should now have members).
              return await this.api.exec(Routes.channel.get(event.channel));
            }
            // Create the channel, if it didn't exist.
            return await this.api.execWithBody(Routes.channel.create(), {
              id: event.channel,
              active: true,
              name: (channelInfo as ChannelInfo).channel.name,
              useThreads: true,
              runDate: new Date(),
              eventInterval: 7, // Interval is in days.
              groupSize: 3,
              members,
              useCaptain: false,
              useReminders: false,
            } as Channel);
          } catch (error) {
            console.log("Error occurred:", error);
          }
        })
        .catch(e => {
          console.error(e);
        });

      // start schedule when bot joins channel
      if (!this.scheduleService.isJobRunning(event.channel)) {
        console.log(`Start job: ${event.channel}_CreateGroups`);
        this.groupFeatures.scheduleStartExisting(event.channel, context.botToken);
      }
    };

    const onUserJoined = (event: MemberJoinedChannelEvent, context: Context): void => {
      // a user has been added to a channel this bot is a member of.
      console.log(Routes.channel.isBotInChannel(event.channel));
      this.api.exec<boolean>(Routes.channel.isBotInChannel(event.channel)).then(async isInChannel => {
        console.log(isInChannel);
        if (isInChannel) {
          console.log("USER JOINED CHANNEL! with bot.....", event.channel);
          if (process.pid) {
            console.log(`User joined bot with PID: ${process.pid}`);
          }
          this.api
            .execWithBody(Routes.channelMember.create(), {
              userId: event.user,
              skipRounds: 0,
              channel: event.channel,
              mail: await this.getMailOfUser(context.botToken, event.user),
            } as ChannelMember)
            .then(() => {
              this.WelcomeToUser(event, context);
            })
            .catch(err => {
              console.error(err);
            });
        }
      });
    };

    this.app.event("member_joined_channel", ({ event, context }) => {
      if (event.user === context.botUserId) {
        onBotJoined(event, context);
      } else {
        onUserJoined(event, context);
      }
    });
  }

  private async getMailOfUser(token: string, userId: string): Promise<string> {
    return await this.app.client.users
      .info({
        token,
        user: userId,
      })
      .then(info => {
        return (info as UsersInfo).user.profile.email;
      })
      .catch(error => {
        console.warn(error);
        throw error;
      });
  }

  private async WelcomeToUser(event: any, context: any): Promise<void> {
    const userWelcomeMessage = await this.textApi.get(TextType.WelcomeUserText, { channelId: event.channel });
    this.app.client.chat.postEphemeral({
      user: event.user,
      token: context.botToken,
      channel: event.channel,
      text: userWelcomeMessage,
    });
  }

  public onLeaveEvent(): void {
    const onBotLeftChannel = (event: MemberLeftChannelEvent): void => {
      // this bot has been removed from a channel
      console.log("BOT LEFT CHANNEL!");
      this.api.execWithBody(Routes.channel.updateActive(event.channel), {
        active: false,
      });
      this.api.exec(Routes.channelMember.deleteAll(event.channel));
      this.groupFeatures.scheduleStopJob(event.channel);
    };

    const onUserLeftChannel = (event: MemberLeftChannelEvent): void => {
      // a user has left a channel this bot is a member of
      console.log("USER LEFT CHANNEL!");
      this.api.exec(Routes.channelMember.delete(event.channel, event.user));
    };

    this.app.event("member_left_channel", ({ event, context }) => {
      if (event.user === context.botUserId) {
        onBotLeftChannel(event);
      } else {
        onUserLeftChannel(event);
      }
    });
  }
}
