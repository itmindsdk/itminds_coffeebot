import { App, MessageEvent } from "@slack/bolt";
import { injectable } from "tsyringe";
import { MessageAttachment } from "@slack/types";
import { IApi, TextType, Routes, ITextApi, TextApi, Group, Locale, Api, DynamicContentContext } from "common";
/**
 * # Img upload features contain all fileupload related features
 * Identify an img upload, setting the group to done and post msg to the members.
 */
@injectable()
export default class ImgUploadFeatures {
  private textApi: ITextApi;
  private api: IApi;

  constructor(private app: App) {
    this.api = new Api(process.env.DATABROKER_URL);
    this.textApi = new TextApi(this.api, Locale.default);
  }

  /**
   * Handle all message events, but only take action in case of a file_share there is a image.
   * @returns void
   */
  public onFileSharedEvent(): void {
    this.app.event("message", ({ event, context }) => {
      if (event.subtype !== "file_share") return; // check the subtype --> only respond to file uploads.
      // check it is a img there is uploaded.
      if (this.isImage(event.files)) {
        console.log("event: ", event);
        this.updateGroupIteration(event) // update the group that is now done.
          .then(groupCompleted => {
            this.repost(event, context.botToken, groupCompleted); // post to the channel and the group.
          })
          .catch(err => {
            console.error("Error occurred in ImgUploadFeatures", err);
            throw err;
          });
      } else {
        console.debug("Uploaded file was not an image.");
      }
    });
  }

  private getChannelAndMsgId = async (messageEvent: MessageEvent): Promise<{ channelId: string; msgId: string }> => {
    if (messageEvent.channel_type === "channel") {
      console.log("Message received in channel with ID: ", messageEvent.channel);
      return { channelId: messageEvent.channel, msgId: null };
    }

    const { channelId } = await this.api.exec<{ channelId: string }>(
      Routes.group.getChannelIdFromMsgId(messageEvent.channel),
    );

    console.log(
      "Message received in private message with ID:",
      messageEvent.channel,
      "Connected channel is:",
      channelId,
    );

    return { channelId, msgId: messageEvent.channel };
  };

  private async updateGroupIteration(event: MessageEvent): Promise<Group> {
    const getGroup = (useThreads: boolean, iterationId: number, threadTs: any, msgId: string): Promise<Group> => {
      if (useThreads) {
        return this.api.exec<Group>(Routes.group.getByIterationAndThreads(iterationId), {
          thread_ts: threadTs,
        });
      }
      return this.api.exec<Group>(Routes.group.getByIterationIdAndMsgId(iterationId, msgId));
    };

    const { channelId, msgId } = await this.getChannelAndMsgId(event);

    if (!channelId) {
      throw Error("UpdateGroupIteration: Channel ID not found...");
    }

    const currentIterationId = await this.api.exec<number>(Routes.groupIteration.getCurrentIterationId(channelId));
    const useThreads = await this.api.exec<boolean>(Routes.channel.getUseThreads(channelId));
    console.log("Using threads: ", !!useThreads);
    const group = await getGroup(useThreads, currentIterationId, event.thread_ts, msgId);

    console.log("ImgUploadFeatures: Group found:", group);

    if (!group) {
      throw Error("Group not found. Can't 'complete' group meetup.");
    } else {
      await this.api.execWithBody(Routes.group.updateIsComplete(group.id), {
        isCompleted: true,
      });
      group.isCompleted = true;
    }

    return group;
  }

  private isImage(files: any[]): boolean {
    return (files[0].mimetype as string).toLowerCase().includes("image");
  }

  private async repost(event: MessageEvent, token: string, group: Group) {
    const { channelId, msgId } = await this.getChannelAndMsgId(event);

    const useThreads = await this.api.exec<boolean>(Routes.channel.getUseThreads(channelId));

    const attachments: MessageAttachment[] = [];
    // TODO: The event type of slack bolt is severely lacking as of v.1.4.1, hence type is omitted for now
    event.files.forEach((element: any) => {
      const attachment: MessageAttachment = {
        title: element.title,
        image_url: element.url_private,
        thumb_url: element.thumb_360,
      };
      attachments.push(attachment);
    });

    const context = {
      channelId,
      groupId: group.id,
      authorMessage: { authorUserId: event.user, message: event.text },
    } as DynamicContentContext;
    const repostMsgTextPromise = this.textApi.get(TextType.RepostMsg, context);
    // Post message to channel for all to see.
    repostMsgTextPromise.then(repostMsgText => {
      this.app.client.chat.postMessage({
        token,
        channel: channelId,
        attachments,
        text: repostMsgText,
      });
    });

    const yourGroupCompletedText = await this.textApi.get(TextType.YourGroupCompleted, { channelId });

    // feedback to thread or private msg that this round is completed
    if (useThreads) {
      this.app.client.chat.postMessage({
        token,
        channel: channelId,
        thread_ts: group.thread_ts,
        text: yourGroupCompletedText,
      });
    } else {
      // Reply in the private message.
      this.app.client.chat.postMessage({
        token,
        channel: event.channel,
        text: yourGroupCompletedText,
      });
    }
  }
}
