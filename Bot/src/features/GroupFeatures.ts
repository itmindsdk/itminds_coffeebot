/* eslint-disable no-shadow */
/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { App, AckFn, RespondArguments, SayFn } from "@slack/bolt";
import { injectable, inject } from "tsyringe";
import {
  IApi,
  TextType,
  Routes,
  Channel,
  ChannelMember,
  ITextApi,
  TextApi,
  Group,
  GroupIteration,
  GroupMember,
  Schedule,
  Locale,
  VacationRequest,
  Api,
} from "common";
import IScheduleService from "../scheduling/interfaces/IScheduleService";
import { UsersInfo, MyWebAPICallResult } from "../types/Slack";
import { isInteger, getNextRunDate, generateGroups, formatDate } from "../helpers/misc";

/**
 * # Group features contain all group related features
 * group commands
 * create groups and create jobs for scheduler
 */
@injectable()
export default class GroupFeatures {
  private textApi: ITextApi;
  private api: IApi;

  constructor(private app: App, @inject("IScheduleService") private scheduleService: IScheduleService) {
    this.api = new Api(process.env.DATABROKER_URL);
    this.textApi = new TextApi(this.api, Locale.default);
  }

  public onGroupsCommand(): void {
    this.app.command("/groups", ({ command, ack, say, context }) => {
      // Check if user is admin
      this.app.client.users
        .info({
          token: context.botToken,
          user: command.user_id,
        })
        .then(res => res as UsersInfo)
        .then(async (res: UsersInfo) => {
          const commandText: string[] = command.text
            .trim()
            .toLowerCase()
            .split(" ");
          console.log(`COMMAND: ${command.command} `, commandText);
          if (commandText[0] === "create") {
            if (!res.user.is_admin) {
              const onBotJointText = await this.textApi.get(TextType.BotJoinText, { channelId: command.channel_id });
              ack(onBotJointText);
              return;
            }

            this.createGroups(command.channel_id, context.botToken);
            ack();
          } else if (commandText[0] === "details") {
            this.getGroupsDetails(ack, command.channel_id, command.user_id);
          } else if (commandText[0] === "size") {
            if (!commandText[1]) {
              const groupSizeText = await this.textApi.get(TextType.GroupSizeText, { channelId: command.channel_id });
              ack(groupSizeText);
            } else if (!res.user.is_admin) {
              const errorOnlyAdminGroupSizeText = await this.textApi.get(TextType.ErrorOnlyAdminGroupSize, {
                channelId: command.channel_id,
              });
              ack(errorOnlyAdminGroupSizeText);
            } else if (!isInteger(commandText[1])) {
              const errorNoIntText = await this.textApi.get(TextType.ErrorNotInt, { channelId: command.channel_id });
              ack({
                text: errorNoIntText,
                response_type: "ephemeral",
              });
            } else if (Number(commandText[1]) <= 2) {
              const errorGroupGreaterThanText = await this.textApi.get(TextType.ErrorGroupGreaterThan, {
                channelId: command.channel_id,
              });
              ack({
                text: errorGroupGreaterThanText,
                response_type: "ephemeral",
              });
            } else {
              await this.api.execWithBody(Routes.channel.updateGroupSize(command.channel_id), {
                groupSize: Number(commandText[1]),
              });
              const groupSizeText = await this.textApi.get(TextType.GroupSizeText, { channelId: command.channel_id });
              ack(groupSizeText);
            }
          } else {
            const errorUnknownCommandText = await this.textApi.get(TextType.ErrorUnknownCommandText, {
              channelId: command.channel_id,
            });
            ack({
              text: errorUnknownCommandText,
              response_type: "ephemeral",
            });
            console.log("Unknown command argument(s)");
          }
        });
    });
  }
  asyncFilter = (arr: ChannelMember[], filterFn: Function): Promise<ChannelMember[]> => {
    return new Promise<ChannelMember[]>(resolve => {
      const booleanArr = [];
      arr.forEach(e => {
        booleanArr.push(filterFn(e));
      });
      Promise.all(booleanArr).then(booleanArr => {
        const arr2 = arr.filter((e, i) => {
          return booleanArr[i];
        });
        resolve(arr2);
      });
    });
  };

  private async filterMembersOnVacation(members: ChannelMember[], channelId: string): Promise<ChannelMember[]> {
    const sch: Schedule = await this.api.exec(Routes.channel.getEventSchedule(channelId));
    const { runDate } = sch;
    const nextRunDate = getNextRunDate(sch.runDate, sch.eventInterval);

    // prøv ikke med filter dette virker ikke med async func.
    // Returns members which are not on vacation.
    return this.asyncFilter(members, async member => {
      const vacations = await this.api.exec<VacationRequest[]>(Routes.sursen.getVacationsForEmployee(), {
        email: member.mail as string,
      });

      if (vacations !== undefined) {
        const hasVacation = vacations.some(v => {
          const vStart = new Date(v.vacationStart).getTime();
          const vEnd = new Date(v.vacationEnd).getTime();
          const rStart = new Date(runDate).getTime();
          const rEnd = new Date(nextRunDate).getTime();
          return (
            (vStart >= rStart && vStart <= rEnd) || (vEnd >= rStart && vEnd <= rEnd) || (vStart < rStart && vEnd > rEnd)
          );
        });
        return !hasVacation;
      }
      return true;
    });
  }

  private async createGroups(channelId: string, botToken: string): Promise<void> {
    console.log("CREATING GROUPS!");

    // Function that creates groups for this iteration.
    const createGroups = async (channel: Channel) => {
      const prevGroups: Group[] | null = await this.api
        .exec<GroupIteration>(Routes.groupIteration.getCurrentIteration(channelId))
        .then(groupIterations => {
          if (groupIterations && groupIterations.groups) {
            return groupIterations.groups;
          }
          return null;
        });

      // Get all channel members.
      const channelMembers: ChannelMember[] = await this.api.exec<ChannelMember[]>(
        Routes.channelMember.getAll(channelId),
      );

      console.log("CHANNEL MEMBERS FOR GROUPS:", channelMembers);

      // Remove members currently on vacation.
      const channelMembersNotOnVacation = await this.filterMembersOnVacation(channelMembers, channelId);

      console.log("filtered: ", channelMembersNotOnVacation);

      if (prevGroups != null) {
        return generateGroups(channelMembersNotOnVacation, channel.groupSize, prevGroups);
      }
      return generateGroups(channelMembersNotOnVacation, channel.groupSize);
    };

    /**
     * Posts the created groups to the channel.
     * @returns Promises of groups with added thread_ts values.
     */
    // eslint-disable-next-line no-shadow
    const postGroupMessages = (botToken: string, channelId: string, groups: Group[]) => {
      return groups.map((group, index) => {
        const { members } = group;
        const groupMembersStr: string = members.reduce(
          (acc, member, mIndex) => `${mIndex > 0 ? ", " : " & "}<@${member.userId}>${acc}`,
          ".",
        );
        return this.app.client.chat
          .postMessage({
            token: botToken,
            channel: channelId,
            text: `Group ${index + 1}: ${groupMembersStr.slice(2, groupMembersStr.length)}`, // slice prefixed comma
          })
          .then(msg => {
            return { ...group, thread_ts: msg.ts } as Group;
          });
      });
    };

    /**
     * Creates and saves the group iteration to the database.
     * @returns Returns a promise for the created group iteration.
     */
    const createGroupIteration = async (channelId: string, groups: Group[]) => {
      const addGroupCaptainToGroup = (group: Group) => {
        const memberCount = group.members.length;
        const captainIndex: number = Math.floor(Math.random() * memberCount);
        return { ...group, captain: captainIndex } as Group;
      };

      const useCaptain = await this.api.exec<boolean>(Routes.channel.getUseCaptain(channelId));

      // If using coffee captains, update groups with a captain.
      const updatedGroups = useCaptain ? groups.map(group => addGroupCaptainToGroup(group)) : groups;

      const groupIteration: GroupIteration = {
        groups: updatedGroups,
        channelId,
      };

      const savedGroupIterationPromise = this.api.execWithBody<GroupIteration>(
        Routes.groupIteration.create(),
        groupIteration,
      );

      return savedGroupIterationPromise;
    };

    const postIndividualGroupWelcomeMessages = (channel: Channel, savedGroupIteration: GroupIteration) => {
      // Method for creating a welcome message for a group.
      const createWelcomeMsg = async (channelId: string, group: Group, useCaptain: boolean): Promise<string> => {
        const getCaptain = (group: Group) => {
          const captainIndex = group.captain as number;
          const captain = group.members[captainIndex];
          return captain;
        };

        let welcomeMsg = await this.textApi.get(TextType.WelcomeGroup, { channelId });

        if (useCaptain) {
          const cap = getCaptain(group);
          const whoIsCaptainTextPromise = this.textApi.get(TextType.WhoIsCaptain, {
            channelId,
            captainUserId: cap.userId,
          });
          const coffeeCaptainDescriptionTextPromise = this.textApi.get(TextType.CoffeeCaptainDescription, {
            channelId,
          });

          const [whoIsCaptainText, coffeeResponsibleText] = await Promise.all([
            whoIsCaptainTextPromise,
            coffeeCaptainDescriptionTextPromise,
          ]);

          welcomeMsg += "\n\n";
          welcomeMsg += whoIsCaptainText;
          welcomeMsg += "\n\n";
          welcomeMsg += coffeeResponsibleText;
        }

        return welcomeMsg;
      };

      // Method for sending welcome message to group and updating group msg id.
      const sendWelcomeAndUpdateGroup = async (channelId: string, group: Group, useThreads: boolean) => {
        const useCaptain = await this.api.exec<boolean>(Routes.channel.getUseCaptain(channelId));
        const welcomeMessage = await createWelcomeMsg(channelId, group, useCaptain);

        if (useThreads) {
          this.postThreadMsg(group.thread_ts as string, channelId, welcomeMessage, botToken);
        } else {
          this.postConversationMsg(botToken, group.members, welcomeMessage).then(msgId => {
            // TODO: Update group with msg ID here!
            return this.api.exec(Routes.group.updateMsgId(group.id, msgId));
          });
        }
      };

      const { groups } = savedGroupIteration;

      Promise.all(groups?.map(group => sendWelcomeAndUpdateGroup(channel.id, group, !!channel.useThreads)) ?? []);
    };

    // Create new groups for this iteration.
    const channel = await this.api.exec<Channel>(Routes.channel.get(channelId));
    const createdGroups = await createGroups(channel);

    console.log("GROUPS: ", createdGroups);

    // Post messages with groups, create group iteration and post a welcome message for each of the groups.
    const groupsWithThreadTsPromises = postGroupMessages(botToken, channelId, createdGroups);
    const groupsWithThreadTs = await Promise.all(groupsWithThreadTsPromises);
    const groupIteration = await createGroupIteration(channelId, groupsWithThreadTs);
    postIndividualGroupWelcomeMessages(channel, groupIteration);
  }

  private async postConversationMsg(botToken: string, members: GroupMember[], welcomeMsg: string): Promise<string> {
    const groupMembers = members.reduce((acc, member) => `${acc}${member.userId},`, "");
    return this.app.client.conversations
      .open({
        token: botToken,
        return_im: true,
        users: groupMembers.slice(0, groupMembers.length - 1),
      })
      .then(res => res as MyWebAPICallResult)
      .then((conversation: MyWebAPICallResult) => {
        this.app.client.chat.postMessage({
          token: botToken,
          channel: conversation.channel.id,
          text: welcomeMsg,
        });
        return conversation.channel.id;
      });
  }

  private postThreadMsg(msgts: string, channelId: string, welcomeMsg: string, botToken: string): void {
    this.app.client.chat.postMessage({
      token: botToken,
      channel: channelId,
      thread_ts: msgts,
      text: welcomeMsg,
    });
  }

  private async getGroupsDetails(
    ack: AckFn<string | RespondArguments>,
    channelId: string,
    userId: string,
  ): Promise<void> {
    console.log("GROUP ITERATION DETAILS REQUESTED!");
    const nextRunDate = await this.api.exec<Date>(Routes.channel.getNextRunDate(channelId));
    const groupIteration = await this.api.exec<GroupIteration>(Routes.groupIteration.getCurrentIteration(channelId));

    let outputMsg = `Deadline this round: ${formatDate(new Date(nextRunDate))}\n\n`;
    outputMsg += "On this date new groups will be created.\n\n\n";
    outputMsg += "Groups:\n\n";
    if (groupIteration.groups) {
      groupIteration.groups.forEach((group, i) => {
        const groupMembers = group.members.reduce(
          (acc, member, index) =>
            `${index > 0 ? ", " : " & "}<@${member.userId}>${acc}${
            member.userId === userId ? " <--- Your group!" : ""
            }`,
          ".",
        );
        outputMsg = `${outputMsg}Group ${i + 1}: ${groupMembers.slice(2, groupMembers.length)}\n`; // slice prefixed comma
      });
    } else {
      outputMsg += "No groups";
    }
    ack({ text: outputMsg, response_type: "ephemeral" });
  }

  public onScheduleCommand(): void {
    this.app.command("/schedule", async ({ command, ack, say, context }) => {
      const commandText: string[] = command.text
        .trim()
        .toLowerCase()
        .split(" ");
      console.log(`COMMAND: ${command.command} `, commandText);
      if (commandText[0] === "start") {
        this.scheduleStartExisting(command.channel_id, context.botToken, say, ack);
      } else if (commandText[0] === "stop") {
        this.scheduleStop(ack);
      } else if (commandText[0] === "set") {
        this.scheduleSet(ack);
      } else if (commandText[0] === "get") {
        this.scheduleGet(ack, command.channel_id);
      } else {
        const errorUnknownCommandText = await this.textApi.get(TextType.ErrorUnknownCommandText, {
          channelId: command.channel_id,
        });
        ack({
          text: errorUnknownCommandText,
          response_type: "ephemeral",
        });
        console.log("Unknown command argument(s)");
      }
    });
  }

  public async scheduleStartExisting(
    channelId: string,
    botToken: string,
    say?: SayFn,
    ack?: AckFn<string | RespondArguments>,
  ): Promise<void> {
    const schedule = await this.api.exec<Schedule>(Routes.channel.getEventSchedule(channelId));
    const { runDate } = schedule;
    try {
      this.scheduleService.createJob(`${channelId}_CreateGroups`, channelId, () => {
        this.createGroups(channelId, botToken).then(() => this.api.exec(Routes.channel.setNextRunDate(channelId)));
      });
      this.scheduleService.start();
      if (ack) {
        ack({
          text: `Scheduled events started! \n\n next run date: ${runDate}`,
          response_type: "in_channel",
        });
      }
      console.log("Scheduled events started! \n\n next run date: ", runDate);
    } catch (error) {
      if (ack) {
        ack({ text: "no jobs to run", response_type: "in_channel" });
      }
      console.error(error);
    }
  }

  private scheduleStop(ack: AckFn<string | RespondArguments>): void {
    this.scheduleService.stopAll();
    ack({ text: "Scheduled events stopped!", response_type: "in_channel" });
    console.log("Scheduled events stopped!");
  }

  public scheduleStopJob(channelId: string): void {
    this.scheduleService.removeJob(channelId);
  }

  private scheduleSet(ack: AckFn<RespondArguments>): void {
    ack({
      text: "setting schedule (NOT IMPLEMENTED!)",
      response_type: "ephemeral",
    });
    console.log("setting schedule");
  }

  private scheduleGet(ack: AckFn<RespondArguments>, channelId: string): void {
    const msg = this.scheduleService.isJobRunning(channelId)
      ? "Scheduler job is running"
      : "scheduler job is NOT running";
    ack({
      text: msg,
      response_type: "ephemeral",
    });
    console.log("setting schedule");
  }

  private async getNextRunDate(channelId: string): Promise<Date> {
    return this.api
      .exec<Schedule>(Routes.channel.getEventSchedule(channelId))
      .then(({ eventInterval, runDate }) => getNextRunDate(runDate, eventInterval));
  }
}
