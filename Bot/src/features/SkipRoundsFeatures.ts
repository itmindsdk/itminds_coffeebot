import { App } from "@slack/bolt";
import { injectable, inject } from "tsyringe";
import { IApi, Routes, Api } from "common";
import { isInteger } from "../helpers/misc";

@injectable()
export default class SkipRounds {
  private api: IApi;

  constructor(private app: App) {
    this.api = new Api(process.env.DATABROKER_URL);
  }

  public onSkipRoundsCommand(): void {
    this.app.command("/skiprounds", ({ command, ack, say }) => {
      ack();
      const skipRoundCount = command.text.trim();
      if (!isInteger(skipRoundCount)) {
        say("Not an integer");
      } else {
        this.api
          .exec(Routes.channelMember.update(command.channel_id, command.user_id), {
            skipRounds: skipRoundCount,
          })
          .then(() => {
            console.log("Skip Rounds result: ", "Success!");
            say(`You will not be put in a group for the next ${skipRoundCount} rounds.`);
          })
          .catch(err => console.error(err));
      }
    });
  }
}
