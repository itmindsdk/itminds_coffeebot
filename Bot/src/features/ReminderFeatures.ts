/* eslint-disable no-shadow */
/* eslint-disable no-lone-blocks */
/* eslint-disable default-case */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-return-await */
import { inject, injectable } from "tsyringe";
import { App } from "@slack/bolt";
import {
  Locale,
  IApi,
  Routes,
  ITextApi,
  Group,
  GroupIteration,
  ReminderType,
  ReminderRepeatType,
  Reminder,
  TextApi,
  TextType,
  Api,
  Schedule,
} from "common";
import { getRelativeDate } from "../helpers/misc";
import actionReminderBlocks from "../helpers/ReminderBlocks";
/* import GroupIteration from "~entities/mainDB/GroupIteration";
import Group from "~entities/mainDB/Group";
import Reminder from "~entities/mainDB/Reminder";
import Schedule from "~entities/mainDB/Schedule";
import actionReminderBlocks from "~helpers/ReminderBlocks";
import TextType from "~enums/TextType";
import ReminderType from "~enums/ReminderType";
import ReminderRepeatType from "~enums/ReminderRepeatType";
import Routes from "~api/Routes";
import IApi from "~api/IApi";
import ITextApi from "~api/ITextApi";
import TextApi from "~api/TextApi";
import Locale from "~enums/Locale"; */

/**
 * All the code concerning how reminders work.
 * Reminders will be triggered by a job in the scheduler. (look at: /src/scheduling/ScheduleServiceBase.ts)
 */
@injectable()
export default class ReminderFeatures {
  private textApi: ITextApi;
  private api: IApi;

  constructor(private app: App) {
    this.api = new Api(process.env.DATABROKER_URL);
    this.textApi = new TextApi(this.api, Locale.default);
  }

  /**
   * This function needs to be invoked in app.ts to enable the buttons in the action reminders.
   */
  public enableActionButtons() {
    this.app.action("confirm_group", async info => {
      info.ack();
      this.repost(
        info.body.channel.id,
        info.context.botToken,
        await this.updateGroupIteration(info.body.channel.id, info.body.user.id),
      );
      info.say(`Thanks <@${info.body.user.id}> for confiming 👍`);
    });
    this.app.action("deny_group", ({ ack, say }) => {
      ack();
      say("ok");
    });
  }

  private findThreadTsByUser(currentIteration: GroupIteration, userId: string): string | null | undefined {
    for (const group of currentIteration.groups as Group[]) {
      for (const member of group.members) {
        if (member.userId === userId) return group.thread_ts;
      }
    }
    return null;
  }

  private async updateGroupIteration(msgId: string, userId: string): Promise<Group> {
    const privateMessageChannelId = await this.api.exec<string>(Routes.group.getChannelIdFromMsgId(msgId));

    const channelId = privateMessageChannelId || msgId;

    const groupIteration = await this.api.exec<GroupIteration>(Routes.groupIteration.getCurrentIteration(channelId));

    const getGroupFetcher = async (
      messageId: string,
      channelId: string,
      userId: string,
      currentIteration: GroupIteration,
    ) => {
      const useThreads = await this.api.exec<boolean>(Routes.channel.getUseThreads(channelId));
      if (useThreads) {
        const threadTs = this.findThreadTsByUser(currentIteration, userId) as string;
        return async () => {
          // eslint-disable-next-line no-return-await
          return await this.api.exec<Group>(Routes.group.getByIterationAndThreads(currentIteration.id), {
            thread_ts: threadTs,
          });
        };
      }
      return async () => {
        return await this.api.exec<Group>(Routes.group.getByIterationIdAndMsgId(currentIteration.id, messageId));
      };
    };

    const groupFetcher = await getGroupFetcher(msgId, channelId, userId, groupIteration);
    const group = await groupFetcher();

    if (group) {
      await this.api.execWithBody(Routes.group.updateIsComplete(group.id), {
        isCompleted: true,
      });
      group.isCompleted = true;
    }

    return group;
  }

  private async repost(msgId: string, token: string, group: Group) {
    const privateMessageChannelId = await this.api.exec<string>(Routes.group.getChannelIdFromMsgId(msgId));

    const channelId = privateMessageChannelId || msgId;

    const useThreads = await this.api.exec<boolean>(Routes.channel.getUseThreads(channelId));

    const repostMsgTextPromise = this.textApi.get(TextType.RepostMsg, { channelId });
    repostMsgTextPromise.then(repostMsgText => {
      this.app.client.chat.postMessage({
        token,
        channel: channelId,
        text: repostMsgText,
      });
    });

    const groupCompleteText = await this.textApi.get(TextType.YourGroupCompleted, { channelId });

    // feedback to thread or private msg that this round is completed
    if (useThreads) {
      this.app.client.chat.postMessage({
        token,
        channel: channelId,
        thread_ts: group.thread_ts,
        text: groupCompleteText,
      });
    } else {
      this.app.client.chat.postMessage({
        token,
        channel: msgId,
        text: groupCompleteText,
      });
    }
  }

  public async runReminders(channelId: string) {
    const useReminders = await this.api.exec<boolean>(Routes.channel.getUseReminders(channelId));
    if (!useReminders) {
      return;
    }

    const notTriggeredReminders = await this.api.exec<Reminder[]>(Routes.reminder.getAllNotTrigged(channelId));

    notTriggeredReminders.forEach(reminder => {
      // TODO: Used to check: if (reminder.channel.id === 0), but channel is a string.
      if (!reminder.channel) return;

      if (Date.now() >= reminder.triggerDate.valueOf()) {
        switch (reminder.type) {
          case ReminderType.SIMPLE:
            this.simpleReminder(reminder);
            break;
          case ReminderType.ACTION:
            this.actionReminder(reminder);
            break;
        }
        this.resetForRepeat(reminder);
      }
    });
  }

  // TODO this is not tested it is mayby better to just let dem repeat like "normal" jobs.
  private async resetForRepeat(reminder: Reminder) {
    switch (reminder.repeatType as ReminderRepeatType) {
      case ReminderRepeatType.NONE:
        this.api.execWithBody(Routes.reminder.updateHasTriggered(reminder.id), {
          hasTriggered: true,
        });
        break;
      case ReminderRepeatType.BEFORE_END: // TODO: new type does not match this case
        {
          const schedule = await this.api.exec<Schedule>(Routes.channel.getEventSchedule(reminder.channel));
          const newTriggerDate = getRelativeDate(schedule.runDate, -reminder.RepeatDayNumber);

          this.api.execWithBody(Routes.reminder.updateTriggerDate(reminder.id), {
            triggerDate: newTriggerDate,
          });
        }
        break;
      case ReminderRepeatType.AFTER_START: // TODO: new type does not match this case
        {
          const schedule = await this.api.exec<Schedule>(Routes.channel.getEventSchedule(reminder.channel));
          const dateDiff = -schedule.eventInterval + reminder.RepeatDayNumber;
          const newTriggerDate = getRelativeDate(schedule.runDate, dateDiff);

          this.api.execWithBody(Routes.reminder.updateTriggerDate(reminder.id), {
            triggerDate: newTriggerDate,
          });

          /*   const newTriggerDate = oneDayBefore(reminder.triggerDate);
          this.api.execWithBody(Routes.reminder.updateTriggerDate(reminder.id), {
            triggerDate: newTriggerDate
          }); */
        }
        break;
    }
  }

  private async actionReminder(reminder: Reminder) {
    this.app.client.chat.postMessage({
      token: process.env.SLACK_BOT_TOKEN,
      channel: reminder.channel,
      text: reminder.text,
      blocks: (await actionReminderBlocks(this.textApi, reminder.channel)) as any[],
    });
  }

  private simpleReminder(reminder: Reminder) {
    this.app.client.chat.postMessage({
      token: process.env.SLACK_BOT_TOKEN,
      channel: reminder.channel,
      text: reminder.text,
    });
  }
}
