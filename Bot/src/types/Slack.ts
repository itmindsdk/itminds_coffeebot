import { ViewOutput } from "@slack/bolt";
import { WebAPICallResult } from "@slack/web-api";

export interface ViewOutputExtension extends ViewOutput {
  state: {
    values: {
      startDate: {
        startDateSelected: {
          selected_date: string;
        };
      };
      interval: {
        intervalSelected: {
          selected_option: {
            value: string;
          };
        };
      };
      groupSize: {
        groupSizeSelected: {
          selected_option: {
            value: string;
          };
        };
      };
      msgType: {
        msgTypeSelected: {
          selected_option: {
            value: string;
          };
        };
      };
      useCaptain: {
        useCaptainSelected: {
          selected_option: {
            value: string;
          };
        };
      };
    };
  };
}

export interface UsersInfo extends WebAPICallResult {
  user: {
    is_admin: boolean;
    profile: Profile;
  };
}

export interface Profile {
  email: string;
}

export interface MyWebAPICallResult extends WebAPICallResult {
  user: User;
  channel: Channel;
}

export interface ChannelInfo extends WebAPICallResult {
  channel: {
    id: string;
    name: string;
    is_channel: boolean;
    created: number;
    is_archived: boolean;
    is_general: boolean;
    unlinked: number;
    creator: string;
    name_normalized: string;
  };
}

export interface User {
  is_admin: boolean;
}

export interface Channel {
  id: string;
}

export interface Reminder {
  channel: Channel;
}
