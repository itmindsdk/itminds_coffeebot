export default interface BotConfig {
  useThreads: boolean;
  startDate: string;
  interval: number;
  groupSize: number;
  useCaptain: boolean;
}
