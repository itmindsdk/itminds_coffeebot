export type Job = {
  channelId: string;
  invoke: (nextRun: Date) => void;
};
