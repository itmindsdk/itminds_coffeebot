import "./loadEnv"; // this MUST be the topmost import
import "reflect-metadata";
import { container } from "tsyringe";
import "isomorphic-unfetch";
import { App, ExpressReceiver, LogLevel } from "@slack/bolt";
import { Channel, Api, Routes } from "common";
import ReminderFeatures from "./features/ReminderFeatures";
import GroupFeatures from "./features/GroupFeatures";
import ImgUploadFeatures from "./features/ImgUploadFeatures";
import SkipRounds from "./features/SkipRoundsFeatures";
import ChannelFeatures from "./features/ChannelFeatures";
import setContainers from "./helpers/SetupDI";

// Get environment variables.
const slackSigningSecret = process.env.SLACK_SIGNING_SECRET;
const slackBotToken = process.env.SLACK_BOT_TOKEN;
const port = process.env.PORT || 3000;
const databrokerUrl = process.env.DATABROKER_URL;

const receiver = new ExpressReceiver({
  signingSecret: slackSigningSecret,
});

const app = new App({
  token: slackBotToken,
  signingSecret: slackSigningSecret,
  logLevel: LogLevel.DEBUG,
  receiver,
});

// Added to test if the bot is running and HEALTHY! - It is a fit bot.
receiver.app.get("/health", (_, res) => {
  res.status(200).send(`
  <h1>I am healthy!</h1>
  <h2>Bot info:</h2>
  <table>
  <tr>
    <td>Port</td>
    <td>${port}</td>
  </tr>
  <tr>
    <td>Databroker URL</td>
    <td>${databrokerUrl}</td>
  </tr>
</table>
`); // respond 200 OK to the default health check method
});

receiver.app.all("/slack/events", event => {
  console.log("event", event);
});

// setup DI.
setContainers(app);

app.start(port).then(() => {
  // resolve features
  const reminder = container.resolve(ReminderFeatures);
  const group = container.resolve(GroupFeatures);
  const imgUpload = container.resolve(ImgUploadFeatures);
  const skipRounds = container.resolve(SkipRounds);
  const channelFeatures = container.resolve(ChannelFeatures);

  // put all other features here!
  channelFeatures.onJoinEvent();
  channelFeatures.onLeaveEvent();
  channelFeatures.setupModal();
  group.onGroupsCommand();
  group.onScheduleCommand();
  imgUpload.onFileSharedEvent();
  skipRounds.onSkipRoundsCommand();
  reminder.enableActionButtons();

  const api = new Api(process.env.DATABROKER_URL);
  api
    .exec<Channel[]>(Routes.channel.getAllActiveIds())
    .then((allActiveChannels: Channel[]) => {
      if (allActiveChannels !== undefined) {
        allActiveChannels.forEach(channel => {
          if (channel === undefined) {
            console.log("Channel from allActiveChannels is undefined...");
            return;
          }
          group.scheduleStartExisting(channel.id, process.env.SLACK_BOT_TOKEN);
        });
      }
    })
    .catch(error => console.error(error));

  console.log("⚡️ - SlackBot is alive! - ⚡️");
});
