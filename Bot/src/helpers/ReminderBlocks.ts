import { TextType, ITextApi } from "common";

/**
 * This function compute a action reminder.
 * @returns slack blocks
 */
export default async function actionReminderBlocks(textApi: ITextApi, channelId: string): Promise<{}[]> {
  return [
    {
      type: "section",
      text: {
        type: "mrkdwn",
        text: await textApi.get(TextType.ActionReminder, { channelId }),
      },
    },
    {
      type: "section",
      text: {
        type: "mrkdwn",
        text: await textApi.get(TextType.ActionReminderDetails, { channelId }),
      },
      accessory: {
        type: "image",
        image_url: "https://www.zwillgen.com/wp-content/uploads/2014/11/reminder-finger.jpg",
        alt_text: "Reminder thumbnail",
      },
    },
    {
      type: "actions",
      elements: [
        {
          type: "button",
          text: {
            type: "plain_text",
            emoji: true,
            text: "The meeting have been held",
          },
          style: "primary",
          value: "click_me_123",
          action_id: "confirm_group",
        },
        {
          type: "button",
          text: {
            type: "plain_text",
            emoji: true,
            text: "Nope not yet",
          },
          style: "danger",
          value: "click_me_123",
          action_id: "deny_group",
        },
      ],
    },
  ];
}
