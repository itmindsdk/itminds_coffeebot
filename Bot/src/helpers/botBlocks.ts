import { ViewsOpenArguments } from "@slack/web-api"; // eslint-disable-line import/no-extraneous-dependencies

export const botSetupButton = {
  type: "section",
  text: {
    type: "mrkdwn",
    text: "Hi, I am coffee bot! Configure me by pressing the button.",
  },
  accessory: {
    action_id: "doSetup", // When you press the button, it will hit the app.action('doSetup') in app.ts
    type: "button",
    text: {
      type: "plain_text",
      text: "Setup bot",
    },
  },
};

const selectStartDate = {
  type: "input",
  block_id: "startDate",
  element: {
    type: "datepicker",
    action_id: "startDateSelected",
    placeholder: {
      type: "plain_text",
      text: "Select start date of event",
    },
  },
  label: {
    type: "plain_text",
    text: "Start date",
  },
};

const selectInterval = {
  type: "input",
  block_id: "interval",
  element: {
    type: "static_select",
    action_id: "intervalSelected",
    placeholder: {
      type: "plain_text",
      text: "Select interval of event",
    },
    options: [
      {
        text: {
          type: "plain_text",
          text: "Every week",
        },
        value: "1",
      },
      {
        text: {
          type: "plain_text",
          text: "Every other week",
        },
        value: "2",
      },
      {
        text: {
          type: "plain_text",
          text: "Every third week",
        },
        value: "3",
      },
      {
        text: {
          type: "plain_text",
          text: "Every fourth week",
        },
        value: "4",
      },
    ],
  },
  label: {
    type: "plain_text",
    text: "Interval",
  },
};

const selectGroupSize = {
  type: "input",
  block_id: "groupSize",
  element: {
    type: "static_select",
    action_id: "groupSizeSelected",
    placeholder: {
      type: "plain_text",
      text: "Select group size",
    },
    options: [
      {
        text: {
          type: "plain_text",
          text: "2",
        },
        value: "2",
      },
      {
        text: {
          type: "plain_text",
          text: "3",
        },
        value: "3",
      },
      {
        text: {
          type: "plain_text",
          text: "4",
        },
        value: "4",
      },
      {
        text: {
          type: "plain_text",
          text: "5",
        },
        value: "5",
      },
    ],
  },
  label: {
    type: "plain_text",
    text: "Group size",
  },
};

const msgType = {
  type: "input",
  block_id: "msgType",
  element: {
    type: "static_select",
    action_id: "msgTypeSelected",
    placeholder: {
      type: "plain_text",
      text: "Select message type",
    },
    options: [
      {
        text: {
          type: "plain_text",
          text: "Use threads",
        },
        value: "thread",
      },
      {
        text: {
          type: "plain_text",
          text: "Use conversations",
        },
        value: "conversation",
      },
    ],
  },
  label: {
    type: "plain_text",
    text: "Message type",
  },
};

const useCaptain = {
  type: "input",
  block_id: "useCaptain",
  element: {
    type: "static_select",
    action_id: "useCaptainSelected",
    placeholder: {
      type: "plain_text",
      text: "Select if you want to have a Coffee Captain",
    },
    options: [
      {
        text: {
          type: "plain_text",
          text: "Have a Coffee Captain for each group",
        },
        value: "true",
      },
      {
        text: {
          type: "plain_text",
          text: "No Coffee Captain",
        },
        value: "false",
      },
    ],
  },
  label: {
    type: "plain_text",
    text: "Use Captain for each group",
  },
};

export function createBotSetupModal(args: any): ViewsOpenArguments {
  return {
    token: args.context.botToken,
    // Pass a valid trigger_id within 3 seconds of receiving it
    trigger_id: args.body.trigger_id,
    // View payload
    view: {
      type: "modal",
      callback_id: "botModalSubmitted", // When you press the submit button, it will hit the app.view('botModalSubmitted') in app.ts
      title: {
        type: "plain_text",
        text: "Coffe bot setup",
      },
      submit: {
        type: "plain_text",
        text: "Submit",
      },
      close: {
        type: "plain_text",
        text: "Cancel",
      },
      blocks: [selectStartDate, selectInterval, selectGroupSize, msgType, useCaptain],
    },
  };
}

export const haveMeetButton = {
  type: "section",
  text: {
    type: "mrkdwn",
    text: "text",
  },
  accessory: {
    action_id: "haveMeetAction",
    type: "button",
    text: {
      type: "plain_text",
      text: "Setup bot",
    },
  },
};
