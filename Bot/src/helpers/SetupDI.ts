import { App } from "@slack/bolt";
import { container } from "tsyringe";
import { Api } from "common";
import HttpScheduler from "../scheduling/schedulers/HttpScheduler";
import NodeScheduler from "../scheduling/schedulers/NodeScheduler";
import ScheduleService from "../scheduling/ScheduleService";

/**
 * Helper function setting up all the services to use in the DI.
 */
export default function setContainers(app: App): void {
  if (process.env.NODE_ENV === "production") {
    container.register("ScheduleListener", {
      useClass: HttpScheduler,
    });
  } else {
    container.register("ScheduleListener", {
      useClass: NodeScheduler,
    });
  }

  /*   container.register<Api>("IApi", {
    useValue: new Api(process.env.DATABROKER_URL)
  });  */

  /*   container.register("String", {
    useValue: new Api(process.env.DATABROKER_URL)
  }); */

  container.register("IScheduleService", {
    useClass: ScheduleService,
  });

  container.registerInstance(App, app);
}
