import { Group, GroupMember, ChannelMember } from "common";

export function getNextRunDate(runDate: Date, eventInterval: number): Date {
  const nextRunDate = new Date(runDate);
  nextRunDate.setDate(new Date(runDate).getDate() + eventInterval);
  return nextRunDate;
}

export function shuffleArray<T>(a: T[]): T[] {
  const b = a;
  for (let i = b.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    const x = b[i];
    b[i] = b[j];
    b[j] = x;
  }
  return b;
}

function GroupMembersToGroup(createdGroup: GroupMember[]): Group {
  return {
    isCompleted: false,
    members: createdGroup,
  } as Group;
}

function GroupMemberArraysToGroups(createdGroups: GroupMember[][]): Group[] {
  const groups = createdGroups.map(groupMembers => GroupMembersToGroup(groupMembers));
  return groups;
}

export function twoDArrayChannelMemberToGroupMember(groups: ChannelMember[][]): GroupMember[][] {
  const groupsWithGroupMembers = groups.map(channelMembers =>
    channelMembers.map(
      cm =>
        ({
          userId: cm.userId,
        } as GroupMember),
    ),
  );
  return groupsWithGroupMembers;
}

export function ChannelMemberToGroupMemberArray(channelMember: ChannelMember[]): GroupMember[] {
  return channelMember.map(
    cm =>
      ({
        userId: cm.userId,
      } as GroupMember),
  );
}

export function isInteger(number: string): boolean {
  return /^\d+$/.test(number);
}

export function formatDate(date: Date): string {
  const monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  const day = date.getDate();
  const monthIndex = date.getMonth();
  const year = date.getFullYear();

  return `${day} ${monthNames[monthIndex]} ${year}`;
}

export function getMidPointdate(runDate: Date, eventInterval: number): Date {
  return new Date(runDate.getTime() - (eventInterval * 7) / 2);
}

export function oneDayBefore(runDate: Date): Date {
  return new Date(new Date(runDate).getDate() - 1);
}

/**
 * Returns a date which is the given number of days before or after (depending on sign).
 * @param date Initial date.
 * @param days The days to move into the future or past relative to the initial date.
 */
export function getRelativeDate(date: Date, days: number): Date {
  return new Date(new Date(date).getDate() + days);
}

/**
 * Ensures that there will be at least one new group member in the generated groups (compared to prevGroups)
 */
export function generateGroups(channelMembers: ChannelMember[], groupSize: number, prevGroups?: Group[]): Group[] {
  // If amount of channelMembers are less than groupSize+1, just return channelMembers
  if (channelMembers.length <= groupSize + 1) {
    return [GroupMembersToGroup(ChannelMemberToGroupMemberArray(channelMembers))];
  }

  let membersNotYetInGroup: ChannelMember[] = channelMembers;
  const newGroups: ChannelMember[][] = [];

  const groupIncludesMember: (group: ChannelMember[], member: ChannelMember) => boolean = (group, member) =>
    group.some(m => m.userId === member.userId);

  shuffleArray(membersNotYetInGroup);

  while (membersNotYetInGroup.length > 0) {
    const memberToPutInGroup = membersNotYetInGroup.pop() as ChannelMember;
    // Find memberToPutInGroup's previous group (if they have any)
    const previousGroup = prevGroups
      ? prevGroups.find(group => groupIncludesMember(group.members, memberToPutInGroup))
      : null;
    let newGroup: ChannelMember[];

    // Member has not been in a previous group or there are no prevGroups
    if (!previousGroup) {
      newGroup = [memberToPutInGroup, ...membersNotYetInGroup.splice(0, groupSize - 1)];

      // Member has been in a previous group.
    } else {
      // Create new array of members that needs a group (without the members from memberToPutInGroups' last group)
      const withoutPrevGroupMembers = membersNotYetInGroup.filter(
        member => !groupIncludesMember(previousGroup.members, member),
      );

      newGroup = [memberToPutInGroup, ...withoutPrevGroupMembers.splice(0, groupSize - 1)];

      // Remove the group members that just got put in a group from newGroupMembers
      membersNotYetInGroup = membersNotYetInGroup.filter(member => !groupIncludesMember(newGroup, member));
    }

    // If there is a single member left, put them in newGroup
    if (membersNotYetInGroup.length === 1) {
      newGroup.push(membersNotYetInGroup.pop() as ChannelMember);
    }

    newGroups.push(newGroup);
  }

  // Create Group objects with groups.
  const groupMembers = twoDArrayChannelMemberToGroupMember(newGroups);
  return GroupMemberArraysToGroups(groupMembers);
}
