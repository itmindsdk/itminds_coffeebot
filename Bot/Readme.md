## Overblik

Workspace'et består af 4 projekter

- Bot - som er ret langt i udviklingen.
- Scheduler - som anvendes af botten når den køres lokalt (færdig).
- Frontend - som lige er påbegyndt og mere eller mindre er template kode.
- Databroker - som håndtere alt adgang til databasen og udstiller det som et REST Api

Læs dette document, der beskriver nogle basale ting i forhold til opsætning af botten, arkitekturen har ændret sig lidt i forhold til hvad det står i dokumentet:
https://docs.google.com/document/d/1HQZ-oDAcik8bD4BYRTuePVQG8uA8q-M_cK3ZvGLWdXY/edit?usp=sharing

# Vær opmæksom på:

- Det er vigtigt at alle commands og events er sat op korrekt i slack app indstillinger, dokumentet på google drev beskriver dette. Læs gerne slacks egen dokumentation https://api.slack.com/apps.
- Udfyld .env filen udfra .env.sample
- Hver opmærksom på at der findes to ormconfig.js i Databrokeren filer til at konfigurere typeorm. (ormconfig.js - til lokal og ormconfigAzure.js som anvendes på Azure)

# botten

Vi Anvedner slack bolt (https://slack.dev/bolt) til at kommunikere med slack's api.
Der er mange kommentarer i koden på bot projeket og det burde derfor være til at gå til.

Hele opsætningen af appen forgår i app.ts så start evt. med at se her.

Der bliver anvendt TypeORM (https://typeorm.io/) til håndtering af databaserne, bottens database er MSSQL i projektet er den henvist til som "MainDB" mens Sursen bruger MSSQL og hedder blot "Sursen" i projektet.

Projektet anvender dependency injection med tsyringe (https://github.com/microsoft/tsyringe) al setup kan ses i Helpers/SetupHelper.ts.

Der er en række TODO's i koden så start evt. med dem.

# scheduleren

Denne bruges kun når projektet køres lokalt. Azure web jobs bruges i deployment, se: https://docs.microsoft.com/en-us/azure/app-service/webjobs-create

# Frontend

Jeg nået kun lige at starte på frontenden da jeg fik en anden opgave. Der mangler at blive udformet et API som denne frontend kan hive data fra.
Jeg har valgt at bruge ionic framework (https://ionicframework.com/) men i og med jeg blot lige er startet kan dette bare laves om.

## Deployment

Databroker : https://data-1dr887vnv.azurewebsites.net/swagger-html
Bot: http://bot-1dr887vnv.azurewebsites.net/health

Common NPM pack on: https://www.myget.org/feed/Packages/itm-npm
