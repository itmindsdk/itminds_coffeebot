import schedule from "node-schedule";
import fetch from "isomorphic-unfetch";
import express from "express";

require("dotenv").config();

const cronConfig = process.env.CRON_CONFIG as string;
const botUrl = process.env.BOT_URL as string;
const port = process.env.PORT;

console.log("⚡️ - Scheduler started - ⚡️");
console.log("Cron config: ", cronConfig);
console.log("Bot url: ", botUrl);

const app = express();

app.get("/", (req, res) => {
  res.status(200);
});

app.get("/health", (req, res) => {
  res.status(200).send(`
  <h1>I am healthy!</h1>
  <h2>Scheduler info:</h2>
  <table>
  <tr>
    <td>Cron configuration</td>
    <td>${cronConfig}</td>
  </tr>
  <tr>
    <td>Bot URL</td>
    <td>${botUrl}</td>
  </tr>
</table>
`); // respond 200 OK to the default health check method
});

app.listen(port, () => console.log(`/health endpoint listening on port ${port}!`));

schedule.scheduleJob("Check tick", cronConfig, () => {
  console.log("Sending request! ", new Date().toISOString());

  fetch(botUrl).catch(e => {
    console.error(e);
  });
});
