export interface AppPage {
  url: string;
  icon: { ios: string; md: string };
  title: string;
  relation: AppType;
  channelId: string;
}

export interface BotApp {
  type: AppType;
  imgUrl: any;
  title: string;
}

export enum AppType {
  PROFILE,
  COFFEE_BUDDIES,
  TEAM_LEAD
}
