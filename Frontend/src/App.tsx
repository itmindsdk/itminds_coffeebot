import React from "react";
import { useDispatch } from "react-redux";
import { Route } from "react-router-dom";
import { IonApp, IonRouterOutlet, IonSplitPane } from "@ionic/react";
import { IonReactRouter } from "@ionic/react-router";
import { home, list, contact } from "ionicons/icons";
import { getNamePrefix } from "./utils/routerNameHelper";
import NotFound from "./pages/NotFound";
import Profile from "./pages/Profile";
import { AppPage, AppType, BotApp } from "./declarations";

import Menu from "./components/menu/Menu";
import Home from "./pages/Home";
import ChannelSettings from "./pages/ChannelSettings/ChannelSettings";

/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";

/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";

/* Optional CSS utils that can be commented out */
import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";

/* Theme variables */
import "./theme/variables.css";
import { selectedApp } from "./redux/slices/appSelectedSlice";

const channels: AppPage[] = [
  {
    title: "Team lead helper",
    url: "/Channel/Seniordev",
    icon: home,
    relation: AppType.TEAM_LEAD,
    channelId: "",
  },
  {
    title: "Team lead helper",
    url: "/Channel/Teamlead",
    icon: list,
    relation: AppType.TEAM_LEAD,
    channelId: "",
  },
  {
    title: "Profile information",
    url: "/profile/info",
    icon: contact,
    relation: AppType.PROFILE,
    channelId: "",
  },
];

export const apps: BotApp[] = [
  {
    title: "Profile",
    type: AppType.PROFILE,
    imgUrl: "https://cdn1.vectorstock.com/i/1000x1000/51/05/male-profile-avatar-with-brown-hair-vector-12055105.jpg",
  },
  {
    title: "Coffie buddies",
    type: AppType.COFFEE_BUDDIES,
    imgUrl: "/bot.png",
  },
  {
    title: "Team lead helper",
    type: AppType.TEAM_LEAD,
    imgUrl: "https://gravatar.com/avatar/dba6bae8c566f9d4041fb9cd9ada7741?d=identicon&f=y",
  },
];

const App: React.FC = () => {
  const dispatch = useDispatch();
  return (
    <IonApp>
      <IonReactRouter>
        <IonSplitPane contentId="main">
          <Menu appPages={channels} apps={apps} appClick={appType => dispatch(selectedApp(Number(appType)))} />
          <IonRouterOutlet id="main">
            <Route
              path={[`/${getNamePrefix(AppType.PROFILE)}/`, `${getNamePrefix(AppType.PROFILE)}/info`, "/"]}
              component={Profile}
            />
            <Route path={[`/${getNamePrefix(AppType.COFFEE_BUDDIES)}/:id`]} component={ChannelSettings} />
            <Route path={[`/${getNamePrefix(AppType.COFFEE_BUDDIES)}/`]} exact={true} component={Home} />
            <Route path={[`/404`]} component={NotFound} />
            {/*  <Route path="/" render={() => <Redirect to="/profile/info" exact={true} />} /> */}
          </IonRouterOutlet>
        </IonSplitPane>
      </IonReactRouter>
    </IonApp>
  );
};

export default App;
