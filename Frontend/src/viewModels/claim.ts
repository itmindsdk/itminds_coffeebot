interface Claim {
  typ: string;
  val: string;
}

export default Claim;
