import Claim from "./claim";

interface ProfileInfo {
  access_token: string;
  expires_on: string;
  id_token: string;
  provider_name: string;
  user_claims: Claim[];
  user_id: string;
}

export default ProfileInfo;
