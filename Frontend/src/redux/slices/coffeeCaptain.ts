import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { AppThunk } from "../store";

interface CoffeeSettingsData {
  isFeatureEnabled: boolean;
  error: string | null;
}
const initialState: CoffeeSettingsData = {
  isFeatureEnabled: true,
  error: null,
};

const GeneralSettings = createSlice({
  name: "GeneralSettingsData",
  initialState,
  reducers: {
    getGeneralSettingsSuccess: (state, action: PayloadAction<CoffeeSettingsData>): void => {
      /* state.eventInterval = action.payload; */
      state.error = null;
    },
    getGeneralSettingsFailed: (state, action: PayloadAction<string>): void => {
      state.error = action.payload;
    },
  },
});

export const {
  getGeneralSettingsSuccess: getRepoDetailsSuccess,
  getGeneralSettingsFailed: getRepoDetailsFailed,
} = GeneralSettings.actions;

export default GeneralSettings.reducer;

export const postCoffieCapFeature = (channelId: string, isOn: boolean): AppThunk => async (dispatch): Promise<void> => {
  try {
    /*     const api = new Api(process.env.DATABROKER_URL);
    const useCaptain = await api.exec(routes.channel.(channelId)); */
    dispatch(
      getRepoDetailsSuccess({
        isFeatureEnabled: isOn,
        error: null,
      }),
    );
  } catch (err) {
    dispatch(getRepoDetailsFailed(err.toString()));
  }
};
