import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Channel, Api, Routes } from "common";
import { AppThunk } from "../store";

export interface ChannelData {
  loading: boolean;
  channels: Channel[];
  channelId: string;
}

const initialState: ChannelData = {
  loading: false,
  channels: [] as Channel[],
  channelId: "",
};

const channelSlice = createSlice({
  name: "channels",
  initialState,
  reducers: {
    fetchStarted(state): void {
      state.loading = true;
    },
    fetchSuccess(state, action: PayloadAction<Channel[]>): void {
      state.channels = action.payload;
      state.loading = false;
    },
    setChannelId(state, action: PayloadAction<string>): void {
      state.channelId = action.payload;
    },
  },
});

export const { fetchSuccess, fetchStarted, setChannelId } = channelSlice.actions;

export const fetchChannel = (): AppThunk => async (dispatch): Promise<void> => {
  try {
    dispatch(fetchStarted());
    const api = new Api(String(process.env.DATABROKER_URL));
    api
      .exec(Routes.channel.getAll())
      .then((data) => {
        dispatch(fetchSuccess(data as Channel[]));
      })
      .catch((e) => console.log(e));
  } catch (error) {
    console.debug(error);
  }
};

export default channelSlice.reducer;
