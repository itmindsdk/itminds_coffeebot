import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import GeneralSettings from "../../components/GeneralSettings/GeneralSettings";
import { SettingPanel } from "../../pages/ChannelSettings/ChannelSettings";
import CoffeeCapSettings from "../../components/CoffeeCapSettings/CoffeeCapSettings";
import ReminderSettings from "../../components/ReminderSettings";

const settingsListSlice = createSlice({
  name: "settingsListSlice",
  initialState: {
    allSettingPanels: [
      {
        element: GeneralSettings,
        featureEnabled: true,
        tags: [
          "general",
          "dato",
          "start",
          "week",
          "group",
          "size",
          "send",
          "message",
          "threads",
          "text",
        ],
      } as SettingPanel,
      {
        element: CoffeeCapSettings,
        featureEnabled: true,
        tags: ["coffee", "captain", "custom"],
      } as SettingPanel,
      {
        element: ReminderSettings,
        featureEnabled: true,
        tags: ["reminders", "custom", "text"],
      } as SettingPanel,
    ],
  },
  reducers: {
    setPanels: (state, action: PayloadAction<SettingPanel[]>): void => {
      state.allSettingPanels = action.payload;
    },
  },
});

export const { setPanels } = settingsListSlice.actions;

export default settingsListSlice.reducer;
