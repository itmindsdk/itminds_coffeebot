import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Api, Routes as routes } from "common";
import { isUndefined } from "util";
import { AppThunk } from "../store";

export interface GeneralSettingsData {
  useThreads: boolean;
  groupSize: number;
  eventInterval: number;
  error: string | undefined | null;
  runDate: Date;
}

export interface EventInterval {
  eventInterval: number;
  runDate: Date;
}

const initialState: GeneralSettingsData = {
  useThreads: false,
  groupSize: 2,
  eventInterval: 1,
  error: null,
  runDate: new Date(),
};

const GeneralSettings = createSlice({
  name: "GeneralSettingsData",
  initialState,
  reducers: {
    getGeneralSettingsSuccess(state, action: PayloadAction<GeneralSettingsData>): void {
      state.eventInterval = action.payload.eventInterval;
      state.groupSize = action.payload.groupSize;
      /*     state.joke = action.payload.joke; */
      state.useThreads = action.payload.useThreads;
      state.runDate = action.payload.runDate;
      state.error = null;
    },
    getGeneralSettingsFailed(state, action: PayloadAction<string>): void {
      state.eventInterval = -1;
      state.groupSize = -1;
      state.useThreads = false;
      state.error = action.payload;
      state.runDate = new Date();
    },
  },
});

export const {
  getGeneralSettingsSuccess: getRepoDetailsSuccess,
  getGeneralSettingsFailed: getRepoDetailsFailed,
} = GeneralSettings.actions;

export default GeneralSettings.reducer;

export const fetchGeneralSettings = (channelId: string): AppThunk => async (dispatch): Promise<void> => {
  try {
    let useThreads = true;
    let groupSize = 5;
    let eventInterval: EventInterval = {
      eventInterval: 5,
      runDate: new Date(),
    };

    if (!isUndefined(channelId)) {
      const api = new Api(String(process.env.DATABROKER_URL));
      useThreads = await api.exec(routes.channel.getUseThreads(channelId));
      groupSize = await api.exec(routes.channel.getGroupSize(channelId));
      eventInterval = await api.exec(routes.channel.getEventSchedule(channelId));

      dispatch(
        getRepoDetailsSuccess({
          useThreads,
          error: null,
          groupSize,
          eventInterval: eventInterval.eventInterval,
          runDate: eventInterval.runDate,
        }),
      );
    } else {
      dispatch(getRepoDetailsFailed("channel id was undefined"));
    }
  } catch (err) {
    dispatch(getRepoDetailsFailed(err.toString()));
  }
};

export const postGeneralSettings = (channelId: string, data: GeneralSettingsData): AppThunk => async (dispatch): Promise<void> => {
  try {
    const api = new Api(String(process.env.DATABROKER_URL));
    console.log("URL ", process.env.DATABROKER_URL, "POST data: ", data);
    if (process.env.NODE_ENV !== "production") {
      await api.execWithBody(routes.channel.updateConfig(channelId), data);
    }

    dispatch(getRepoDetailsSuccess(data));
  } catch (err) {
    dispatch(getRepoDetailsFailed(err.toString()));
  }
};
