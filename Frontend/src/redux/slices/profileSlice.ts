import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { isUndefined } from "util";
import { AppThunk } from "../store";
import ProfileInfo from "../../viewModels/ProfileInfo";

export interface ProfileInfoData {
  loading: boolean;
  profileInfo: ProfileInfo | undefined;
}

const initialState: ProfileInfoData = {
  loading: false,
  profileInfo: undefined,
};

const profileSlice = createSlice({
  name: "profileInfo",
  initialState,
  reducers: {
    fetchStarted(state): void {
      state.loading = true;
    },
    fetchSuccess(state, action: PayloadAction<ProfileInfo>): void {
      state.loading = false;
      state.profileInfo = action.payload;
    },
    setProfileInfo(state, action: PayloadAction<ProfileInfo>): void {
      state.profileInfo = action.payload;
    },
  },
});

export const { fetchSuccess, fetchStarted, setProfileInfo } = profileSlice.actions;

export default profileSlice.reducer;

export const fetchProfileInfo = (): AppThunk => async (dispatch): Promise<void> => {
  try {
    dispatch(fetchStarted());
    let profileInfo: ProfileInfo[] | undefined;
    if (process.env.NODE_ENV === "development") {
      profileInfo = [
        {
          access_token:
            "ya29.ImC6B7wGbv3DuSWkCcjjGybQ8WcwK5-mj7thgw4My0C6phZdGkHmslu9dic5H05CG0Z1hHmTao8clT_ZXbsS_9FeCqaHUf0ghpVNyg7G70AOBQomCchKDPOr_yx7p-dG1Wc",
          expires_on: "2020-01-17T11:34:47.9376615Z",
          id_token:
            "eyJhbGciOiJSUzI1NiIsImtpZCI6IjVlZGQ5NzgyZDgyMDQwM2VlODUxOGM0YWFiYjJiOWZlMzEwY2FjMTIiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJhenAiOiIxMDExNjAyMjY4MjU3LTcwbzlpbHQwcW1zOG8zZGwydXFtZGg1YWZ1cmJyNzMyLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiYXVkIjoiMTAxMTYwMjI2ODI1Ny03MG85aWx0MHFtczhvM2RsMnVxbWRoNWFmdXJicjczMi5hcHBzLmdvb2dsZXVzZXJjb250ZW50LmNvbSIsInN1YiI6IjEwNDU4MTY1NTE3NDE5NDQwMjE5MSIsImhkIjoiaXQtbWluZHMuZGsiLCJlbWFpbCI6InRsdUBpdC1taW5kcy5kayIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJhdF9oYXNoIjoiR0lxZ0d4WHNSNUxvWUJ2Yy0yd1NCdyIsIm5hbWUiOiJUcm9lbHMgTHVuZCIsInBpY3R1cmUiOiJodHRwczovL2xoNC5nb29nbGV1c2VyY29udGVudC5jb20vLTkxR1prTzRHcVY4L0FBQUFBQUFBQUFJL0FBQUFBQUFBQUFBL0FDSGkzcmRMRTMxVzB1YWtKQl95bTJUeENRQW5kY1JsUEEvczk2LWMvcGhvdG8uanBnIiwiZ2l2ZW5fbmFtZSI6IlRyb2VscyIsImZhbWlseV9uYW1lIjoiTHVuZCIsImxvY2FsZSI6ImRhIiwiaWF0IjoxNTc5MjU3Mjg5LCJleHAiOjE1NzkyNjA4ODl9.c5eAxP1GaWSn4Jr841vvEqFixsietqn6mGuRSxnanh_2_4zf1CZIOJuuVUMRuJYuT6DK__3mjpf_Cz3L3HQhQhWmsBfhCMQIBBRAPwuHVbhJKPtqSQntwvJsbItwTw0WrvSbMAiFjNThIc3kHawJNz33BLVZq-aWU55b3YFt1D2QRsd860i25SVL6SuM4vfoQRTR6dtQrXvE1u2oNmq7A45F4qa6raB7MOe0Q5oqkzvqzTOz7BXR7FtCKbLbGNNVfjkrNemxp-EQikNj7d6ZYVymZfzUdLut8C10ZwCmZxCZgO-0U7gK5_4ADV5rohj-mybQwvUmrH3jElKRLeDy3Q",
          provider_name: "google",
          user_claims: [
            { typ: "iss", val: "https://accounts.google.com" },
            { typ: "azp", val: "1011602268257-70o9ilt0qms8o3dl2uqmdh5afurbr732.apps.googleusercontent.com" },
            { typ: "aud", val: "1011602268257-70o9ilt0qms8o3dl2uqmdh5afurbr732.apps.googleusercontent.com" },
            {
              typ: "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier",
              val: "104581655174194402191",
            },
            { typ: "hd", val: "it-minds.dk" },
            { typ: "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress", val: "tlu@it-minds.dk" },
            { typ: "email_verified", val: "true" },
            { typ: "at_hash", val: "GIqgGxXsR5LoYBvc-2wSBw" },
            { typ: "name", val: "Troels Lund" },
            {
              typ: "picture",
              val:
                "https://lh4.googleusercontent.com/-91GZkO4GqV8/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3rdLE31W0uakJB_ym2TxCQAndcRlPA/s96-c/photo.jpg",
            },
            { typ: "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname", val: "Troels" },
            { typ: "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname", val: "Lund" },
            { typ: "locale", val: "da" },
            { typ: "iat", val: "1579257289" },
            { typ: "exp", val: "1579260889" },
          ],
          user_id: "tlu@it-minds.dk",
        },
      ] as ProfileInfo[];
    } else {
      profileInfo = (await (await fetch("/.auth/me")).json()) as ProfileInfo[];
    }

    /*    const api = new Api(String(process.env.DATABROKER_URL));
    const slackId: string = await api.exec(Routes.frontend.getSlackUserId("", profileInfo[0].user_id)); */

    if (!isUndefined(profileInfo)) {
      /*   profileInfo[0].slack_id = slackId; */
      dispatch(fetchSuccess(profileInfo[0] as ProfileInfo));
    }
  } catch (error) {
    console.debug(error);
  }
};
