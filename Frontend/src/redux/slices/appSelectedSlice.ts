import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { AppType } from "../../declarations";

const appSelectedSlice = createSlice({
  name: "appType",
  initialState: {
    selected: 0,
  },
  reducers: {
    selectedApp(state, action: PayloadAction<AppType>): void {
      const appType = action.payload;
      state.selected = appType;
    },
  },
});

export const { selectedApp } = appSelectedSlice.actions;

export default appSelectedSlice.reducer;
