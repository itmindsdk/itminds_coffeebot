import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Reminder, Api, Routes as routes } from "common";
import { isUndefined } from "util";
import { ReminderItemData } from "../../components/ReminderSettings/ReminderSettings";
import { AppThunk } from "../store";

interface RemindersData {
  isFeatureEnabled: boolean;
  listOfReminders: ReminderItemData[];
  error: string | null;
  loading?: boolean;
}

const initialState: RemindersData = {
  isFeatureEnabled: false,
  listOfReminders: [],
  error: null,
  loading: false,
};

const RemindersSettings = createSlice({
  name: "GeneralSettingsData",
  initialState,
  reducers: {
    fetchStated(state): void {
      state.loading = true;
    },
    setReminders(state, action: PayloadAction<ReminderItemData[]>): void {
      state.listOfReminders = action.payload;
    },
    getRemindersSuccess(state, action: PayloadAction<RemindersData>): void {
      state.loading = false;
      state.isFeatureEnabled = action.payload.isFeatureEnabled;
      state.listOfReminders = action.payload.listOfReminders;
      state.error = null;
    },
    getRemindersFailed(state, action: PayloadAction<string>): void {
      state.loading = false;
      state.error = action.payload;
      /*  dispatch(getRemindersList()) */
    },
    FailedToCreateItem(state, action: PayloadAction<string>): void {
      state.error = action.payload;
    },
    successCreateItem(state, action: PayloadAction<string>): void {},
    deleteItemSuccess(state, action: PayloadAction<string>): void {},
    failedToDeleteItem(state, action: PayloadAction<string>): void {
      state.error = action.payload;
    },
    FailedToGetFeatureEnabled(state, action: PayloadAction<string>): void {
      state.error = action.payload;
    },
    getFeatureEnabledSuccess(state, action: PayloadAction<RemindersData>): void {
      state.isFeatureEnabled = action.payload.isFeatureEnabled;
      state.error = null;
    },
    /*     updateObjectInArray(state, action: PayloadAction<Number>): void {
      return state.listOfReminders.map((item, index) => {
        if (index !== action.payload) {
          // This isn't the item we care about - keep it as-is
          return item;
        }
        if (!item.saved && !item.toBeDeleted) {
          item.channel = id;
          console.debug("Saved item: ", item);
        } else if (item.toBeDeleted) {
          console.debug("Deleted item: ", item);
        }
        item.saved = true;
        item.toBeDeleted = false;
        return item;
      });
    } */
  },
});

export const {
  getRemindersSuccess: getSuccess,
  getRemindersFailed: getFailed,
  FailedToCreateItem: createFailed,
  failedToDeleteItem: deleteFailed,
  getFeatureEnabledSuccess: FeatureEnabledSuccess,
  FailedToGetFeatureEnabled: FeatureEnabledFailed,
  successCreateItem,
  setReminders,
  fetchStated,
  deleteItemSuccess,
} = RemindersSettings.actions;

export default RemindersSettings.reducer;

export const getRemindersList = (channelId: string): AppThunk => async (dispatch): Promise<void> => {
  try {
    dispatch(fetchStated());
    const api = new Api(String(process.env.DATABROKER_URL));

    if (!isUndefined(channelId)) {
      const useReminders: boolean = await api.exec(routes.channel.getUseReminders(channelId));
      const listOfReminders: ReminderItemData[] = await api.exec(routes.reminder.getAll(channelId));

      listOfReminders.forEach(item => {
        item.saved = true;
        item.toBeDeleted = false;
      });
      dispatch(
        getSuccess({
          loading: false,
          isFeatureEnabled: useReminders,
          listOfReminders,
          error: null,
        }),
      );
    } else {
      dispatch(getFailed("channel id was undefined"));
    }
  } catch (err) {
    dispatch(getFailed(err.toString()));
  }
};

export const deleteReminder = (reminderId: number): AppThunk => async (dispatch): Promise<void> => {
  try {
    const api = new Api(String(process.env.DATABROKER_URL));
    await api.exec(routes.reminder.deleteById(reminderId));
    dispatch(deleteItemSuccess(""));
  } catch (err) {
    dispatch(deleteFailed(err.toString()));
  }
};

export const createReminder = (reminder: ReminderItemData): AppThunk => async (dispatch): Promise<void> => {
  try {
    const newReminder: Reminder = {
      type: reminder.type,
      RepeatDayNumber: reminder.RepeatDayNumber,
      channel: reminder.channel,
      text: reminder.text,
      haveTriggered: reminder.haveTriggered,
      triggerDate: reminder.triggerDate,
      repeatType: reminder.repeatType,
    };

    const api = new Api(String(process.env.DATABROKER_URL));
    await api.execWithBody(routes.reminder.create(), newReminder);
    dispatch(successCreateItem(""));
  } catch (err) {
    dispatch(createFailed(err.toString()));
  }
};

export const setEnabled = (enabled: boolean, channelId: string): AppThunk => async (dispatch): Promise<void> => {
  try {
    const api = new Api(String(process.env.DATABROKER_URL));

    const obj = {
      useReminders: enabled,
    };

    await api.execWithBody(routes.channel.updateUseReminders(channelId), obj);

    const listOfReminders: ReminderItemData[] = await api.exec(routes.reminder.getAll(channelId));
    const useReminders: boolean = await api.exec(routes.channel.getUseReminders(channelId));

    dispatch(
      FeatureEnabledSuccess({
        isFeatureEnabled: useReminders,
        listOfReminders,
        error: null,
      }),
    );
  } catch (err) {
    dispatch(FeatureEnabledFailed(err.toString()));
  }
};
