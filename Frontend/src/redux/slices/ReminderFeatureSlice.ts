import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Api, Routes } from "common";
import { isUndefined } from "util";
import { AppThunk } from "../store";

export interface IsFeatureEnabled {
  isFeatureEnabled: boolean;
  error: string | null;
}

const initialState: IsFeatureEnabled = {
  isFeatureEnabled: false,
  error: null,
};

const RemindersSettingsEnabled = createSlice({
  name: "GeneralSettingsData",
  initialState,
  reducers: {
    getRemindersSuccess(state, action: PayloadAction<IsFeatureEnabled>): void {
      state.isFeatureEnabled = action.payload.isFeatureEnabled;
      state.error = null;
    },
    getRemindersFailed(state, action: PayloadAction<string>): void {
      state.error = action.payload;
    },
  },
});

export const { getRemindersSuccess: getSuccess, getRemindersFailed: getFailed } = RemindersSettingsEnabled.actions;

export default RemindersSettingsEnabled.reducer;

export const getUseReminders = (channelId: string): AppThunk => async (dispatch): Promise<void> => {
  try {
    if (isUndefined(channelId)) {
      const api = new Api(String(process.env.DATABROKER_URL));
      const useReminders: boolean = await api.exec(Routes.channel.getUseReminders(channelId));

      dispatch(
        getSuccess({
          isFeatureEnabled: useReminders,
          error: null,
        }),
      );
    } else {
      dispatch(getFailed("channel id was undefined"));
    }
  } catch (err) {
    dispatch(getFailed(err.toString()));
  }
};
