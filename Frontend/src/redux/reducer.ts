import { combineReducers } from "@reduxjs/toolkit";
import appSelectedReducer from "./slices/appSelectedSlice";
import settingsListReducer from "./slices/settingsListSlice";
import generalSettingsReducer from "./slices/GeneralSettingsSlice";
import ReminderReducer from "./slices/RemindersSlice";
import RemindersFeature from "./slices/ReminderFeatureSlice";
import channelReducer from "./slices/channelSlice";
import profileReducer from "./slices/profileSlice";

const rootReducer = combineReducers({
  appSelected: appSelectedReducer,
  settingsList: settingsListReducer,
  GeneralSettings: generalSettingsReducer,
  ReminderList: ReminderReducer,
  channelList: channelReducer,
  profileInfo: profileReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
export default rootReducer;
