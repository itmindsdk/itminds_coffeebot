import {
  IonButtons,
  IonContent,
  IonHeader,
  IonMenuButton,
  IonPage,
  IonTitle,
  IonToolbar,
  IonCard,
  IonCardTitle,
} from "@ionic/react";
import React from "react";
import "./Profile.css";
import { fetchProfileInfo } from "redux/slices/profileSlice";
import { useDispatch, useSelector } from "react-redux";
import Claim from "viewModels/claim";
import { RootState } from "redux/reducer";

const Profile: React.FC = () => {
  // set the app the is being used.
  const dispatch = useDispatch();
  /*  dispatch(selectedApp(Number(AppType.PROFILE))); */

  const profileInfo = useSelector((state: RootState) => state.profileInfo).profileInfo;

  React.useEffect(() => {
    dispatch(fetchProfileInfo());
  }, [dispatch]);

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Profile</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonCard className="contentCard">
          <IonCardTitle>Information</IonCardTitle>
          <img
            alt={profileInfo?.user_id}
            className="profileImg"
            src={profileInfo?.user_claims.find((item: Claim) => item.typ === "picture")?.val}
          />
          <h3>Name</h3>
          <p>{profileInfo?.user_claims.find((item: Claim) => item.typ === "name")?.val}</p>
          <h3>User id</h3>
          <p>{profileInfo?.user_id}</p>
        </IonCard>
      </IonContent>
    </IonPage>
  );
};

export default Profile;
