import {
  IonButtons,
  IonContent,
  IonHeader,
  IonMenuButton,
  IonPage,
  IonTitle,
  IonToolbar,
  IonSearchbar,
  IonList,
} from "@ionic/react";
import React, { useState } from "react";
import "./ChannelSettings.css";
import GeneralSettings from "../../components/GeneralSettings/GeneralSettings";
import CoffeeCapSettings from "../../components/CoffeeCapSettings/CoffeeCapSettings";
import { isUndefined } from "util";
import ReminderSettings from "../../components/ReminderSettings";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "redux/reducer";
import { RouteComponentProps } from "react-router";
import { AppType } from "declarations";
import { selectedApp } from "../../redux/slices/appSelectedSlice";

interface SettingCards
  extends RouteComponentProps<{
    id: string;
  }> {}
{
}
export interface SettingPanel {
  element: any;
  featureEnabled: boolean;
  tags: string[];
}

const ChannelSettigs: React.FC<SettingCards> = ({ match }) => {
  const channelId = useSelector((state: RootState) => state.channelList).channelId;

  // set the app the is being used.
  const dispatch = useDispatch();
  /*  dispatch(selectedApp(Number(AppType.COFFEEBUDDIES))); */

  const allSettingPanels: SettingPanel[] = [
    {
      element: GeneralSettings,
      featureEnabled: true,
      tags: ["general", "dato", "start", "week", "group", "size", "send", "message", "threads", "text"],
    } as SettingPanel,
    {
      element: CoffeeCapSettings,
      featureEnabled: true,
      tags: ["coffee", "captain", "custom"],
    } as SettingPanel,
    {
      element: ReminderSettings,
      featureEnabled: true,
      tags: ["reminders", "custom", "text"],
    } as SettingPanel,
  ];

  const [panels, setPanels] = useState(allSettingPanels);

  const filter = (panels: SettingPanel[], searchWord: string | undefined): SettingPanel[] => {
    if ((searchWord as string).length > 0 && !isUndefined(searchWord)) {
      const word = (searchWord as string).toLowerCase();
      console.debug("searchWord: ", word);
      if (word.localeCompare("enabled") === 0) {
        console.debug("#enabled filter");
        //console.debug(allSettingPanels);

        return allSettingPanels.filter(panel => panel.featureEnabled);
      } else if (word.localeCompare("disabled") === 0) {
        console.debug("#disabled filter");
        return allSettingPanels.filter(panel => !panel.featureEnabled);
      } else {
        return allSettingPanels.filter(panel => panel.tags.filter(tag => tag.indexOf(word) > -1).length > 0);
      }
    }
    return allSettingPanels;
  };
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>List</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonSearchbar
        onIonChange={event => setPanels(filter(panels, event.detail.value))}
        placeholder="Search for setting"
        autoCorrect="on"
        spellcheck={true}
        animated
      ></IonSearchbar>
      <IonContent>
        <IonList>
          {panels.map((panel, i) => {
            return React.createElement(panel.element, {
              panel,
              key: i,
              channelId: channelId,
            });
          })}
        </IonList>
      </IonContent>
    </IonPage>
  );
};

export default ChannelSettigs;
