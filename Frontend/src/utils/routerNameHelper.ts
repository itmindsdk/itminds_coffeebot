import { AppType } from "declarations";
import { apps } from "../App";

export const URLNameFix = (name: string) => name
  .split(" ")
  .join("")
  .toLocaleLowerCase();

export const getNamePrefix = (appType: AppType) => {
  const app = apps.find((item) => item.type === appType);
  return app ? URLNameFix(app?.title) : "unknown_app";
};

export const getAppPrefixFromURL = (): string => window.location.href.split("/")[3]; // return name of app in use.
