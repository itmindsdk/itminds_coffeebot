import React, { useState } from "react";
import { IonToggle } from "@ionic/react";
import { withRouter, RouteComponentProps } from "react-router";
import { SettingPanel } from "../../pages/ChannelSettings/ChannelSettings";

interface toggelProps extends RouteComponentProps {
  featureEnabled: boolean;
  featureName: string;
  setFun: React.Dispatch<any>;
  panel: SettingPanel;
}

const SettingsToggle: React.FunctionComponent<toggelProps> = ({ featureEnabled, featureName, setFun, panel }) => {
  const [enabled, setEnabled] = useState(featureEnabled);

  const handleChange = (event: any) => {
    event.preventDefault();
    console.debug(featureName + " ", featureEnabled);
    setEnabled(Boolean(event.detail.checked));
    setFun(Boolean(event.detail.checked));
  };

  React.useEffect(() => {
    setEnabled(featureEnabled);
  }, [featureEnabled]);

  return (
    <div className="settingsToggle">
      <IonToggle onIonChange={handleChange} checked={enabled} />
    </div>
  );
};

export default withRouter(SettingsToggle);
