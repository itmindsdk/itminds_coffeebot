import React, { useState } from "react";
import { withRouter, RouteComponentProps } from "react-router";
import {
  IonCard,
  IonCardHeader,
  IonCardTitle,
  IonList,
  IonButton,
  IonIcon,
  IonText,
  IonCardContent,
  IonToast,
} from "@ionic/react";
import SettingsToggle from "../settingsToggle/SettingsToggle";
import { CustomText } from "../TextEdit/TextEdit";
import { SettingPanel } from "../../pages/ChannelSettings/ChannelSettings";
import ReminderItem from "../ReminderItem/ReminderItem";
import { add, alert, refresh } from "ionicons/icons";
import { Reminder, ReminderRepeatType } from "common";
import "./ReminderSettings.scss";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "redux/reducer";
import { getRemindersList, createReminder, deleteReminder, setEnabled } from "redux/slices/RemindersSlice";
import "../../assets/main.css";
import ReminderType from "components/ReminderItem/ReminderType";
import { setReminders } from "redux/slices/RemindersSlice";

interface ReminderSettingsProps
  extends RouteComponentProps<{
    id: string;
  }> {
  channelId: string;
  texts: CustomText[];
  panel: SettingPanel;
}

export interface ReminderItemData extends Reminder {
  id: number;
  saved: boolean;
  repeatType: number;
  repeatTypeEnumValue: number;
  typeEnumValue: number;
  toBeDeleted: boolean;
}

const ReminderSettings: React.FunctionComponent<ReminderSettingsProps> = ({ panel, match }) => {
  const id = match.params.id;
  const dispatch = useDispatch();
  const reminderList = useSelector((state: RootState) => state.ReminderList).listOfReminders;
  const featureEnabled: boolean = useSelector((state: RootState) => state.ReminderList.isFeatureEnabled);
  const loading: boolean | undefined = useSelector((state: RootState) => state.ReminderList.loading);
  const error: string | null = useSelector((state: RootState) => state.ReminderList.error);

  React.useEffect(() => {
    dispatch(getRemindersList(id));
  }, [error, id]);

  /*   React.useEffect(() => {
    setReminders(reminderList);
  }, [reminderList]); */

  const isThereAnyUnsavedItems = () => reminderList.every(item => item.saved);

  /*  const [reminders, setReminders] = useState(reminderArray); */
  const [, setReminderCount] = useState(reminderList.length);
  const [isFeatureEnabled, setIsFeatureEnabled] = useState(featureEnabled);
  const [unsavedItems, setUnsavedItems] = useState(isThereAnyUnsavedItems());

  const [showToast, setShowToast] = useState(false);

  React.useEffect(() => {
    dispatch(setEnabled(isFeatureEnabled, id));
  }, [isFeatureEnabled]);

  const addReminder = () => {
    const newReminder = {
      id: reminderList.length + 1,
      saved: false,
      RepeatDayNumber: 0,
      haveTriggered: false,
      channel: id,
      repeatType: ReminderRepeatType.NONE,
      repeatTypeEnumValue: ReminderRepeatType.NONE,
      text: "new",
      typeEnumValue: ReminderType.SIMPLE,
      type: ReminderType.SIMPLE,
    } as ReminderItemData;
    dispatch(setReminders([...reminderList, newReminder]));
    setReminderCount(reminderList.length);
    setUnsavedItems(isThereAnyUnsavedItems);
  };

  const refreshList = () => {
    dispatch(getRemindersList(id));
  };

  const saveAllReminder = () => {
    const mappedReminders = reminderList
      .map((item: ReminderItemData) => {
        if (!item.saved && !item.toBeDeleted) {
          const newItem = {
            ...item,
            channel: id,
          };
          console.debug("Saved item: ", newItem);
          dispatch(createReminder(item));
          return newItem;
        } else if (item.toBeDeleted) {
          console.debug("Deleted item: ", item);
          dispatch(deleteReminder(item.id));
          return undefined;
        }
        return {
          ...item,
          saved: true,
          toBeDeleted: false,
        };
      })
      .filter(rm => rm !== undefined) as ReminderItemData[];
    setReminders(mappedReminders);
    setReminderCount(mappedReminders.length);
    dispatch(getRemindersList(id));
  };

  const setToDeleteReminder = (itemIndex: Number) => {
    const mappedReminders = reminderList.map((item: ReminderItemData, index: Number) => {
      if (index === itemIndex) {
        const newItem = {
          ...item,
          saved: true,
          toBeDeleted: !item.toBeDeleted,
        };
        return newItem;
      }
      return item;
    });
    dispatch(setReminders([...mappedReminders]));
    setReminders(mappedReminders);
    setReminderCount(mappedReminders.length);
    /*     dispatch(getRemindersList(channelId)); */
  };

  const setReminderChanged = (reminderId: Number) => {
    const itemIndex = reminderList.findIndex(item => item.id == reminderId);
    const mappedReminders = reminderList.map((item: ReminderItemData, index: Number) => {
      if (index === itemIndex) {
        const newItem = {
          ...item,
          saved: false,
          toBeDeleted: false,
        };
        return newItem;
      }
      return item;
    });
    dispatch(setReminders([...mappedReminders]));
    setReminders(mappedReminders);
    setReminderCount(mappedReminders.length);
  };

  return (
    <IonCard className={`${isFeatureEnabled ? "" : "disabled"}` + " transAll"}>
      <IonCardContent>
        {error && <p color="red">An error occurred while fetching the data.</p>}
        <IonToast
          isOpen={showToast}
          onDidDismiss={() => setShowToast(false)}
          message="Your settings have been saved."
          duration={200}
        />
        {loading && <img src={"../../assets/loading.svg"} className="loading" />}
        <IonCardHeader>
          <div className="topcontrols">
            <IonCardTitle className="reminderTitle">
              Reminders
              {/*               {!unsavedItems && <IonBadge color="primary">unsaved</IonBadge>} */}
            </IonCardTitle>
            <SettingsToggle
              panel={panel}
              setFun={setIsFeatureEnabled}
              featureEnabled={isFeatureEnabled}
              featureName="Reminders"
            ></SettingsToggle>
            <IonButton className="addBtn" color="success" onClick={addReminder} disabled={!isFeatureEnabled}>
              <IonIcon slot="icon-only" icon={add} />
            </IonButton>
            <IonButton className="refreshBtn" color="primary" onClick={refreshList} disabled={!isFeatureEnabled}>
              <IonIcon slot="icon-only" icon={refresh} />
            </IonButton>
          </div>
        </IonCardHeader>
        <IonList>
          {reminderList.length > 0 ? (
            reminderList.map((reminder, i) => (
              <ReminderItem
                changeFunc={setReminderChanged}
                deleteFunc={() => {
                  setToDeleteReminder(i);
                }}
                key={i}
                featureEnabled={isFeatureEnabled}
                reminderData={reminder}
              ></ReminderItem>
            ))
          ) : (
            <IonText color="default">
              <div className="noReminders">
                <p>
                  <IonIcon icon={alert} size="large"></IonIcon>
                  There is no reminders on this channel.
                </p>
              </div>
            </IonText>
          )}
        </IonList>
        <IonButton onClick={saveAllReminder} color="success" className="ion-float-right saveBtn">
          Save
        </IonButton>
        {/*  <TextEdit texts={texts} /> */}
      </IonCardContent>
    </IonCard>
  );
};

export default withRouter(ReminderSettings);
