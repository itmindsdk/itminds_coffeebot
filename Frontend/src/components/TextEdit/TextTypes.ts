/**
 * All the texts that need to be changed by overiding the default have an enum value that can identified the text needed.
 */
enum TextTypes {
  WelcomeUserText,
  VacationText,
  BotPresentationText,
  OnlyAdminText,
  GroupSizeText,
  GroupSizeErrorText,
  SetGroupSizeText,
  UnknownCommandText,
  GroupWelcomeText,
  CoffeeCaptainPresentationText,
  GroupDetailsText,
  BotJoinText,
  OnlyAdminGroupSize,
  NotInt,
  GroupGraterThen,
  WelcomeGroup,
  WhoIsCaptain,
  CoffeeResponsible,
  RepostMsg,
  YourGroupCompleted,
  ChannelGroupCompleted,
  actionReminder,
  actionReminderDetails
}

export default TextTypes;
