import * as React from "react";
import TextTypes from "./TextTypes";
import {
  IonItem,
  IonLabel,
  IonInput,
  IonChip,
  IonGrid,
  IonRow,
  IonModal,
  IonButton,
  IonText,
  IonIcon,
} from "@ionic/react";
import "./TextEdit.css";
import { helpCircleOutline, refreshCircle } from "ionicons/icons";

export interface Props {
  texts: CustomText[];
  featureEnabled: boolean;
}

export interface TextAction {
  command: string;
  discription: string;
}

export interface CustomText {
  text: string;
  type: TextTypes;
  discription: string;
  actions: TextAction[];
}

interface ModalData {
  header: string;
  discription: string;
}

const TextEdit: React.FunctionComponent<Props> = ({ texts, featureEnabled }) => {
  const [showModal, setShowModal] = React.useState(false);
  const [modalContent, setModalContent] = React.useState({
    header: "tom",
    discription: "tom",
  } as ModalData);

  const [isfeatureEnabled, setisfeatureEnable] = React.useState(featureEnabled);

  React.useEffect(() => {
    setisfeatureEnable(featureEnabled);
  }, [featureEnabled]);

  return (
    <>
      <IonItem lines="none">
        <IonGrid>
          <IonModal isOpen={showModal} backdropDismiss={false}>
            <IonText color="primary" className="modalContent">
              <h2>{modalContent.header}</h2>
              <p>{modalContent.discription}</p>
            </IonText>
            <IonButton onClick={() => setShowModal(false)}>Close Modal</IonButton>
          </IonModal>
          {texts.map((text, i) => {
            return (
              <IonRow key={i}>
                <div className="block">
                  <IonLabel>
                    {text.discription}
                    {text.actions.map((action, i) => (
                      <IonChip
                        outline
                        color="primary"
                        key={i}
                        onClick={() => {
                          if (isfeatureEnabled) {
                            setModalContent({
                              header: action.command,
                              discription: action.discription,
                            } as ModalData);
                            setShowModal(true);
                          }
                        }}
                        className="tooltip-left"
                      >
                        <IonIcon icon={helpCircleOutline}></IonIcon>
                        <IonLabel>{action.command}</IonLabel>
                      </IonChip>
                    ))}
                    <IonButton className="ion-float-right" disabled={!isfeatureEnabled}>
                      <IonIcon icon={refreshCircle} slot="start"></IonIcon>
                      <IonLabel>Reset to deafult</IonLabel>
                    </IonButton>
                    {/*                     <IonButton className="ion-float-right">
                      <IonIcon icon={create}></IonIcon>
                      <IonLabel>Edit</IonLabel>
                    </IonButton> */}
                  </IonLabel>
                </div>
                <div className="block">
                  <IonItem className="text">
                    <IonInput disabled={!isfeatureEnabled} placeholder={text.text} className="input"></IonInput>
                  </IonItem>
                </div>
              </IonRow>
            );
          })}
        </IonGrid>
      </IonItem>
      <IonButton color="success" className="ion-float-right saveBtn" disabled={!isfeatureEnabled}>
        Save
      </IonButton>
    </>
  );
};

export default TextEdit;
