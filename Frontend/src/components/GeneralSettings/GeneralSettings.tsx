import * as React from "react";
import { RouteComponentProps, withRouter } from "react-router";
import {
  IonRange,
  IonLabel,
  IonItem,
  IonToggle,
  IonCardContent,
  IonDatetime,
  IonCard,
  IonCardTitle,
  IonCardHeader,
} from "@ionic/react";
import { SettingPanel } from "../../pages/ChannelSettings/ChannelSettings";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "redux/reducer";
import { GeneralSettingsData, fetchGeneralSettings, postGeneralSettings } from "redux/slices/GeneralSettingsSlice";

interface GeneralSettings
  extends RouteComponentProps<{
    id: string;
  }> {
  panel: SettingPanel;
  channelId: string;
}

const GeneralSettings: React.FunctionComponent<GeneralSettings> = ({ channelId, match }) => {
  /*   const channelId = useSelector((state: RootState) => state.channelList)
    .channelId; */

  const dispatch = useDispatch();
  const data = useSelector((state: RootState) => state.GeneralSettings);

  const [useThreads, setUseThreads] = React.useState(false);
  const [groupSize, setGroupSize] = React.useState(1);
  const [eventInterval, setEventInterval] = React.useState(1);
  const [RunDate, setRunDate] = React.useState(new Date());

  React.useEffect(() => {
    dispatch(fetchGeneralSettings(match.params.id));
  }, [channelId, match]);

  React.useEffect(() => {
    setUseThreads(data.useThreads);
    setGroupSize(data.groupSize);
    setEventInterval(data.eventInterval);
    setRunDate(data.runDate);
  }, [data.groupSize, data.error, data.eventInterval, data.useThreads, data.runDate]);

  const handelDataChange = () => {
    dispatch(
      postGeneralSettings(channelId, {
        error: null,
        groupSize: groupSize,
        useThreads: useThreads,
        eventInterval: eventInterval,
        runDate: RunDate,
      } as GeneralSettingsData),
    );
  };

  const customDayShortNames = ["s\u00f8n", "man", "tir", "ons", "tor", "fre", "l\u00f8r"];

  /* console.log("Date ", RunDate.toLocaleString()); */

  return (
    <IonCard>
      <IonCardHeader>
        <IonCardTitle>General settings</IonCardTitle>
      </IonCardHeader>
      <IonCardContent>
        <IonItem>
          <IonLabel>Next group creation date</IonLabel>
          <IonLabel>DDD. MMM DD, YY</IonLabel>
          <IonDatetime
            value={RunDate.toLocaleString()}
            dayShortNames={customDayShortNames}
            displayFormat="DD/MM/YYYY H:mm"
            monthShortNames="jan, feb, mar, apr, mai, jun, jul, aug, sep, okt, nov, des"
            onIonChange={e => () => {
              setRunDate(new Date(e.detail.value as string));
              handelDataChange();
            }}
          ></IonDatetime>
        </IonItem>
        <IonItem>
          <IonLabel>Group size</IonLabel>
          <IonRange
            ticks={true}
            snaps={true}
            pin={true}
            min={2}
            max={10}
            color="primary"
            className="range"
            value={groupSize}
            onIonChange={e => setGroupSize(Number(e.detail.value))}
            onIonBlur={handelDataChange}
          >
            <IonLabel slot="start">2</IonLabel>
            <IonLabel slot="end">10</IonLabel>
          </IonRange>
        </IonItem>
        <IonItem>
          <IonLabel>Send messeges in threads</IonLabel>
          <IonToggle
            slot="end"
            checked={useThreads}
            onIonChange={e => setUseThreads(e.detail.checked)}
            onIonBlur={handelDataChange}
          />
        </IonItem>
        <IonItem>
          <IonLabel>Number of week between creating groups</IonLabel>
          <IonRange
            ticks={true}
            snaps={true}
            pin={true}
            min={1}
            max={12}
            color="primary"
            className="range"
            value={eventInterval}
            onIonChange={e => setEventInterval(Number(e.detail.value))}
            onIonBlur={handelDataChange}
          >
            <IonLabel slot="start">1</IonLabel>
            <IonLabel slot="end">12</IonLabel>
          </IonRange>
        </IonItem>
        {/*         <IonItem>
          <IonText color="primary">
            <h2>Custom texts</h2>
          </IonText>
          <IonLabel>Custom texts</IonLabel>
          <IonChip>
            <IonLabel>#Group</IonLabel>
          </IonChip>
          <IonChip>
            <IonLabel>#Random</IonLabel>
          </IonChip>

          <input placeholder="one custom text"></input>
        </IonItem> */}
      </IonCardContent>
    </IonCard>
  );
};

export default withRouter(GeneralSettings);
