import React, { useEffect, useState } from "react";
import "./ReminderItem.css";
import {
  IonItem,
  IonGrid,
  IonRow,
  IonCol,
  IonSelect,
  IonSelectOption,
  IonInput,
  IonButton,
  IonIcon,
  IonReorder,
} from "@ionic/react";
import { trash, arrowBack } from "ionicons/icons";
import { ReminderItemData } from "components/ReminderSettings/ReminderSettings";
import ReminderType from "components/ReminderItem/ReminderType";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "redux/reducer";
import { setReminders } from "redux/slices/RemindersSlice";

export interface Props {
  featureEnabled: boolean;
  reminderData: ReminderItemData;
  /*  onReminderChange: (reminder: ReminderItemData) => void; */
  deleteFunc: (id: number) => void;
  changeFunc: (id: number) => void;
}

const ReminderItem: React.FunctionComponent<Props> = ({ featureEnabled, reminderData, deleteFunc, changeFunc }) => {
  const dispatch = useDispatch();
  const reminderList = useSelector((state: RootState) => state.ReminderList).listOfReminders;

  /*   const onReminderChange = () => {
    console.log("onReminderChange");

    dispatch(
      setReminders(
        reminderList.map(item =>
          item.id === reminderData.id
            ? (({
                id: item.id,
                RepeatDayNumber: repeatNum,
                channel: reminderData.channel,
                haveTriggered: reminderData.haveTriggered,
                saved: true,
                repeatType,
                repeatTypeEnumValue: repeatType,
                typeEnumValue: reminderType,
                type: reminderType,
                text,
                triggerDate: new Date()
              } as unknown) as ReminderItemData)
            : item
        )
      )
    );
  }; */
  const [isEdited, setIsEdited] = useState(false);
  const [isUnsaved, setIsUnsaved] = useState(reminderData.saved);
  const [repeatType, setRepeatType] = useState(reminderData.repeatTypeEnumValue);
  const [reminderType, setReminderType] = useState(reminderData.typeEnumValue);
  const [text, setText] = useState(reminderData.text);
  const [repeatNum, setRepeatNum] = useState(reminderData.RepeatDayNumber);

  useEffect(() => {
    setIsUnsaved(reminderData.saved);
    setText(reminderData.text);
    setRepeatType(reminderData.repeatTypeEnumValue);
    setRepeatNum(reminderData.RepeatDayNumber);
    setReminderType(reminderData.typeEnumValue);
    changeFunc(reminderData.id);
  }, [
    reminderData.saved,
    reminderData.text,
    reminderData.repeatType,
    reminderData.RepeatDayNumber,
    reminderData.repeatTypeEnumValue,
    reminderData.typeEnumValue,
  ]);

  useEffect(() => {
    if (!isEdited) {
      return;
    }
    /*  onReminderChange(); */
    changeFunc(reminderData.id);
  }, [repeatType, reminderType, text, repeatNum, isEdited]);

  return (
    <IonItem lines="none">
      <IonReorder slot="start" />
      <IonGrid className={`reminder ${!isUnsaved ? "unsaved" : ""}`}>
        <IonRow>
          <IonCol className="repeatType">
            <IonSelect
              interface="popover"
              value={reminderType}
              className="repeatTypeSelect"
              placeholder="Reminder type"
              disabled={!featureEnabled}
              onIonChange={e => {
                setReminderType(e.detail.value);
                setIsEdited(false);
              }}
            >
              <IonSelectOption value={0}>Simple reminder</IonSelectOption>
              <IonSelectOption value={1}>Action reminder</IonSelectOption>
            </IonSelect>
          </IonCol>
          <IonCol className="repeatType">
            <IonSelect
              value={repeatType}
              interface="popover"
              className="repeatTypeSelect"
              placeholder="repeat"
              disabled={!featureEnabled}
              onIonChange={e => {
                setRepeatType(Number(e.detail.value));
                setIsEdited(true);
              }}
            >
              <IonSelectOption value={0}>After {/* //Days after group creation */}</IonSelectOption>
              <IonSelectOption value={1}>
                Before
                {/* Days before next group creation */}
              </IonSelectOption>
            </IonSelect>
          </IonCol>
          <IonCol className="numInput">
            <IonInput
              value={String(repeatNum)}
              type="number"
              placeholder="number of days"
              disabled={!featureEnabled}
              onIonChange={e => {
                console.log(Number(e.detail.value));

                setRepeatNum(Number(e.detail.value));
                setIsEdited(true);
              }}
            ></IonInput>
          </IonCol>
          <IonCol>
            <IonInput
              value={text}
              placeholder="Text to be displayed in reminder"
              disabled={!featureEnabled || reminderType === Number(ReminderType.ACTION)}
              onIonChange={e => {
                setText(String(e.detail.value));
                setIsEdited(true);
              }}
            ></IonInput>
          </IonCol>
          <IonCol className="repeatType">
            <IonButton
              className="deleteBtn"
              color={reminderData.toBeDeleted ? "primary" : "danger"}
              disabled={!featureEnabled}
              onClick={() => {
                deleteFunc(reminderData.id);
              }}
            >
              <IonIcon slot="start" icon={reminderData.toBeDeleted ? arrowBack : trash}></IonIcon>
              {reminderData.toBeDeleted ? "Undo" : "Delete"}
            </IonButton>
          </IonCol>
        </IonRow>
      </IonGrid>
    </IonItem>
  );
};

export default ReminderItem;
