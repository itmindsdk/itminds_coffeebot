import React from "react";
import { AppPage, BotApp, AppType } from "declarations";
import { IonSlides, IonSlide, IonItem, IonAvatar, IonList, IonLabel, IonMenuToggle, IonIcon } from "@ionic/react";
import Claim from "viewModels/claim";
import ProfileInfo from "viewModels/ProfileInfo";

interface MenuMobileProp {
  menu: AppPage[];
  apps: BotApp[];
  profileInfo: ProfileInfo | undefined;
}

const MenuMobile: React.FC<MenuMobileProp> = ({ menu, apps, profileInfo }) => {
  return (
    <div className="mobileMenu">
      <IonSlides pager={true} className="menuSlider">
        <IonSlide>
          <IonList>
            {apps.map((app, i) => {
              return (
                <IonItem key={i}>
                  <IonAvatar slot="start">
                    <img
                      alt={app.title}
                      src={
                        app.type === AppType.PROFILE
                          ? profileInfo?.user_claims.find((item: Claim) => item.typ === "picture")?.val ?? app.imgUrl
                          : app.imgUrl
                      }
                    />
                  </IonAvatar>
                  <IonLabel>{app.title}</IonLabel>
                </IonItem>
              );
            })}
          </IonList>
        </IonSlide>
        <IonSlide>
          <IonList>
            {menu.map((appPage, index) => {
              return (
                <IonMenuToggle key={index} autoHide={false}>
                  <IonItem routerLink={appPage?.url ?? "/404"} routerDirection="forward">
                    <IonIcon slot="start" icon={appPage?.icon ?? alert} />
                    <IonLabel>{appPage?.title ?? "Not found"}</IonLabel>
                  </IonItem>
                </IonMenuToggle>
              );
            })}
          </IonList>
        </IonSlide>
      </IonSlides>
    </div>
  );
};

export default MenuMobile;
