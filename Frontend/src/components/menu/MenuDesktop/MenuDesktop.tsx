import React from "react";
import { RouteComponentProps, withRouter } from "react-router";
import { AppPage, BotApp, AppType } from "declarations";
import { useDispatch } from "react-redux";
import { IonList, IonRouterLink, IonMenuToggle, IonToolbar, IonTitle, IonItem, IonIcon, IonLabel } from "@ionic/react";
import { setChannelId } from "redux/slices/channelSlice";
import ReactTooltip from "react-tooltip";
import { URLNameFix } from "utils/routerNameHelper";
import ProfileInfo from "viewModels/ProfileInfo";
import Claim from "viewModels/claim";

interface MenuDesktopProp extends RouteComponentProps<{ id: string }> {
  menu: AppPage[];
  apps: BotApp[];
  profileInfo: ProfileInfo | undefined;
  appClick: (type: AppType) => void;
  appSelected: number;
  nameOfApp: BotApp | undefined;
}

const MenuDesktop: React.FC<MenuDesktopProp> = ({ menu, apps, profileInfo, appSelected, nameOfApp, appClick }) => {
  const dispatch = useDispatch();

  return (
    <div className="desktopMenu">
      <IonList lines="none" className="desktopApps">
        {apps.map((apps, i) => {
          return (
            <IonRouterLink key={i} routerDirection="forward" routerLink={`/${URLNameFix(apps.title)}/`}>
              <div
                data-tooltip={apps.title}
                onClick={() => {
                  appClick(apps.type);
                }}
                className={appSelected === apps.type ? "appActive tooltip-right app" : "tooltip-right app"}
              >
                <img
                  src={
                    apps.type === AppType.PROFILE
                      ? profileInfo?.user_claims.find((item: Claim) => item.typ === "picture")?.val ?? apps.imgUrl
                      : apps.imgUrl
                  }
                  className="appIcons"
                  alt={apps.title}
                />
                {apps.type === AppType.PROFILE ? <div className="bottomBar"></div> : ""}
              </div>
            </IonRouterLink>
          );
        })}
      </IonList>
      <IonList>
        <IonToolbar>
          <IonTitle>{nameOfApp ? nameOfApp.title : "no app selected"}</IonTitle>
        </IonToolbar>
        {menu.map((appPage, index) => {
          if (appPage !== undefined) {
            return (
              <IonMenuToggle key={index} autoHide={false}>
                <IonItem
                  routerLink={appPage?.url ?? "/404"}
                  routerDirection="forward"
                  onClick={() => {
                    dispatch(setChannelId(appPage.channelId));
                    console.debug("channel:", appPage.channelId);
                  }}
                >
                  <IonIcon slot="start" icon={appPage?.icon ?? alert} />
                  <IonLabel>{appPage?.title ?? "Not found"}</IonLabel>
                </IonItem>
                <ReactTooltip place="right" type="light" effect="solid" />
              </IonMenuToggle>
            );
          }
        })}
      </IonList>
    </div>
  );
};

export default withRouter(MenuDesktop);
