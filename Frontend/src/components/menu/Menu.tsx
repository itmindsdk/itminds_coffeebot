import { IonContent, IonHeader, IonMenu, IonTitle, IonToolbar } from "@ionic/react";
import React, { useEffect, useState } from "react";
import { RouteComponentProps, withRouter } from "react-router-dom";
import "./Menu.scss";
import { useSelector, useDispatch } from "react-redux";
import { settings } from "ionicons/icons";
import { Channel } from "common";
import { fetchProfileInfo } from "redux/slices/profileSlice";
import { getNamePrefix, getAppPrefixFromURL, URLNameFix } from "utils/routerNameHelper";
import { selectedApp } from "redux/slices/appSelectedSlice";
import MenuDesktop from "./MenuDesktop/MenuDesktop";
import MenuMobile from "./MenuMobile/MenuMobile";
import { fetchChannel } from "../../redux/slices/channelSlice";
import { RootState } from "../../redux/reducer";
import { AppPage, BotApp, AppType } from "../../declarations";

interface MenuProps extends RouteComponentProps {
  appPages: AppPage[];
  apps: BotApp[];
  appClick: (type: AppType) => void;
}

const Menu: React.FunctionComponent<MenuProps> = ({ appPages, apps, appClick, match }) => {
  const appSelected = useSelector((state: RootState) => state.appSelected).selected;
  const { profileInfo } = useSelector((state: RootState) => state.profileInfo);
  const appSelectedFilter = (page: AppPage) => page.relation === appSelected;
  const menuInit: AppPage[] = appPages.filter(appSelectedFilter);
  const [menu, setMenu] = useState(menuInit);
  const dispatch = useDispatch();
  const channelsData = useSelector((state: RootState) => state.channelList);
  const nameOfApp = apps.find(item => item.type === appSelected);

  const prefix = getAppPrefixFromURL();
  apps.forEach(app => {
    if (prefix.localeCompare(URLNameFix(app.title)) === 0) {
      dispatch(selectedApp(app.type));
    }
  });

  // fetch on mount
  useEffect(() => {
    dispatch(fetchChannel());
    dispatch(fetchProfileInfo());
  }, []);

  useEffect(() => {
    const tempChannels = channelsData.channels?.map((item: Channel) => {
      const appPage: AppPage = {
        url: `/${getNamePrefix(AppType.COFFEE_BUDDIES)}/${item.id}`,
        channelId: item.id,
        icon: settings,
        relation: AppType.COFFEE_BUDDIES,
        title: item.name ?? "Unknown channel",
      };
      return appPage;
    });
    setMenu(menuInit.concat(tempChannels?.filter(appSelectedFilter)));
  }, [appSelected, channelsData]);

  return (
    <IonMenu contentId="main" swipeGesture={false} type="overlay" className="menufix">
      <IonHeader>
        <IonToolbar>
          <IonTitle>Apps</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <MenuMobile apps={apps} menu={menu} profileInfo={profileInfo} />
        <MenuDesktop
          apps={apps}
          menu={menu}
          profileInfo={profileInfo}
          appClick={appClick}
          appSelected={appSelected}
          nameOfApp={nameOfApp}
        />
      </IonContent>
    </IonMenu>
  );
};

export default withRouter(Menu);
