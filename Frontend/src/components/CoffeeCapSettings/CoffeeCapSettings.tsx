import * as React from "react";
import { RouteComponentProps, withRouter } from "react-router";
import { IonCardContent, IonCard, IonCardTitle, IonCardHeader, IonCardSubtitle } from "@ionic/react";
import GeneralSettings from "../GeneralSettings/GeneralSettings";
import "./CoffeeCapSettings.css";

import TextTypes from "../TextEdit/TextTypes";
import SettingsToggle from "../settingsToggle/SettingsToggle";
import { SettingPanel } from "../../pages/ChannelSettings/ChannelSettings";
import TextEdit, { CustomText } from "../TextEdit/TextEdit";

interface GeneralSettings extends RouteComponentProps {
  featureEnabled: boolean;
  panel: SettingPanel;
  channelId: string;
}

const CoffeeCapSettings: React.FunctionComponent<GeneralSettings> = ({ featureEnabled, panel, channelId }) => {
  const [isFeatureEnabled, setIsFeatureEnabled] = React.useState(featureEnabled);

  /*  panel.featureEnabled = isFeatureEnabled;
   */
  /* const dispatch = useDispatch(setPanels([panel])); */

  const texts: CustomText[] = [];
  const one: CustomText = {
    type: TextTypes.BotJoinText,
    discription: "When the bot join's",
    text: "hej",
    actions: [{ command: "#hej", discription: "siger hej" }],
  };
  const two: CustomText = {
    type: TextTypes.BotJoinText,
    discription: "When the bot join's",
    text: "hej",
    actions: [{ command: "#hej", discription: "LOL" }],
  };
  texts.push(one);
  texts.push(two);

  return (
    <IonCard className={`${isFeatureEnabled ? "" : "disabled"}`}>
      <IonCardContent>
        <IonCardHeader>
          <SettingsToggle
            panel={panel}
            setFun={setIsFeatureEnabled}
            featureEnabled={isFeatureEnabled}
            featureName="Coffie cap"
          ></SettingsToggle>
          <IonCardTitle>Coffie captain</IonCardTitle>
          <IonCardSubtitle>Edit the text of the Coffie captain.</IonCardSubtitle>
        </IonCardHeader>
        <TextEdit texts={texts} featureEnabled={isFeatureEnabled} />
      </IonCardContent>
    </IonCard>
  );
};

export default withRouter(CoffeeCapSettings);
